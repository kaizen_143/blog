/*
SQLyog Professional v12.09 (64 bit)
MySQL - 10.1.13-MariaDB : Database - blog_db
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`blog_db` /*!40100 DEFAULT CHARACTER SET latin1 */;

/*Table structure for table `banner` */

DROP TABLE IF EXISTS `banner`;

CREATE TABLE `banner` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `org_file_name` varchar(200) NOT NULL,
  `file_name` varchar(50) NOT NULL,
  `order` int(3) NOT NULL DEFAULT '0',
  `display` int(1) DEFAULT '0',
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*Data for the table `banner` */

/*Table structure for table `blog` */

DROP TABLE IF EXISTS `blog`;

CREATE TABLE `blog` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `blog_url` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `blog_title` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `blog_sub_title` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `blog_desc` text CHARACTER SET utf8,
  `blog_author` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `type` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `date` date DEFAULT NULL,
  `date_released` date DEFAULT NULL,
  `order` int(11) DEFAULT '1',
  `user_id` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT '0',
  `display` text CHARACTER SET utf8,
  `thumbnail` text CHARACTER SET utf8,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

/*Data for the table `blog` */

/*Table structure for table `post_attach` */

DROP TABLE IF EXISTS `post_attach`;

CREATE TABLE `post_attach` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `blog_id` int(11) DEFAULT NULL,
  `path_file` text,
  `type` varchar(100) DEFAULT NULL,
  `thumb` int(10) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `post_attach` */

/*Table structure for table `star` */

DROP TABLE IF EXISTS `star`;

CREATE TABLE `star` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `star_desc` text CHARACTER SET utf8,
  `star_type` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `star_link` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `date` date DEFAULT NULL,
  `date_released` date DEFAULT NULL,
  `status` tinyint(1) DEFAULT '0',
  `order` int(11) DEFAULT '1',
  `user_id` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*Data for the table `star` */

/*Table structure for table `star_attach` */

DROP TABLE IF EXISTS `star_attach`;

CREATE TABLE `star_attach` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `star_id` int(11) DEFAULT NULL,
  `filepath` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `star_attach` */

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(25) DEFAULT NULL,
  `last_name` varchar(25) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `date_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `profile_pic` text,
  `role` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `users` */

insert  into `users`(`id`,`first_name`,`last_name`,`email`,`username`,`password`,`date_time`,`profile_pic`,`role`) values (2,'admin','admin','admin','admin','$2a$08$Pmo5hxwNl0Inim/4//8BdeU8njX89ORU1X9xcTj8u9vwrj4HlgEIC','2017-02-06 17:37:16','user_icon.png',1);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
