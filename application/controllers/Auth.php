<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

	public function __construct(){
			parent::__construct();
			$this->load->model('users_model');
			$this->load->library('bcrypt');
			$this->load->library('session');
		}

		public function check_loggedin(){
			if (!$this->session->logged_in) {
			redirect('/');
			}
			return;
			}		
	public function index()
	{
		if ($this->session->userdata('logged_in')) {
				redirect('admin/post');		
		}else{

		$data['js'] = array('actions/auth.js','actions/users.js');	
		$data['css'] = array('font-awesome.min.css','pnotify.css','bootstrap.min.css'  );
		$this->load->view('auth/auth',$data);
		$this->load->view('common/footer',$data);
			}
		
	}

	public function login(){
	
		$user = $this->input->post('username');
		$passinput = $this->input->post('password');
		$pass = $this->users_model->get_users($user);
		if ($pass == NULL OR $pass == "") {
			$msg = array('success' => FALSE,'msg' => 'Wrong Username of Password' );
					echo json_encode($msg);
					die();
		}
		if ($this->bcrypt->check_password($passinput, $pass->password) )
			{	
					$data = array(
					'id'  => $pass->id,
					'username'     =>  $pass->username,
					'logged_in' => TRUE,
					'role' => $pass->role,
					);

					$this->session->set_userdata($data);	
					$msg = array('success' => TRUE,'msg' => 'Login Success' );
					echo json_encode($msg);
			}
			else
			{
				$msg = array('success' => FALSE,'msg' => 'Login Fail' );
					echo json_encode($msg);

			}
			
	}

	public function logout()
	{
		$this->session->sess_destroy();
		redirect('/');
	}


}