<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Study extends CI_Controller {
		public function __construct()
			{
			parent::__construct();
			$this->config->load('pagination', TRUE);
			$this->load->library('pagination');
			$this->pager_settings = $this->config->item('pager_settings', 'pagination');
			$this->load->model('post_model');
			$this->load->model('study_model');

			}
		public function check_loggedin(){
			if (!$this->session->logged_in) {
			redirect('/');
			}
			return;
			}


		public function index()
			{
			$this->check_loggedin();
			$data['title'] =  "Download Maintenance";
			$user_id = $this->session->userdata('id');
			$data['js'] = array('actions/download.js');	
			$type = "study";
			$data['page_title'] = "趣味学习";
			$data['link'] = 'study/add_study';
			$this->load->library('pagination');
			$this->pager_settings['base_url'] = base_url().'/study/index';
            $this->pager_settings['total_rows'] = $this->post_model->count_info($user_id,$type);          
			$this->pagination->initialize($this->pager_settings);
			$data['pager']   = $this->pagination->create_links();
 			$data['download_center'] = $this->post_model->view_posts($type,$user_id,$this->pager_settings['per_page'], $this->uri->segment(3));	
 			$data['count_data'] = $this->post_model->count_data($type);			
		$this->load->view('common/header',$data);
		$this->load->view('common/admin_nav',$data);
		$this->load->view('download_center/index');
		$this->load->view('common/footer',$data);
			}

		public function add_study(){
			$this->check_loggedin();
			$data['js'] = array('actions/download.js');
			$data['type'] = 'study';
			$data['link'] = 'admin/study';
			
			$this->load->view('common/header');
			$this->load->view('common/admin_nav');
			$this->load->view('download_center/add_download',$data);
			$this->load->view('common/footer',$data);
			}

		public function add_new_study(){

			if ($this->session->userdata('logged_in')) {
			$title = $this->input->post('study_title');
			$status = $this->input->post('status');
			$link = $this->input->post('study_link');
			$find = array('https://','http://');
			$link = str_replace($find,"",$link);
			$link = 'http://'.$link;
			$desc = $this->input->post('study_desc');
			$desc = str_replace(base_url(),"../../",$desc);	
			$user_id = $this->session->userdata('id');
			$date_today = gmdate('Y-m-d', strtotime('now'));
			if ($status == 0) {
			$data = array(	'user_id' => $user_id,
			'blog_title' 	=> $title,
			'blog_link' 	=> $link,
			'blog_desc' 	=> $desc,
			'type' 			=> 'study',
			'date' => $date_today);
			}
			if ($status == 1) {
			$data = array(	
			'status' => $status,
			'user_id' => $user_id,
			'blog_title' 	=> $title,
			'blog_link' 	=> $link,
			'blog_desc' 	=> $desc,
			'type' 			=> 'study',
			'date' => $date_today,
			'date_released' => $date_today);
			}

			$last_id = $this->post_model->add_post($data);
			if(!empty($last_id)){
			$msg = array('success' => TRUE,'id'=>$last_id );
			echo json_encode($msg);
			}
			}else{
			redirect('/');
			}

			}

		public function update_study(){
			$this->check_loggedin();
			$study_title = $this->input->post('study_title');
			$id = $this->input->post('id');
			$study_link = $this->input->post('study_link');
			$study_desc = $this->input->post('study_desc');
			$study_desc = str_replace(base_url(),"../../",$study_desc);	
			$find = array('https://','http://');
			$study_link = str_replace($find,"",$study_link);
			$study_link = 'http://'.$study_link;
			$data = array('blog_title' => $study_title,
			'blog_link' => $study_link,
			'blog_desc' => $study_desc, );

			$result = $this->post_model->update($id,$data);
			if ($result) {
			$msg = array('success' => TRUE,'msg'=>"Post update successfully!" );
			echo json_encode($msg);
			}
			}

		public function edit_study()
			{
			$this->check_loggedin();
			$id = $this->input->post('id');
			$result = $this->study_model->studyInfo($id);
			echo json_encode($result);
			}

		public function upload_file($id,$type){

			if(count($_FILES['file']['name']) > 1){
			foreach ($_FILES['file']['name'] as $key => $value) {
			$file = $_FILES['file']['name'][$key];
			$extension = pathinfo($file,PATHINFO_EXTENSION);
			$filename = basename($file,'.'.$extension);			 
			$date_today = strtotime('now');
			$uploaddir =  FCPATH.'uploads/study/';
			$uploadfile = $uploaddir .$filename.$date_today."." .$extension;
			$uploadurl = $filename.$date_today."." .$extension;
			if (move_uploaded_file($_FILES['file']['tmp_name'][$key], $uploadfile)) {
			//echo "File successfully uploaded.\n";
			$data[] = array('blog_id' => $id,'path_file'=>$uploadurl,'type'=>$type, 'thumb'=> 1 );

			}else{
			//echo "Something WentWrong";
			}
			}
			}else{

			$file = is_array($_FILES['file']['name'])? $_FILES['file']['name'][0] :$_FILES['file']['name'];
			$extension = pathinfo($file,PATHINFO_EXTENSION);
			$filename = basename($file,'.'.$extension);		 
			$date_today = strtotime('now');
			$uploaddir =  FCPATH.'uploads/study/';
			$uploadfile = $uploaddir .$filename.$date_today."." .$extension;
			$uploadurl = $filename.$date_today."." .$extension;

			if (move_uploaded_file(is_array($_FILES['file']['tmp_name'])?$_FILES['file']['tmp_name'][0]:$_FILES['file']['tmp_name'], $uploadfile)) {
			//echo "File successfully uploaded.\n";


			$data[] = array('blog_id' => $id,'path_file'=>$uploadurl,'type'=>$type, 'thumb'=> 1 );

			}else{
			//echo "Something WentWrong";
			}
			}	
			if(!empty($this->post_model->attach_path($data))){
			echo json_encode(array("status"=>"success"));
			}

			} 

		public function change_thumb(){
			$id = $this->input->post('id');
			$result = $this->study_model->change_thumb($id);
			if ($result) {
			$msg = array('success' => TRUE,'msg' => 'please upload new thumbnails' );
			echo json_encode($msg);
			}
			}

		public function delete_study()
			{
				$this->check_loggedin();
				$id = $this->input->post('id');
				$result = $this->study_model->delete($id);
				if ($result) {
				$msg = array('success' => TRUE, 'msg' => 'Post Deleted', );
				echo json_encode($msg);
				}
			}

		public function change_thumbnails()
			{
				$file_id = $this->input->post('file_id');
				$id = $this->input->post('id');
				$data = array('status' => 0, );
				$this->post_model->update($id,$data);
				$result = $this->post_model->change_thumbnails($file_id);

				$path = FCPATH . "uploads/post/".$result;

				$result = unlink($path);

				if ($result) {
				$msg = array('success' => TRUE,'msg' => 'File remove please select new post thumbnail.' );
				}
			}

		public function change_status(){
		$this->check_loggedin();
		$id = $this->input->post('id');
		$status = $this->input->post('status');
		if ($status == 0 ) {
			$message = "Post Save to daft.";
		}

		if ($status ==1) {
			$message = "Post published";
		}
		$date_today = gmdate('Y-m-d', strtotime('now'));
		$data = array('status' => $status, 'date_released'=> $date_today);
		$result = $this->post_model->update($id,$data);
		if ($result) {
			$msg = array('success' => TRUE,'msg'=>$message );
			echo json_encode($msg);
		}

	}	





}