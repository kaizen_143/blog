<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Post extends CI_Controller {
public function __construct()
	{
		parent::__construct();
		$this->config->load('pagination', TRUE);
		$this->load->library('pagination');
        $this->pager_settings = $this->config->item('pager_settings', 'pagination');
		$this->load->helper('download');
		$this->load->model('post_model');

	} 

        public function check_loggedin(){
            if (!$this->session->logged_in) {
                redirect('/');
            }
                return;
        }	
public function index()
	{
		$this->check_loggedin();
			$blog_title 	= $this->input->post('blog_title');
			$blog_status 	= $this->input->post('blog_status');
			$date_from 		= $this->input->post('date_from');
			$date_to 		= $this->input->post('date_to');
			$user_id 		= $this->session->userdata('id');
			$type 			= 'post';
			$this->load->library('pagination');
			$this->pager_settings['base_url'] = base_url().'post/index/';
            $this->pager_settings['total_rows'] = $this->post_model->count_info($user_id,$type);          
			$this->pagination->initialize($this->pager_settings);
			$data['pager']   = $this->pagination->create_links();
 			$data['posts'] = $this->post_model->view_posts($type,$user_id,$this->pager_settings['per_page'], $this->uri->segment(3));

			$data['js'] = array('actions/view_post.js');		
			
			$this->load->view('common/header');
			$this->load->view('common/admin_nav');
			$this->load->view('post/view_posts', $data);
			$this->load->view('common/footer',$data);
		
	}
	public function add_new()
	{
		$this->check_loggedin();
		$data['js'] = array('actions/post.js');
		$this->load->view('common/header');
		$this->load->view('common/admin_nav');
		$this->load->view('post/post');
		$this->load->view('common/footer',$data);
		
	}



	public function add_post(){
		if ($this->session->userdata('logged_in')) {
		$title = $this->input->post('t_post');
		$status = $this->input->post('status');
		$post_author = $this->input->post('post_author');
		$sub_title = $this->input->post('sub_title');
		$blog_url = $title;
		$find = array('+','/','@',' ');
		$blog_url = str_replace($find,"-",$blog_url);
		$blog_url = strtolower($blog_url);
		// $count = $this->post_model->checkblogUrl($blog_url);
		
		// if ($count >= 1) {
		// 	$count = $count+1;
		// 	$blog_url = $blog_url.'-'.$count;
		// }
		
		$desc = $this->input->post('desc');
		
		$desc = str_replace(base_url(),"../../",$desc);		

		$user_id = $this->session->userdata('id');
		$date_today = gmdate('Y-m-d', strtotime('now'));
		if ($status == 0) {
					$data = array(
						'user_id' => $user_id,
						'blog_author' => $post_author,
						'blog_title' 	=> $title,
						'blog_sub_title' 	=> $sub_title,
						// 'blog_url' 	=> $blog_url,
						'blog_desc' 	=> $desc,
						'type' 			=> 'post',
						'date' => $date_today);
				}
		if ($status == 1) {
					$data = array(	
						'status' => $status,
						'user_id' => $user_id,
						'blog_author' => $post_author,
						'blog_title' 	=> $title,
						'blog_sub_title' 	=> $sub_title,
						// 'blog_url' 	=> $blog_url,
						'blog_desc' 	=> $desc,
						'type' 			=> 'post',
						'date' => $date_today,
						'date_released' => $date_today);
		}




		$last_id = $this->post_model->add_post($data);
		if(!empty($last_id)){
			$msg = array('success' => TRUE,'id'=>$last_id );
			echo json_encode($msg);
		}
		}else{
			redirect('/');
		}
		
	}
 public function upload_blog()
    {
    	$this->check_loggedin();
        $callback = 'null';
        $url = '';
        $get = array();
        $msg = '';
 
        $qry = $_SERVER['REQUEST_URI'];
        parse_str(substr($qry, strpos($qry, '?') + 1), $get);
 
        if (!isset($_POST) || !isset($get['CKEditorFuncNum'])) {
            $msg = 'CKEditor instance not defined. Cannot upload image.';
        } else {
            $callback = $get['CKEditorFuncNum'];
 
            try {

    			$config['upload_path']          = FCPATH.'/uploads/blog_image/';
		        $config['allowed_types']        = 'gif|jpg|png';
		        $config['max_size']             = 3000;
		        $config['encrypt_name']			= TRUE;


		        $this->load->library('upload', $config);

		        if ( ! $this->upload->do_upload('upload'))
		        {
		        		$error = array('error' => $this->upload->display_errors());

		        		$msg = "Error! Make sure your image is valid. Allowed file types are .jpg, .gif and .png only. Maximum file size is 3MB";
		        }
		        else
		        {
		        	$data = array('upload_data' => $this->upload->data());
		            
		          
	                $url = base_url() .'uploads/blog_image/'. $data['upload_data']['file_name'];
	                $msg = "File uploaded successfully to: {$url}";
		                
		        }

            } catch (Exception $e) {
                $url = '';
                $msg = $e->getMessage();
            }
        }
 
        // Callback function that inserts image into correct CKEditor instance
        $output = '<html><body><script type="text/javascript">' .
            'window.parent.CKEDITOR.tools.callFunction(' .
            $callback .
            ', "' .
            $url .
            '", "' .
            $msg .
            '");</script></body></html>';
 
        echo $output;
    }


	public function upload_file($id,$type){


if(count($_FILES['file']['name']) > 1){
	foreach ($_FILES['file']['name'] as $key => $value) {
		$file = $_FILES['file']['name'][$key];
		$extension = pathinfo($file,PATHINFO_EXTENSION);
		$filename = basename($file,'.'.$extension);
		 $rn = rand(10,10000000);
		$date_today = strtotime('now');
		$uploaddir =  FCPATH.'uploads/post/';
		$uploadfile = $uploaddir .$rn.$date_today.$rn."." .$extension;
		$uploadurl = $rn.$date_today.$rn."." .$extension;
		if (move_uploaded_file($_FILES['file']['tmp_name'][$key], $uploadfile)) {
	   	 //echo "File successfully uploaded.\n";
	   	 if ($type == 'video' OR $type == 'photo') {
	   	 	$thumb = 1;
	   	 }else{
	   	 	$thumb = 0;
	   	 }
	   	 $data[] = array('blog_id' => $id,'path_file'=>$uploadurl,'type'=>$type, 'thumb'=> $thumb );
	   	 
		}else{
			//echo "Something WentWrong";
		}
	}
}else{

		$file = is_array($_FILES['file']['name'])? $_FILES['file']['name'][0] :$_FILES['file']['name'];
		$extension = pathinfo($file,PATHINFO_EXTENSION);
		$filename = basename($file,'.'.$extension);
		 $rn = rand(10,10000000);
		$date_today = strtotime('now');
		$uploaddir =  FCPATH.'uploads/post/';
		$uploadfile = $uploaddir .$rn.$date_today.$rn."." .$extension;
		$uploadurl = $rn.$date_today.$rn."." .$extension;

		if (move_uploaded_file(is_array($_FILES['file']['tmp_name'])?$_FILES['file']['tmp_name'][0]:$_FILES['file']['tmp_name'], $uploadfile)) {
	   	 //echo "File successfully uploaded.\n";
	   	 if ($type == 'video' OR $type == 'photo') {
	   	 	$thumb = 1;
	   	 }else{
	   	 	$thumb = 0;
	   	 }

	   	 $data[] = array('blog_id' => $id,'path_file'=>$uploadurl,'type'=>$type, 'thumb'=> $thumb );
	   	 
		}else{
			//echo "Something WentWrong";
		}
}	

		if(!empty($this->post_model->attach_path($data))){
			echo json_encode(array("status"=>"success"));
		}
		
		
		

	} 

	

	public function get_post($id)
	{
		$this->post_model->get_post($id);
	}

	public function update_order()
	{
		$this->check_loggedin();
		$id = $this->input->post('id');
		$order_by = $this->input->post('order_by');
		$data = array('order' => $order_by, );
		$result  = $this->post_model->update($id,$data);
		if ($result) {
			$msg = array('success' => TRUE,'msg'=>'Order successfully updated!' );
			echo json_encode($msg);
		}
	}

		public function update_status()
	{
		$this->check_loggedin();
		$id = $this->input->post('id');
		$status = $this->input->post('status');
		$date_today = gmdate('Y-m-d', strtotime('now'));
		$schedule_date = $this->input->post('schedule_post');

	
		if ($status == 1) {
			$thumb = $this->post_model->checkthumb($id);
			if ($thumb >= 1) {
				$data = array('status' => $status,'date_released'=>$date_today, 'date' => $date_today);
				$date_released = $date_today;
				$message = "Post Published!";
			}else{
				$message = "Post Thumbnail is missing";
				$msg = array('success' => FALSE,'msg'=> $message  );
				echo json_encode($msg);
				die();
			}
			
		}

		if ($status == 2) {
			
			$data = array('status' => $status,'date_released'=>$schedule_date, 'date' => $date_today);
			$date_released = $schedule_date;
			$message = "Successfully scheduled post!";

		}

		if ($status == 0) {
			$schedule_date = $this->input->post('schedule_date');
			$data = array('status' => $status,'date_released'=> "Not Yet Published", 'date' => $date_today);
			$date_released = "Not Yet Published";
			$message = "Save post to draft";
		}
		
		$result  = $this->post_model->update($id,$data);

		if ($result) {
			$msg = array('success' => TRUE,'msg'=> $message ,'date'=>$date_released );
			echo json_encode($msg);
		}
	}
	
public function download_checkthumb()
	{
		$this->check_loggedin();
		$id = $this->input->post('id');
		$status = $this->input->post('status');
		$date_today = gmdate('Y-m-d', strtotime('now'));
		$schedule_date = $this->input->post('schedule_post');

	
		if ($status == 1) {
			$thumb = $this->post_model->download_checkthumb($id);
			if ($thumb >= 1) {
				$data = array('status' => $status,'date_released'=>$date_today, 'date' => $date_today);
				$date_released = $date_today;
				$message = "Post Published!";
			}else{
				$message = "Post Thumbnail is missing";
				$msg = array('success' => FALSE,'msg'=> $message  );
				echo json_encode($msg);
				die();
			}
			
		}

		if ($status == 2) {
			
			$data = array('status' => $status,'date_released'=>$schedule_date, 'date' => $date_today);
			$date_released = $schedule_date;
			$message = "Successfully scheduled post!";

		}

		if ($status == 0) {
			$schedule_date = $this->input->post('schedule_date');
			$data = array('status' => $status,'date_released'=> "Not Yet Published", 'date' => $date_today);
			$date_released = "Not Yet Published";
			$message = "Save post to draft";
		}
		
		$result  = $this->post_model->update($id,$data);

		if ($result) {
			$msg = array('success' => TRUE,'msg'=> $message ,'date'=>$date_released );
			echo json_encode($msg);
		}
	}

	public function delete_post()
	{
		$this->check_loggedin();
		$id = $this->input->post('id');
		$result  = $this->post_model->delete_post($id);
		if ($result) {
			$msg = array('success' => TRUE,'msg'=>'Post successfully Deleted!' );
			echo json_encode($msg);
		}
	}

	public function edit_post(){
		$this->check_loggedin();
		$id = $this->input->post('id');	
		$result = $this->post_model->get_post($id);

		if ($result) {			
			echo json_encode($result);
		}

	}


	public function edit_post_details()
	{
			$this->check_loggedin();
			$id = $this->input->post('id');
			$post_title = $this->input->post('post_title');
			$post_sub_title = $this->input->post('post_sub_title');
			$blog_desc = $this->input->post('blog_desc');
			$post_author = $this->input->post('post_author');
			$blog_desc = str_replace(base_url(),"../../",$blog_desc);	
			$blog_url = $post_title;
			$find = array('+','/','@',' ');
			$blog_url = str_replace($find,"-",$blog_url);
			$blog_url = strtolower($blog_url);
			// $count = $this->post_model->checkblogUrl($blog_url);
			
			// if ($count >= 1) {
			// 	$count = $count+1;
			// 	$blog_url = $blog_url.'-'.$count;
			// }
						
			$data = array('blog_title' => $post_title,
			'blog_sub_title ' => $post_sub_title, 
			'blog_url ' => $blog_url, 
			'blog_author ' => $post_author, 
			'blog_desc ' => $blog_desc );
			$result = $this->post_model->update($id,$data);
			if ($result){
			$msg = array('success' => TRUE,'msg'=>'Blog Post has been updated' );
			echo json_encode($msg);
			}
	}

	public function view_image(){

		$id = $this->input->post('id');
		$result  = $this->post_model->view_image($id);		
		echo json_encode($result);
		
	}

	public function download_file(){
		$id = $this->input->post('id');
		$result  = $this->post_model->view_image($id);
		echo "<pre>";
		var_dump($result);
		echo "</pre>";
		foreach ($result as $row) {
			$filepath = FCPATH."uploads/post/".$row->path_file;
			force_download($filepath,null);
		}
		
	}

	public function change_thumbnails()
	{
		
		$file_id = $this->input->post('file_id');
		$id = $this->input->post('id');
		if (empty($id)) {
			die();
		}
		$data = array('status' => 0, );
		$this->post_model->update($id,$data);
		$result = $this->post_model->change_thumbnails($file_id);	
		$path = FCPATH . "uploads/post/".$result;		
		$del = unlink($path);

		if ($del) {
			$msg = array('success' => TRUE,'msg' => 'File remove please select new post thumbnail.' );
			echo json_encode($msg);
					
		}
	}

		public function change_file()
	{
		
		$file_id = $this->input->post('file_id');
		$id = $this->input->post('id');
		if (empty($id)) {
			die();
		}
		// $data = array('status' => 0, );
		// $this->post_model->update($id,$data);
		$result = $this->post_model->change_thumbnails($file_id);	
		$path = FCPATH . "uploads/post/".$result;		
		$del = unlink($path);

		if ($del) {
			$msg = array('success' => TRUE,'msg' => 'File remove please add new file.' );
			echo json_encode($msg);
					
		}
	}


	public function view_files()
	{
		$id = $this->input->post('id');
		$result = $this->post_model->GetFiles($id);
		echo json_encode($result);
	}

	
}
