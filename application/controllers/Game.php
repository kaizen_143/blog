<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Game extends CI_Controller {
public function __construct()
	{
		parent::__construct();
		 $this->config->load('pagination', TRUE);
        $this->pager_settings = $this->config->item('pager_settings', 'pagination');
		$this->load->model('game_model');
		$this->load->model('post_model');

	}

	public function check_loggedin(){
		if (!$this->session->logged_in) {
			redirect('/');
		}
		return;
	}


	public function add_game(){
		$this->check_loggedin();
		$data['js'] = array('actions/game.js');	
		$this->load->view('common/header');
		$this->load->view('common/admin_nav');
		$this->load->view('game/add_game');
		$this->load->view('common/footer',$data);
	}

	public function index(){
		$this->check_loggedin();
		$data['js'] = array('actions/game.js');	
		$id = $this->session->id;

		$this->load->library('pagination');
		$this->pager_settings['base_url'] = base_url().'/game/index/';
        $this->pager_settings['total_rows'] = $this->game_model->count_game($id);       
       echo $this->pager_settings['total_rows'];
		$this->pagination->initialize($this->pager_settings);
		$data['pager']   = $this->pagination->create_links();
		
		$data['game_info'] = $this->game_model->game_info($id,$this->pager_settings['per_page'], $this->uri->segment(3));
		$this->load->view('common/header');
		$this->load->view('common/admin_nav');
		$this->load->view('game/game_main',$data);
		$this->load->view('common/footer',$data);
		
		
	}

	public function add_game_func(){
		$this->check_loggedin();
		$game_desc = $this->input->post('game_desc');
		$status = $this->input->post('status');
		$post_link = $this->input->post('post_link');
		$game_sub = $this->input->post('game_sub');
		$type = $this->input->post('type');
		$user_id = $this->session->userdata('id');
		$game_title = $this->input->post('game_title');
		$blog_auth = $this->input->post('game_link');
		$date_today = gmdate('Y-m-d', strtotime('now'));
		$find = array('https://','http://');
		$link = str_replace($find,"",$post_link);
		$post_link = 'http://'.$link;
		if ($status == 1) {
			$date_release = $date_today;
		}

		if ($status == 0) {
			$date_release = "";
		}

		$data = array('blog_title' => $game_title,
					'blog_author'=> $blog_auth,
					'blog_sub_title'=> '',
					'blog_author'=> '',
					'blog_desc' => '',
					'display' => '',
					'thumbnail' => '',					
					'date' => $date_today,
					'blog_url' => $post_link,
					'status' => $status,
					'date_released' => $date_release,
					'user_id'=> $user_id,
					'type'=> $type);
		$result = $this->game_model->insert_game($data);

		$msg = array('success' => true,'id'=>$result );
		echo json_encode($msg);
	}
	public function test(){
			$rn = rand(10,10000000);
			echo $rn;
	}
	public function upload_file($id,$type){
		$this->check_loggedin();
		$array = explode('.', $_FILES['file']['name']);
		$date_today = gmdate('Y-m-d', strtotime('now'));
		$extension = end($array);
		$rn = rand(10,10000000);
		$uploaddir =  FCPATH.'uploads/game/';
		$uploadfile = $uploaddir .$date_today.$rn."." .$extension;
		$uploadurl = $date_today.$rn."." .$extension;

			if (move_uploaded_file($_FILES['file']['tmp_name'], $uploadfile)) {
		   	 echo "File successfully uploaded.\n";

		   	 $data = array('blog_id' => $id,'path_file'=>$uploadurl,'type'=>$type);
		   	 $this->game_model->attach_path($data);
			}else{
				echo "Something Went Wrong";
			}
	}

	public function delete_game(){
		$this->check_loggedin();
		$id = $this->input->post('id');
		$result = $this->game_model->delete($id);
		if ($result) {
			$msg = array('success' => TRUE, 'msg' => 'Game Post Delete', );
			echo json_encode($msg);
		}
	}

	public function edit_game()
	{
		$this->check_loggedin();
		$id = $this->input->post('id');
		$result = $this->game_model->GameInfo($id);
		echo json_encode($result);
	}

	public function change_thumb(){
		$this->check_loggedin();
		$id = $this->input->post('id');
		$type = $this->input->post('type');
		
		
		$result = $this->game_model->change_thumb($id);
		if ($result['result']) {
			$path = FCPATH . 'uploads/game/'.$result['file'];
			unlink($path);
			$msg = array('success' => TRUE,'msg' => 'please upload new thumbnails' );
			echo json_encode($msg);
		}

	}

	public function change_status(){
		$this->check_loggedin();
		$id = $this->input->post('id');
		$status = $this->input->post('status');
		if ($status == 0 ) {
			$message = "Post Save to daft.";
		}

		if ($status ==1) {
			$message = "Post published";
		}
		$date_today = gmdate('Y-m-d', strtotime('now'));
		$data = array('status' => $status, 'date_released'=> $date_today);
		$result = $this->game_model->change_status($id,$data);
		if ($result) {
			$msg = array('success' => TRUE,'msg'=>$message );
			echo json_encode($msg);
		}

	}

	public function update_game(){
		$this->check_loggedin();
		$game_title = $this->input->post('game_title');
		$blog_url = $this->input->post('blog_url');
		$id = $this->input->post('id');
				$find = array('https://','http://');
		$link = str_replace($find,"",$blog_url);
		$blog_url = 'http://'.$link;
	

		$data = array('blog_title' => $game_title,
					  'blog_url' => $blog_url,
					   );

		$result = $this->post_model->update($id,$data);
		if ($result) {
			$msg = array('success' => TRUE,'msg'=>"Post update successfully!" );
			echo json_encode($msg);
		}
	}

}