<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
	public function __construct(){
			parent::__construct();
			
			$this->load->library('session');
			$this->load->model('users_model');
				$this->load->library('bcrypt');
		}
	public function check_loggedin(){
		if (!$this->session->logged_in) {
			redirect('/');
		}
		return;
	}		
	public function index()
	{
		$this->check_loggedin();

		$data['js'] = array('login-common.js');
		$data['css'] = array('dropzone.min.css','basic.min.css');
		$id =$this->session->userdata('id');
		$data['userinfo'] = $this->users_model->getUserData($id);
		$this->load->view('main',$data);
		$this->load->view('common/footer',$data);
		
	}

	public function edit_account()
	{
		if ($this->session->logged_in) {
			
		}else{
			redirect('/');
		}
	}	

	public function change_profile(){
		$this->check_loggedin();
	
		$id = $this->session->id;
		$array = explode('.', $_FILES['file']['name']);
		$date_today = gmdate('Y-m-d', strtotime('now'));
		$extension = end($array);
		$uploaddir =  FCPATH.'profile_pic/';
		$uploadfile = $uploaddir .$_FILES['file']['name'].$date_today."." .$extension;
		$uploadurl = $_FILES['file']['name'].$date_today."." .$extension;

			if (move_uploaded_file($_FILES['file']['tmp_name'], $uploadfile)) {
		   	 echo "File successfully uploaded.\n";

		   	 $data = array('profile_pic'=>$uploadurl);
		   	 $result = $this->users_model->profile_change($id,$data);

			}else{
				echo "Something Went Wrong";
			}
	
	}

	public function update_profile()
	{	
		$this->check_loggedin();
		$id = $this->session->id;
			$first_name = $this->input->post('first_name');
			$last_name = $this->input->post('last_name');
			$email = $this->input->post('email');
			$pass = $this->input->post('pass');			
			if (!empty($pass)) {
			$hash_password = $this->bcrypt->hash_password($pass);				
			$data = array('first_name' => $first_name,
						  'last_name' => $last_name,
						  'email' => $email,
						  'password'=> $hash_password);
			}else{				
			$data = array('first_name' => $first_name,
						   'last_name' => $last_name,
						   'email' => $email,);
			}	

			
		   	$result = $this->users_model->profile_change($id,$data);
			if ($result) {
				$msg = array('success' => true,'msg'=>'Profile updated!' );
				echo json_encode($msg);
			}
	}
}