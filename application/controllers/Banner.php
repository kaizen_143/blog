<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Banner extends CI_Controller {
	public function __construct()
		{
		parent::__construct();
		$this->config->load('pagination', TRUE);
		$this->load->library('pagination');
		$this->pager_settings = $this->config->item('pager_settings', 'pagination');

		$this->load->model('banner_model');

		}
        public function check_loggedin(){
            if (!$this->session->logged_in) {
                redirect('/');
            }
                return;
        }   
	public function index()
	{
        $this->check_loggedin();
		$data['title'] = "Banner";
		$data['js'] = array('actions/banner.js');
		 $this->load->library('pagination');
            $this->pager_settings['base_url'] = base_url() . '/Banner/index/';
            $this->pager_settings['total_rows'] = $this->banner_model->getCount();


            if (count($_GET) > 0) {
                $this->pager_settings['suffix'] = '?' . http_build_query($_GET, '', "&");
                $this->pager_settings['first_url'] = $this->pager_settings['base_url'] . '?' . http_build_query($_GET);
            }
            $this->pagination->initialize($this->pager_settings);
            $data['pager'] = $this->pagination->create_links();
             $data['banner'] = $this->banner_model->get($this->pager_settings['per_page'], $this->uri->segment(3));
            $data['banners'] = $this->banner_model->get_for_site();
		$this->load->view('common/header');
		// $this->load->view('common/navigation');
		$this->load->view('banner/index', $data);
		$this->load->view('common/footer',$data);
	}	

	    public function delete_banner($id)
    {
         
        $this->banner_model->delete_banner($id);
    }

	    private function random_string($length)
    {

        $key = '';
        $keys = array_merge(range(0, 9), range('a', 'z'));

        for ($i = 0; $i < $length; $i++) {
            $key .= $keys[array_rand($keys)];
        }

        return $key;
    }

	  public function upload_image()
    {
         $this->check_loggedin();
        $upload_path = FCPATH . 'uploads/banners/';

        if (!file_exists($upload_path)) {
            mkdir($upload_path, 0777, true);
        }
        $cpt = count($_FILES['file']['name']);

        for ($i = 0; $i < $cpt; $i++) {
            $file_type = $_FILES["file"]["type"][$i];
            $file_name = $_FILES["file"]["name"][$i];
            $target_file = $upload_path . basename($file_name);
            $file_ext = pathinfo($target_file, PATHINFO_EXTENSION);
            $tmp_name = $_FILES["file"]["tmp_name"][$i];

            $rand_name = $this->random_string(30);
            $target_file = $upload_path . "{$rand_name}.{$file_ext}";
            if (in_array($file_ext, array('jpg', 'jpeg', 'png')) && in_array($file_type, array('image/jpeg', 'image/png'))) {
                if (@move_uploaded_file($tmp_name, $target_file)) {
                    $this->banner_model->save($file_name, "{$rand_name}.{$file_ext}");
                } else {
                    echo "Sorry, there was an error uploading your file.";
                }
            }
        }
    }


    public function display_banner($id)
    {
         $this->check_loggedin();
        $data = $_POST['check'];
        $this->banner_model->display_banner($id, $data);

    }

    public function update_order()
    {
         $this->check_loggedin();
        $data = $_POST;
        $this->banner_model->update_order($data);
    }

    public function display_order()
    {
         $this->check_loggedin();
        $data = $this->banner_model->display_order();        
        echo json_encode($data);
    }
}	