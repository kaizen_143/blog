<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Star extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->config->load('pagination', TRUE);
		$this->pager_settings = $this->config->item('pager_settings', 'pagination');	
		$this->load->model('star_model');
	}




	public function show_star_student($offset){
		// if (empty($offset)) {
		// 	$offset = 0;
		// }
		// if ($offset <=-1) {
		// 	$offset = 0;
		// }
		$data['js'] = array('actions/star.js');
		$star = "Student";
		$data['video'] = $this->star_model->show_video($offset,$star);	
		echo count($data['video']);		
		// if (count($video) >=1) {
		// 	$this->session->set_userdata(array('count_student_data'=>count($video)));
		// 	echo $this->session->count_student_data;
		// 	$data['video'] = $this->star_model->show_video($offset,$star);					
		// }

		// $this->load->view('common/header');
		$this->load->view('star/show_video',$data);
		// $this->load->view('common/footer',$data);

	}
	public function star_load($offset){
		$data['js'] = array('actions/star.js');
		$star = "";
		$data['video'] = $this->star_model->show_video($offset,$star);
		// $this->load->view('common/header');
		$this->load->view('star/show_video',$data);
		// $this->load->view('common/footer',$data);

	}
	public function add_parent(){
		$data['type'] = 'Parent';
		$data['js'] = array('actions/star.js');
		$this->load->view('common/header');
		$this->load->view('common/admin_nav');
		$this->load->view('star/star',$data);
		$this->load->view('common/footer',$data);		
		

	}

	public function add_student(){
		$data['type'] = 'Student';
		$data['js'] = array('actions/star.js');
		$this->load->view('common/header');
		$this->load->view('common/admin_nav');
		$this->load->view('star/star',$data);
		$this->load->view('common/footer',$data);		
		

	}

	public function add_school(){
		$data['type'] = 'School';
		$data['js'] = array('actions/star.js');
		$this->load->view('common/header');
		$this->load->view('common/admin_nav');
		$this->load->view('star/star',$data);
		$this->load->view('common/footer',$data);		
		

	}		
	public function logged_in(){
		if (!$this->session->userdata('logged_in')) {

			redirect('/');
		}
	}

	public function index(){
		$this->logged_in();
		$data['page_title'] = '明星学员';
		$type = "Student";
		$data['type'] = $type;
		$user_id = $this->session->userdata('id');
		$this->load->library('pagination');
		$this->pager_settings['base_url'] = base_url().'/star/index/';
		$this->pager_settings['total_rows'] = $this->star_model->count_info($type,$user_id);          
		$this->pagination->initialize($this->pager_settings);
		$data['pager']   = $this->pagination->create_links();
		$data['info'] = $this->star_model->show_star_data($type,$user_id,$this->pager_settings['per_page'], $this->uri->segment(3));
		$data['path_attach'] = $this->star_model->getFileThumb();


		$data['count_data'] = $this->star_model->count_data($type);
		$data['order_by'] = $this->star_model->getStarOrder();
		$data['js'] = array('actions/view_star.js');
		$this->load->view('common/header');
		$this->load->view('common/admin_nav');
		$this->load->view('star/show_star',$data);
		$this->load->view('common/footer',$data);
		
	}


	public function parent(){
		$this->logged_in();
		$type = "Parent";
		$data['page_title'] = '明星家长';
		$data['type'] = $type;
		$user_id = $this->session->userdata('id');
		$this->load->library('pagination');
		$this->pager_settings['base_url'] = base_url().'/star/parent/';
		$this->pager_settings['total_rows'] = $this->star_model->count_info($type,$user_id);

		$this->pagination->initialize($this->pager_settings);
		$data['pager']   = $this->pagination->create_links();
		$data['info'] = $this->star_model->show_star_data($type,$user_id,$this->pager_settings['per_page'], $this->uri->segment(3));
		$data['count_data'] = $this->star_model->count_data($type);
		$data['path_attach'] = $this->star_model->getFileThumb();
		$data['js'] = array('actions/view_star.js');
		$this->load->view('common/header');
		$this->load->view('common/admin_nav');
		$this->load->view('star/show_star',$data);
		$this->load->view('common/footer',$data);		
	}

	public function school(){
		$this->logged_in();
		$type = "School";
		$data['page_title'] = '明星分校';

		$data['type'] = $type;
		$user_id = $this->session->userdata('id');
		$this->load->library('pagination');
		$this->pager_settings['base_url'] = base_url().'/star/school/';
		$this->pager_settings['total_rows'] = $this->star_model->count_info($type,$user_id);

		$this->pagination->initialize($this->pager_settings);
		$data['pager']   = $this->pagination->create_links();
		$data['info'] = $this->star_model->show_star_data($type,$user_id,$this->pager_settings['per_page'], $this->uri->segment(3));
		$data['count_data'] = $this->star_model->count_data($type);
		$data['path_attach'] = $this->star_model->getFileThumb();
		$data['js'] = array('actions/view_star.js');
		$this->load->view('common/header');
		$this->load->view('common/admin_nav');
		$this->load->view('star/show_star',$data);
		$this->load->view('common/footer',$data);		
	}	


	public function update_order(){
		$id = $this->input->post('id');
		$order_by = $this->input->post('order_by');
		$data = array('order' => $order_by, );
		$result = $this->star_model->update_star($id,$data);
		if ($result) {
			$msg = array('success' => TRUE,'msg' => 'Order Updated' );
			echo json_encode($msg);
		}
	}

	public function update_publish_date(){
		$id = $this->input->post('id');
		$schedule_post_input = $this->input->post('schedule_post_input');
		$data = array('date_released' => $schedule_post_input, );
		$result = $this->star_model->update_star($id,$data);
		if ($result) {
			$msg = array('success' => TRUE,'msg' => 'Schedule post Updated' );
			echo json_encode($msg);
		}
	}	

	



	public function add_star(){
		$user_id = $this->session->userdata('id');

		$status = $this->input->post('status');	
		$star_link = $this->input->post('star_link');
		$find = array('https://','http://');
		$link = str_replace($find,"",$star_link);
		$star_link = 'http://'.$link;	
		$star_type = $this->input->post('star_type');	
		$desc = $this->input->post('star_desc');
		$st_title = $this->input->post('st_title');
		$st_author = $this->input->post('st_author');
		$date_today = gmdate('Y-m-d H:i:s', strtotime('now'));
		if ($status == 1) {
			$data = array(	'status' => $status,
				'user_id' => $user_id,
				'star_type' 	=> $star_type,					
				'star_link' 	=> $star_link,					
				'star_title' 	=> $st_title,					
				'star_author' 	=> $st_author,					
				'star_desc' 	=> $desc,						
				'date' => $date_today,
				'date_released'=> $date_today);
		}else{
			$data = array(	'status' => $status,
				'user_id' => $user_id,
				'star_type' 	=> $star_type,		
				'star_link' 	=> $star_link,				
				'star_desc' 	=> $desc,	
				'star_title' 	=> $st_title,					
				'star_author' 	=> $st_author,						
				'date' => $date_today,
				);
		}


		$last_id = $this->star_model->add_star($data);
		if(!empty($last_id)){
			$msg = array('success' => TRUE,'id'=>$last_id );
			echo json_encode($msg);
		}
		
	}	

	public function show_details(){
		$id = $this->input->post('id');
		$result = $this->star_model->show_details($id);
		$star_desc 		= $result->star_desc;
		$star_type 		= $result->star_type;
		$date_released  = $result->date_released;
		$star_title  = $result->star_title;
		$star_author  = $result->star_author;
		$star_link  = $result->star_link;
		$id  = $result->id;



		$msg = array('success' => TRUE,
			'id' => $id,
			'star_desc' =>$star_desc,
			'star_type' =>$star_type,
			'date_released' =>$date_released ,
			'star_link' =>$star_link ,
			'star_title' =>$star_title,
			'star_author' =>$star_author,
			'filepath'=> $result->filepath);	

		echo json_encode($msg);
	}

	public function update_star()
	{
		$star_desc = $this->input->post('star_desc');
		$id = $this->input->post('id');
		$star_link = $this->input->post('star_link');
		$star_link = str_replace(" ","",$star_link);
		$find = array('https://','http://');
		$link = str_replace($find,"",$star_link);
		$star_link = 'http://'.$link;	
		$type = $this->input->post('type');
		$date_today = gmdate('Y-m-d', strtotime('now'));

		$st_title = $this->input->post('st_title');
		$st_author = $this->input->post('st_author');
		
		$data = array(
			'star_desc' => $star_desc,
			'star_type' => $type,
			'date' => $date_today,
			'star_link' => $star_link,
			'star_title' => $st_title,
			'star_author' => $st_author,
			);

		$result = $this->star_model->update_star($id,$data);

		$msg = array(
			'success' => TRUE,
			'id'=> $id,
			'msg' => "Update Success!"
			);
		echo json_encode($msg);

	}

	public function schedul_post(){
		$id =$this->input->post('id');
		$date_post = $this->input->post('date_post');
		$data = array(
			'date_released' => $date_post,
			'status'=> 2
			);

		$result = $this->star_model->update_star($id,$data);
		if ($result) {
			$msg = array('success' => TRUE,'msg'=>"Schedule posted!" );
			echo json_encode($msg);
		}
	}

	public function update_status()
	{
		$id = $this->input->post('id');
		$status = $this->input->post('status');
		if($status == 1){
			$date_today = gmdate('Y-m-d', strtotime('now'));
		}if($status == 0){
			$date_today = "";
		}
		if ($status == 2) {
			$date_today = "0000-00-00";
		}
		$data = array('status' => $status,'date_released' => $date_today );
		$result = $this->star_model->update_star($id,$data);
		$msg = array('success' => TRUE,
			'msg' => 'Status Updated','date'=>$date_today,'id'=>$id,'status'=>$status);
		echo json_encode($msg);


	}

	public function upload_file($id){
		$array = explode('.', $_FILES['file']['name']);
		$date_today = gmdate('Y-m-d', strtotime('now'));
		$extension = end($array);
		$uploaddir =  FCPATH.'uploads/star/';
		$uploadfile = $uploaddir .$_FILES['file']['name'].$date_today."." .$extension;
		$uploadurl = $_FILES['file']['name'].$date_today."." .$extension;

		if (move_uploaded_file($_FILES['file']['tmp_name'], $uploadfile)) {
			$msg = array('id' => $id,'msg' => 'File successfully uploaded.', );
			echo json_encode($msg);

			$data = array('star_id' => $id,'filepath'=>$uploadurl);
			$this->star_model->attach_path($data);
		}else{
			echo "Something Went Wrong";
		}
	}



	public function delete_video(){
		$id = $this->input->post('id');
		$pathfile = $this->input->post('pathfile');
		$data = array('status' => 0, );
		$this->star_model->update_star($id,$data);
		$filepath = FCPATH.'uploads/star/'.$pathfile;
		
		$data = array('star_id' => $id, );
		$result = $this->star_model->delete_video($id,$data);
		if ($result) {
			unlink($filepath);
			$msg = array('success' => TRUE,'msg' => "Success!" ,'id' => $id);
			echo json_encode($msg);
		}
	}

	public function delete_post(){
		$id = $this->input->post('id');
		$pathfile = $this->input->post('filepath');
		$filepath = FCPATH.'uploads/star/'.$pathfile;
		
		$data1 = array('id' => $id, );
		$data2 = array('star_id' => $id, );
		$result = $this->star_model->delete_post($id,$data1,$data2);
		if ($result) {
			unlink($filepath);
			$msg = array('success' => TRUE,'msg' => "Post deleted." ,'id' => $id);
			echo json_encode($msg);
		}
	}





	
}
