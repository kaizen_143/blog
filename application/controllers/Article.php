<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Article extends CI_Controller {
		public function __construct()
			{
			parent::__construct();
			$this->config->load('pagination', TRUE);
			$this->load->library('pagination');
			$this->pager_settings = $this->config->item('pager_settings', 'pagination');
			$this->load->model('post_model');
			$this->load->model('article_model');

			}
		public function check_loggedin(){
			if (!$this->session->logged_in) {
			redirect('/');
			}
			return;
			}


		public function index()
			{


			$this->check_loggedin();
			$data['title'] =  "Download Maintenance";
			$user_id = $this->session->userdata('id');
			$data['js'] = array('actions/download.js');	
			$type = "article";
			$data['link'] = 'article/add_article';
			$this->load->library('pagination');
			$this->pager_settings['base_url'] = base_url().'article/index';
            $this->pager_settings['total_rows'] = $this->post_model->count_info($user_id,$type);
            $data['page_title'] = "美图美文";          
			$this->pagination->initialize($this->pager_settings);
			$data['pager']   = $this->pagination->create_links();
 			$data['download_center'] = $this->post_model->view_posts($type,$user_id,$this->pager_settings['per_page'], $this->uri->segment(3));	
 			$data['count_data'] = $this->post_model->count_data($type);			
		$this->load->view('common/header',$data);
		$this->load->view('common/admin_nav',$data);
		$this->load->view('download_center/index');
		$this->load->view('common/footer',$data);
			}

		public function add_article(){
			$this->check_loggedin();
			$data['js'] = array('actions/download.js');
			$data['type'] = 'article';
			$data['link'] = 'admin/article';
			$this->load->view('common/header');
			$this->load->view('common/admin_nav');
			$this->load->view('download_center/add_download',$data);
			$this->load->view('common/footer',$data);
			}

		public function add_new_article(){

			$this->check_loggedin();
			$title = $this->input->post('article_title');
			$status = $this->input->post('status');
			$sub_title = $this->input->post('sub_title');
			$link = $this->input->post('article_link');
			// $find = array('https://','http://');
			// $link = str_replace($find,"",$link);
			// $link = 'http://'.$link;
			$desc = $this->input->post('article_desc');
			$user_id = $this->session->userdata('id');
			$date_today = gmdate('Y-m-d', strtotime('now'));
			if ($status == 0) {
			$data = array(	'user_id' => $user_id,
			'blog_title' 	=> $title,
			'blog_author' 	=> $link,
			'blog_sub_title' 	=> $sub_title,
			'blog_desc' 	=> $desc,
			'type' 			=> 'article',
			'date' => $date_today);
			}
			if ($status == 1) {
			$data = array(	
			'status' => $status,
			'user_id' => $user_id,
			'blog_title' 	=> $title,
			'blog_author' 	=> $link,
			'blog_sub_title' 	=> $sub_title,			
			'blog_desc' 	=> $desc,
			'type' 			=> 'article',
			'date' => $date_today,
			'date_released' => $date_today);
			}

			$last_id = $this->post_model->add_post($data);
			if(!empty($last_id)){
			$msg = array('success' => TRUE,'id'=>$last_id );
			echo json_encode($msg);
			}
			

			}

		public function update_article(){
			$this->check_loggedin();
			$article_title = $this->input->post('article_title');
			$id = $this->input->post('id');
			$article_link = $this->input->post('article_link');
			$article_desc = $this->input->post('article_desc');
			$find = array('https://','http://');
			$article_link = str_replace($find,"",$article_link);
			$article_link = 'http://'.$article_link;
			$data = array('blog_title' => $article_title,
			'blog_link' => $article_link,
			'blog_desc' => $article_desc, );

			$result = $this->post_model->update($id,$data);
			if ($result) {
			$msg = array('success' => TRUE,'msg'=>"Post update successfully!" );
			echo json_encode($msg);
			}
			}

		public function edit_article()
			{
			$this->check_loggedin();
			$id = $this->input->post('id');
			$result = $this->article_model->ArticleInfo($id);
			echo json_encode($result);
			}

		public function upload_file($id,$type){
			$this->check_loggedin();
			if(count($_FILES['file']['name']) > 1){
			foreach ($_FILES['file']['name'] as $key => $value) {
			$file = $_FILES['file']['name'][$key];
			$extension = pathinfo($file,PATHINFO_EXTENSION);
			$filename = basename($file,'.'.$extension);			 
			$date_today = strtotime('now');
			$uploaddir =  FCPATH.'uploads/post/';
			$uploadfile = $uploaddir .$filename.$date_today."." .$extension;
			$uploadurl = $filename.$date_today."." .$extension;
			if (move_uploaded_file($_FILES['file']['tmp_name'][$key], $uploadfile)) {
			//echo "File successfully uploaded.\n";
			$data[] = array('blog_id' => $id,'path_file'=>$uploadurl,'type'=>$type, 'thumb'=> 1 );

			}else{
			//echo "Something WentWrong";
			}
			}
			}else{

			$file = is_array($_FILES['file']['name'])? $_FILES['file']['name'][0] :$_FILES['file']['name'];
			$extension = pathinfo($file,PATHINFO_EXTENSION);
			$filename = basename($file,'.'.$extension);		 
			$date_today = strtotime('now');
			$uploaddir =  FCPATH.'uploads/post/';
			$uploadfile = $uploaddir .$filename.$date_today."." .$extension;
			$uploadurl = $filename.$date_today."." .$extension;

			if (move_uploaded_file(is_array($_FILES['file']['tmp_name'])?$_FILES['file']['tmp_name'][0]:$_FILES['file']['tmp_name'], $uploadfile)) {
			//echo "File successfully uploaded.\n";


			$data[] = array('blog_id' => $id,'path_file'=>$uploadurl,'type'=>$type, 'thumb'=> 1 );

			}else{
			//echo "Something WentWrong";
			}
			}	
			if(!empty($this->post_model->attach_path($data))){
			echo json_encode(array("status"=>"success"));
			}

			} 

		public function change_thumb(){
			$this->check_loggedin();
			$id = $this->input->post('id');
			$result = $this->article_model->change_thumb($id);
			if ($result) {
			$msg = array('success' => TRUE,'msg' => 'please upload new thumbnails' );
			echo json_encode($msg);
			}
			}

		public function delete_article()
			{
				$this->check_loggedin();
				$id = $this->input->post('id');
				$result = $this->article_model->delete($id);
				if ($result) {
				$msg = array('success' => TRUE, 'msg' => 'Post Deleted', );
				echo json_encode($msg);
				}
			}

		public function change_thumbnails()
			{	$this->check_loggedin();
				$file_id = $this->input->post('file_id');
				$id = $this->input->post('id');
				$data = array('status' => 0, );
				$this->post_model->update($id,$data);
				$result = $this->post_model->change_thumbnails($file_id);

				$path = FCPATH . "uploads/post/".$result;

				$result = unlink($path);

				if ($result) {
				$msg = array('success' => TRUE,'msg' => 'File remove please select new post thumbnail.' );
				}
			}

		public function change_status(){
		$this->check_loggedin();
		$id = $this->input->post('id');
		$status = $this->input->post('status');
		if ($status == 0 ) {
			$message = "Post Save to daft.";
		}

		if ($status ==1) {
			$message = "Post published";
		}
		$date_today = gmdate('Y-m-d', strtotime('now'));
		$data = array('status' => $status, 'date_released'=> $date_today);
		$result = $this->post_model->update($id,$data);
		if ($result) {
			$msg = array('success' => TRUE,'msg'=>$message );
			echo json_encode($msg);
		}

	}	





}