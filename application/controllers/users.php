<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends CI_Controller {
public function __construct()
	{
		parent::__construct(); 

		$this->load->model('Users_model');
		$this->load->library('bcrypt');
	}

	public function index()
	{
		if ($this->session->logged_in) {

		$data['js'] = array('actions/users.js');
		$this->load->view('common/header');
		$this->load->view('users/user');
		$this->load->view('common/footer',$data);

		}else{
			redirect('auth');
		}
	}
	// public function add_user(){
	// 	$first_name = $this->input->post('first_name');
	// 	$last_name = $this->input->post('last_name');
	// 	$email = $this->input->post('email');
	// 	$user = $this->input->post('user');
	// 	$pass = $this->input->post('pass');
	// 	 $checkUser = $this->Users_model->get_users($user);
	// 	 if (count($checkUser) >=1) {
	// 	 	$msg = array('success' => FALSE,'msg' => 'Sorry, '.$user .' already exist!' );
	// 				echo json_encode($msg);
	// 	 }else{
	// 	$encrypt_pass = $this->bcrypt->hash_password($pass);
		
	// 	$data = array(
	// 					'first_name' 	=> $first_name,					
	// 					'last_name' 	=> $last_name,	
	// 					'email'  => $email,
	// 					'username' =>$user,
	// 					'password' => $encrypt_pass);
	// 		$user = $this->Users_model->add($data);

	// 		if ($user) {
	// 			$msg = array('success' => TRUE,'msg' => 'new users added' );
	// 				echo json_encode($msg);
	// 		}

	// 	 }


	// }

	

}