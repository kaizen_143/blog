<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Blog extends CI_Controller {
public function __construct()
	{
		parent::__construct();
		 $this->config->load('pagination', TRUE);
        $this->pager_settings = $this->config->item('pager_settings', 'pagination');	
		$this->load->model('blog_model');
	
	}

	public function index()
	{	
		$data['js'] = array('actions/blog.js');	
		$data['published'] = $this->blog_model->ViewListBlog();
		$data['banner'] = $this->blog_model->banner();
		
		$this->load->view('common/header');
		$this->load->view('common/navigation');
		$this->load->view('blog/blog',$data);
		$this->load->view('common/footer',$data);
		
	}

	public function microwebsite(){
		// $this->load->view('common/header');
		// $this->load->view('common/navigation');
		$this->load->view('post/micro');
	}

	public function get_post_data()
	{
		if (isset($_GET['offset']) && isset($_GET['limit'])) {
			$type = 'post';
			$offset = $this->input->get('offset');
			$limit 	= $this->input->get('limit');
			$post = $this->blog_model->get_post_data($type,$limit,$offset);
			if (!empty($post)) {
		foreach ($post as $row) {
				$title = strlen($row->blog_title);
				if ($title >=25) {
					$title = substr(substr($row->blog_title, 0,25),0,-4)." . . .";
				}else{
					$title = $row->blog_title;
				}
$path = base_url().'uploads/post/'.$row->path_file;
echo '<a href="'.base_url().'blog/post/'.$row->blog_id.'" target = "_blank">';
echo '<div id="new" class="post_data" style="margin-bottom: 10px;" >';
echo '<div id="post" style ="background:url('.$path.') no-repeat;background-size:cover; filter: grayscale(10%);height: 200px">';
echo '<div id="title_post" style="font-family:Microsoft Yahei;background:-webkit-linear-gradient(top, #222, rgba(0,0,0,0)); color: #fff;  font-size: 15px; width: 95%;white-space: nowrap; overflow: hidden; text-overflow: ellipsis;margin:0px">';
echo '<p style = "font-size: 20px; margin-bottom: -25px; margin-left: 10px;margin-top:5px ">'.$title .'</p><br>';
echo '<span style="font-size:13px;margin-left: 10px;">'.$row->date.'</span><br>';

echo '</div>';
echo '</div>';
echo '</div></a>';
			}				
			}else{
				// echo 'No Post Available';
			}
		}				
			
	}

		public function get_game_data()
	{
		if (isset($_GET['offset']) && isset($_GET['limit'])) {
			$type = 'game';
			$offset = $this->input->get('offset');
			$limit 	= $this->input->get('limit');
			$post = $this->blog_model->get_post_data($type,$limit,$offset);
			if (!empty($post)) {
		foreach ($post as $row) {
			$title = strlen($row->blog_title);
				if ($title >=40) {
					$title = substr(substr($row->blog_title, 0,40),0,-4)." . . .";
				}else{
					$title = $row->blog_title;
				}
$path = base_url().'uploads/game/'.$row->path_file;
echo '<a href="'.$row->blog_url.'" target = "_blank">';
echo '<div id="new" class="post_data" style="margin-bottom: 10px;" >';
echo '<div id="post" style ="background:url('.$path.') no-repeat;background-size:cover; filter: grayscale(10%);height: 200px">';
echo '<div id="title_post" style="font-family:Microsoft Yahei;background:-webkit-linear-gradient(top, #222, rgba(0,0,0,0)); color: #fff;  font-size: 15px; width: 95%;white-space: nowrap; overflow: hidden; text-overflow: ellipsis;margin:0px">';
echo '<p style = "font-size: 22px; margin-bottom: -25px;margin-left: 10px;margin-top:5px ">'.$title .'</p><br>';
echo '<span style="font-size:13px;margin-left: 10px;">'.$row->date.'</span><br>';
echo '</div>';
echo '</div>';
echo '</div></a>';
			}				
			}else{				
				
				// echo 'No Post Available';
			}
		}				
			
	}

	public function post($url)
	{
		if (empty($url)) {
			redirect('blog/');
			die();
		}
		$type = "post";
		$data['blog'] = $this->blog_model->viewArticle($url,$type);
		$this->load->view('common/header');
		$this->load->view('common/navigation');
		$this->load->view('blog/article',$data);
		$this->load->view('common/footer');


	}

		public function game_post($url)
	{
		if (empty($url)) {
			redirect('blog/');
			die();
		}
		$type = "game";
		$data['blog'] = $this->blog_model->viewArticle($url,$type);
		$this->load->view('common/header');
		$this->load->view('common/navigation');
		$this->load->view('blog/article',$data);
		$this->load->view('common/footer');


	}

		public function download($url)
	{
		if (empty($url)) {
			redirect('download_center/');
			die();
		}
		$type = "download";
		$data['blog'] = $this->blog_model->viewArticle($url,$type);
		$data['file_url'] = $this->blog_model->getDownloadFile($url);
		$this->load->view('common/header');
		$this->load->view('common/navigation');
		$this->load->view('blog/article',$data);
		$this->load->view('common/footer');


	}

	public function browse(){
		$this->load->view('common/header');
		$this->load->view('browse');
	}

	public function open_post()
	{
		$id = $this->input->post('id');	
		$result = $this->blog_model->viewBlog($id);	
		$title =  $result->blog_title;
		$desc = $result->blog_desc;
		$path = $result->path_file;
		$link = $result->blog_link;
		
		$msg = array('success'		=> TRUE,
					 'title' 		=> $title ,
					 'desc' 		=> $desc ,
					 'path' 		=> $path ,
					 'link' 		=> $link
					 );
		echo json_encode($msg);
	}

	public function game(){
		$data['js'] = array('actions/blog.js');	
		$type = "game";
		$q	  = $this->input->get('q');
		$key  = "";
		if (!empty($q)) {
			$key = array('blog_title' => $q );
		}			
		// $data['game_info'] = $this->blog_model->getpublishpost($key,$type);		
		$this->load->view('common/header');
		$this->load->view('common/navigation');
		$this->load->view('game/index',$data);
		$this->load->view('common/footer');
	}

	public function stars(){
		$star = $this->input->get('star');
		$offset = $this->input->get('q');

		if (empty($star)) {
			$star = 'Student';
			
		}

		$this->load->library('pagination');
		$this->pager_settings['base_url'] = base_url().'/blog/star/';
		$data['total_rows'] = $this->blog_model->count_student();
	
		// $this->pager_settings['per_page'] = 3;
		$this->pagination->initialize($this->pager_settings);

		$data['js'] = array('actions/star.js');	
		$data['pager']   = $this->pagination->create_links();	
		$data['video'] = $this->blog_model->show_star_student($star,$this->uri->segment(3));		
			// var_dump($data['pager']);
		$this->load->view('common/header');
		$this->load->view('common/navigation');		
		$this->load->view('star/video',$data);
		$this->load->view('common/footer',$data);


	}

		public function star(){


		$this->load->library('pagination');
		$this->pager_settings['base_url'] = base_url().'/blog/star/';
		$data['total_rows'] = $this->blog_model->count_student();
	
		// $this->pager_settings['per_page'] = 3;
		$this->pagination->initialize($this->pager_settings);

		$data['js'] = array('actions/blog.js','actions/star.js');	
		$data['pager']   = $this->pagination->create_links();	
		$data['student'] = $this->blog_model->getStarData(3,0,'Student');		
		$data['school'] = $this->blog_model->getStarData(3,0,'School');	
		$data['parent'] = $this->blog_model->getStarData(3,0,'Parent');		
			// var_dump($data['pager']);
		$this->load->view('common/header');
		$this->load->view('common/navigation');		
		$this->load->view('star/video',$data);
		$this->load->view('common/footer',$data);


	}



	public function download_center(){			
		$data['js'] = array('actions/blog.js');
		$type = "download";
		$data['download'] = $this->blog_model->getDownload(8,0);
		$data['article'] = $this->blog_model->getArticle(8,0);
		$data['study'] = $this->blog_model->getStudy(8,0);
		
		$this->load->view('common/header');
		$this->load->view('common/navigation');
		$this->load->view('post/view_post_dl',$data);
		$this->load->view('common/footer',$data);
	}




	public function getStarView(){
		$re = "";
		$offset = $this->input->get('offset');
		$limit = $this->input->get('limit');
		$page_view = $this->input->get('page_view');
		if ($page_view == 1 ) {
			$type = 'study';
		}
		if ($page_view == 2 ) {
			$type = 'article';
		}
		if ($page_view == 3 ) {
			$type = 'download';
		}				
		$result = $this->blog_model->getAjaxDownload($type,$limit,$offset);
		// echo $type;
		// echo json_encode($result);
		// echo var_dump($result);
		if (!empty($result)) {
			foreach ($result as $row) {

		echo  '<div id="view_infos" style = "height:100px;"><a href="'.base_url().'blog/download/'.$row->id .'">';		
		echo '<div class="col-xs-4"><img src="'. base_url().'uploads/post/'.$row->thumbnail.'" style="width:100px"></div>';
		echo '<div class="col-xs-5">'.$row->blog_title.'<br> '. $row->blog_sub_title.'</div></a>';
		echo '<div class="col-xs-3">';
		echo '<a href="'. base_url().'uploads/post/'.$row->path_file .'" download><button class="view_download_files" data-id = "'.$row->id.'" style="background:transparent;border:none;color:#77e3bc"><i class="fa fa-download fa-4x"></i></button></a>';
		echo '</div>';
		echo  '</div>';
}
		}else{

			// echo '<script> var flag_download = 0;</script>';
		}

	}

	public function getStarViewAjax(){
		$re = "";
		$student_count = 0;
		$perent_count = 0;
		$school_count = 0;
		$offset = $this->input->get('offset');
		$limit = $this->input->get('limit');
		$page_view_star = $this->input->get('page_view_star');
		if ($page_view_star == 1 ) {
			$type = 'Student';
			$star_count_type = "student_count";
		}
		if ($page_view_star == 2 ) {
			$type = 'Parent';
			$star_count_type = "perent_count";
		}
		if ($page_view_star == 3 ) {
			$type = 'School';
			$star_count_type = "school_count";
		}					
		$data['result'] = $this->blog_model->getStarData($limit,$offset,$type);


	}

	public function load_student($offset,$type)
	{
		$re = "";
		$student_count = 0;
		$perent_count = 0;
		$school_count = 0;

		
		// $offset = $this->input->get('offset');
		$limit = 3;
		$page_view_star = $this->input->get('page_view_star');
		if ($page_view_star == 1 ) {
			$type = 'Student';
			$star_count_type = "student_count";
		}
		if ($page_view_star == 2 ) {
			$type = 'Parent';
			$star_count_type = "perent_count";
		}
		if ($page_view_star == 3 ) {
			$type = 'School';
			$star_count_type = "school_count";
		}				
		// $result = $this->blog_model->getStarData($limit,$offset,$type);
		$data['result'] = $this->blog_model->getStarData($limit,$offset,$type);
		if(count($data['result']) < 3){
			$offset = 0;
			if(empty($data['result'])){
				$offset = 0;
				$data['result'] = $this->blog_model->getStarData($limit,$offset,$type);	
			}
			$data['sample'] = 1;
		}else{
			$data['sample'] = count($data['result']);
		}
		
		$data['count'] = ($offset>0)?$offset:0;


		$this->load->view('star/star_student',$data);

	}

		public function load_parent($offset,$type)
	{
				$re = "";
		$student_count = 0;
		$perent_count = 0;
		$school_count = 0;

		
		// $offset = $this->input->get('offset');
		$limit = 3;
		$page_view_star = $this->input->get('page_view_star');
		if ($page_view_star == 1 ) {
			$type = 'Student';
			$star_count_type = "student_count";
		}
		if ($page_view_star == 2 ) {
			$type = 'Parent';
			$star_count_type = "perent_count";
		}
		if ($page_view_star == 3 ) {
			$type = 'School';
			$star_count_type = "school_count";
		}				
		// $result = $this->blog_model->getStarData($limit,$offset,$type);
		$data['result'] = $this->blog_model->getStarData($limit,$offset,$type);
		if(count($data['result']) < 3){
			$offset = 0;
			if(empty($data['result'])){
				$offset = 0;
				$data['result'] = $this->blog_model->getStarData($limit,$offset,$type);	
			}
			$data['sample'] = 1;
		}else{
			$data['sample'] = count($data['result']);
		}
		$data['count'] = ($offset>0)?$offset:0;


		$this->load->view('star/star_parent',$data);



	}

		public function load_school($offset,$type)
	{
		$re = "";
		$student_count = 0;
		$perent_count = 0;
		$school_count = 0;

		
		// $offset = $this->input->get('offset');
		$limit = 3;
		$page_view_star = $this->input->get('page_view_star');
		if ($page_view_star == 1 ) {
			$type = 'Student';
			$star_count_type = "student_count";
		}
		if ($page_view_star == 2 ) {
			$type = 'Parent';
			$star_count_type = "perent_count";
		}
		if ($page_view_star == 3 ) {
			$type = 'School';
			$star_count_type = "school_count";
		}				
		// $result = $this->blog_model->getStarData($limit,$offset,$type);
		$data['result'] = $this->blog_model->getStarData($limit,$offset,$type);
		if(count($data['result']) < 3){
			$offset = 0;
			if(empty($data['result'])){
				$offset = 0;
				$data['result'] = $this->blog_model->getStarData($limit,$offset,$type);	
			}
			$data['sample'] = 1;
		}else{
			$data['sample'] = count($data['result']);
		}
		$data['count'] = ($offset>0)?$offset:0;


		$this->load->view('star/star_school',$data);



	}

	public function article(){			
		$data['js'] = array('actions/blog.js');
		$type = "article";
		$q	  = $this->input->get('q');
		$key  = "";
		if (!empty($q)) {
			$key = array('blog_title' => $q );
		}			
		$data['post'] = $this->blog_model->getpublishpost($key,$type);
		$this->load->view('common/header');
		$this->load->view('common/navigation');
		$this->load->view('article/view_article',$data);
		$this->load->view('common/footer',$data);
	}

	public function study(){			
		$data['js'] = array('actions/blog.js');
		$type = "study";
				$q	  = $this->input->get('q');
		$key  = "";
		if (!empty($q)) {
			$key = array('blog_title' => $q );
		}
		$data['post'] = $this->blog_model->getpublishpost($key,$type);
		$this->load->view('common/header');
		$this->load->view('common/navigation');
		$this->load->view('study/view_study',$data);
		$this->load->view('common/footer',$data);
	}	

	public function pagenotfound(){
		$this->load->view('errors/notfound');
	}


		public function getDownloadView(){
		$re = "";
		$offset = $this->input->get('offset');
		$limit = $this->input->get('limit');
		$page_view = $this->input->get('page_view');
		if ($page_view == 1 ) {
			$type = 'study';
		}
		if ($page_view == 2 ) {
			$type = 'article';
		}
		if ($page_view == 3 ) {
			$type = 'download';
		}				
		$result = $this->blog_model->getAjaxDownload($type,$limit,$offset);

		// echo $type;
		// echo json_encode($result);
		// echo var_dump($result);
		if (!empty($result)) {
			foreach ($result as $row) {
				$title = strlen($row->blog_title);
				$sub_title = strlen($row->blog_sub_title);
				if ($title >=25) {
					$title = substr(substr($row->blog_title, 0,25),0,-4)." . . .";
				}else{
					$title = $row->blog_title;
				}

				if ($sub_title >=25) {
					$sub_title = substr(substr($row->blog_sub_title, 0,25),0,-4)." . . .";
				}else{
					$sub_title = $row->blog_sub_title;
				}				
		// echo  '<div id="view_infos" style = "height:100px;"><a href="'.base_url().'blog/download/'.$row->id .'">';
		// echo '<div class="col-xs-4"><img src="'. base_url().'uploads/post/'.$row->thumbnail.'" style="width:100px"></div>';
		// echo '<div class="col-xs-5">'. $row->blog_sub_title.'</div></a>';
		// echo '<div class="col-xs-3">';
		// echo '<a href="'. base_url().'uploads/post/'.$row->path_file .'" download><button class="view_download_files" data-id = "'.$row->id.'" style="background:transparent;border:none;color:#77e3bc"><i class="fa fa-download fa-4x"></i></button></a>';
		// echo '</div>';
		// echo  '</div>';


	echo '
	<style>
		#img_dl{
			width:80px;
}
		@media screen and (max-width:320px){
			#img_dl{
			width:70px;
		}
		}
	</style>
	<div id="view_infos" style = "height:100px">';
	
    echo '<a href="'.base_url().'blog/download/'.$row->id .'">';
    echo ' <div class="col-xs-3"><img src="'. base_url().'uploads/post/'.$row->thumbnail.'" style="margin-left:-10px" id="img_dl"></div> ';     
     echo ' <div class="col-xs-7"><span style="font-weight:bold">'. $title .'</span><br><span style="font-size:11px;color:#999">'. $sub_title.'</span></div> </a>';
    echo ' <div class="col-xs-2" style = "padding-top:19px;">';
    if (!empty($row->path_file)): 
      echo '  <a href="'. base_url().'uploads/post/'.$row->path_file.'" download><button class="view_download_files" data-id = "'. $row->id.'" style="background:transparent;border:none;color:#77e3bc"><i class="fa fa-download" style="font-size:25px"></i></button></a>';
      endif ;
      echo '</div></div>';
}
		}else{
			// echo '<p id = "no_post">No Post Available</p><script></script>';
		}
	}

}