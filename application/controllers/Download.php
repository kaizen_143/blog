<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Download extends CI_Controller {
public function __construct()
	{
		parent::__construct();
		$this->config->load('pagination', TRUE);
		$this->load->library('pagination');
        $this->pager_settings = $this->config->item('pager_settings', 'pagination');
		$this->load->model('post_model');
	} 

		public function check_loggedin(){
			if (!$this->session->logged_in) {
			redirect('/');
			}
			return;
			}	

	public function index()
	{
		$this->check_loggedin();
		$data['title'] =  "Download Maintenance";
			$user_id = $this->session->userdata('id');
			$data['js'] = array('actions/download.js');	
			$type = "download";
			$data['link'] = 'download/add_download';
			$this->load->library('pagination');
			$this->pager_settings['base_url'] = base_url().'download/index';
            $this->pager_settings['total_rows'] = $this->post_model->count_info($user_id,$type); $data['page_title']  = "下载中心";
			$this->pagination->initialize($this->pager_settings);
			$data['pager']   = $this->pagination->create_links(); 
 			$data['download_center'] = $this->post_model->view_posts($type,$user_id,$this->pager_settings['per_page'], $this->uri->segment(3));		
 			$data['count_data'] = $this->post_model->count_data($type);	
			$this->load->view('common/header',$data);
			$this->load->view('common/admin_nav',$data);
			$this->load->view('download_center/index',$data);
			$this->load->view('common/footer',$data);
	}

	public function add_download()
	{
		$this->check_loggedin();
		$data['title'] =  "Add New Download";
		$data['js'] = array('actions/download.js');	
		$data['type'] = 'download';
		$data['link'] = 'admin/download';
		$this->load->view('common/header',$data);
		$this->load->view('common/admin_nav');
		$this->load->view('download_center/add_download',$data);
		$this->load->view('common/footer',$data);
	}



	public function upload_file($id,$type){
		$this->check_loggedin();
	if(count($_FILES['file']['name']) > 1){
		foreach ($_FILES['file']['name'] as $key => $value) {
			$file = $_FILES['file']['name'][$key];
			$extension = pathinfo($file,PATHINFO_EXTENSION);
			$filename = basename($file,'.'.$extension);			 
			$date_today = strtotime('now');
			$uploaddir =  FCPATH.'uploads/post/';
			$uploadfile = $uploaddir .$filename.$date_today."." .$extension;
			$uploadurl = $filename.$date_today."." .$extension;
			if (move_uploaded_file($_FILES['file']['tmp_name'][$key], $uploadfile)) {
		   	 //echo "File successfully uploaded.\n";
		   	 if ($type == 'video' OR $type == 'photo') {
		   	 	$thumb = 1;
		   	 }else{
		   	 	$thumb = 0;
		   	 }

		   	 $data[] = array('blog_id' => $id,'path_file'=>$uploadurl,'type'=>$type, 'thumb'=> $thumb );
		   	 
			}else{
				//echo "Something WentWrong";
			}
		}
	}else{

			$file = is_array($_FILES['file']['name'])? $_FILES['file']['name'][0] :$_FILES['file']['name'];
			$extension = pathinfo($file,PATHINFO_EXTENSION);
			$filename = basename($file,'.'.$extension);		 
			$date_today = strtotime('now');
			$uploaddir =  FCPATH.'uploads/post/';
			$uploadfile = $uploaddir .$filename.$date_today."." .$extension;
			$uploadurl = $filename.$date_today."." .$extension;

			if (move_uploaded_file(is_array($_FILES['file']['tmp_name'])?$_FILES['file']['tmp_name'][0]:$_FILES['file']['tmp_name'], $uploadfile)) {
		   	 //echo "File successfully uploaded.\n";
		   	 if ($type == 'video' OR $type == 'photo') {
		   	 	$thumb = 1;
		   	 }else{
		   	 	$thumb = 0;
		   	 }

		   	 $data[] = array('blog_id' => $id,'path_file'=>$uploadurl,'type'=>$type, 'thumb'=> $thumb );
		   	 
			}else{
				//echo "Something WentWrong";
			}
	}	
			if(!empty($this->post_model->attach_path($data))){
				echo json_encode(array("status"=>"success"));
			}
			
			

		} 




    	  public function upload_thumbnail($id)
    {
        $upload_path = FCPATH . 'uploads/post/';

        if (!file_exists($upload_path)) {
            mkdir($upload_path, 0777, true);
        }
        $cpt = count($_FILES['file']['name']);

        for ($i = 0; $i < $cpt; $i++) {
            $file_type = $_FILES["file"]["type"][$i];
            $file_name = $_FILES["file"]["name"][$i];
            $target_file = $upload_path . basename($file_name);
            $file_ext = pathinfo($target_file, PATHINFO_EXTENSION);
            $tmp_name = $_FILES["file"]["tmp_name"][$i];

            $rand_name = $this->random_string(30);
            $target_file = $upload_path . "{$rand_name}.{$file_ext}";
            if (in_array($file_ext, array('jpg', 'jpeg', 'png')) && in_array($file_type, array('image/jpeg', 'image/png'))) {
                if (@move_uploaded_file($tmp_name, $target_file)) {
               
                    $data = array('thumbnail' => "{$rand_name}.{$file_ext}", );
                   $this->post_model->update($id,$data);
                } else {
                    echo "Sorry, there was an error uploading your file.";
                }
            }
        }
    }

     private function random_string($length)
    {
        $key = '';
        $keys = array_merge(range(0, 9), range('a', 'z'));

        for ($i = 0; $i < $length; $i++) {
            $key .= $keys[array_rand($keys)];
        }

        return $key;
    }

		public function add_new_download(){
			$this->check_loggedin();
		
		$title = $this->input->post('t_post');
		$status = $this->input->post('status');
		$post_type = $this->input->post('post_type');
		$sub_title = $this->input->post('sub_title');
		$blog_author = $this->input->post('link_post');		
		$desc = $this->input->post('desc');
		$desc = str_replace(base_url(),"../../",$desc);	
		$user_id = $this->session->userdata('id');
		$date_today = gmdate('Y-m-d', strtotime('now'));
		if ($status == 0) {
			$data = array(	'user_id' => $user_id,
				'blog_title' 	=> $title,
				'blog_author' 	=> $blog_author,
				'blog_sub_title' 	=> $sub_title,
				'blog_desc' 	=> $desc,
				'type' 			=> $post_type,
				'date' => $date_today);
				}
		if ($status == 1) {
			$data = array(	
				'status' => $status,
				'user_id' => $user_id,
				'blog_title' 	=> $title,
				'blog_author' 	=> $blog_author,
				'blog_sub_title' 	=> $sub_title,
				'blog_desc' 	=> $desc,
				'type' 			=> $post_type,
				'date' => $date_today,
				'date_released' => $date_today);
		}

		$last_id = $this->post_model->add_post($data);
		if(!empty($last_id)){
			$msg = array('success' => TRUE,'id'=>$last_id );
			echo json_encode($msg);
		}
				
	}

public function change_thumbnails()
	{	$this->check_loggedin();
		$file_id = $this->input->post('file_id');
		$id = $this->input->post('id');
		$data = array('status' => 0, 'thumbnail'=>"");
		$path_file = $this->post_model->change_thumbnails_download($id);
		$result = $this->post_model->update($id,$data);		

		$path = FCPATH . "uploads/post/".$path_file;		

		if ($result) {
			unlink($path);
		$msg = array('success' => TRUE,'msg' => 'File remove please select new post thumbnail.' );
		}
	}	

	public function download_view()
	{
		$this->load->view('download_center/view_download');

	}

}