<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Microwebsite extends CI_Controller {
		public function __construct()
			{
			parent::__construct();
			$this->config->load('pagination', TRUE);
			$this->load->library('pagination');
			$this->pager_settings = $this->config->item('pager_settings', 'pagination');
			$this->load->model('post_model');
			$this->load->model('article_model');

			}
		public function check_loggedin(){
			if (!$this->session->logged_in) {
			redirect('/');
			}
			return;
			}


		public function index()
			{

		$this->load->view('post/micro');
			}

	

}