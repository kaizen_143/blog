<?php
defined('BASEPATH') or exit('No direct script access allowed foo!');


if (! function_exists('js'))
{
	function js($script = NULL)
	{
		if (isset($script)) {
			if (is_array($script)) {
				foreach ($script as $s) {
					echo '<script type="text/javascript" src="' . base_url('assets/js/' . $s) . '"> </script>';
				}
			}
			else{
				echo '<script type="text/javascript" src="' . base_url('assets/js/' . $script) . '"> </script>';
			}
		}
	}
}

if (! function_exists('css')) 
{
	function css($style = NULL)
	{
		if (isset($style)) {
			if (is_array($style)) {
				foreach ($style as $s) {
					echo '<link rel="stylesheet" type="text/css" href="' . base_url('assets/css/' . $s) . '">';
				}
			}
			else{
				echo '<link rel="stylesheet" type="text/css" href="' . base_url('assets/css/' . $s) . '">';
			}
		}
	}	
}