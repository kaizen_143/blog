<div class="container">
<h3>study List</h3>
<a class="btn btn-primary" href="<?php echo base_url('study/add_study')?>">Add New Post</a>
<a class="btn btn-primary" href="<?php echo base_url('blog/study')?>">View Post </a>
<table class="table">
	<thead >
		<th class="text-center" >ID</th>
		<th class="text-center" >Title</th>
		<th class="text-center" >Content</th>
		<th class="text-center" >Date Save</th>
		<th class="text-center" >Status</th>
		<th class="text-center" >Publish Date</th>
		<th class="text-center" >Action</th>
	</thead>
	<tbody>
	<?php if (!empty($study)): ?>	

	<?php foreach ($study as $row): ?>	
	
		<tr data-tr-id = "<?php echo $row->id?>"  >
			<td class="text-center" ><?php echo $row->id?></td>
			<td class="text-center" ><?php echo $row->blog_title?></td>
			<td class="text-center" > <button data-study-id = "<?php echo $row->id?>" class = "btn_blue form-control view_details_study" >View Details</button> </td> 
			<td class="text-center" ><?php echo $row->date?></td>
			<td class="text-center" >
				<?php 
				if ($row->status == 1) {
					$status = "Published";
				}
				if ($row->status == 0 ) {
					$status = "Draft";
				}
				echo $status;
				 ?>
			</td>
			<td class="text-center" >
				<?php $status = $row->status;
				if ($status == 1) {
					$date_publish = $row->date_released;
				}
				if ($status == 0 ) {
					$date_publish =$row->date_released;
				}
				echo $date_publish;
				 ?>
			</td>
			<td class="text-center" ><button id = "" class="btn_red study_delete" data-study-id = "<?php echo $row->id?>"  ><i class="fa fa-trash"></i> </button></td>
			</tr>
<?php endforeach ?>
	<?php endif ?>
	</tbody>
</table>
<div>
	<?php echo $pager; ?>
</div>
</div>



<div id="study_edit_modal" class="modal fade" role="dialog">
  <div class="modal-dialog ">
   
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Page Post: <span id = "modal_status"></span></h4><input type="hidden" value = "" id = "study_id_post">
        <input type="hidden" id = "thumb_check">
      </div>
      <div class="modal-body">
     
<div id="star_register">
		
		Status <br>
		<select name="" id="status_study" class="form-control">
			<option value="">Change Status</option>
			<option value="1" >Publish Now</option>
			<option value="0">Draft</option>
		</select>
<br>

Title <br>
<input type="text" id = "study_title" class="form-control"><br>
Link <br>
<input type="text" id = "study_link" class="form-control"><br>
<div id="thumb_change" class="text-center" style = "display:none">
Are you sure you want to change the thumbnails <br>
	
	<button class="btn_red" data-study-id = "" id = "yes_change_thumb">Yes</button>
	<button class="btn_green " >No</button>
	
</div> 
<div id="thumb_view" style="display:none">
	<button class="btn_blue" id = "chang_the_thumb">Change Thumbnail</button>
	<img src="" id = "thum_prev" alt="thumbnails" style="width:100% !important">
</div>

<div id="thumb_image_modal" style = "display:none">
		
		Thumbnail Image:<br>
		 <form action="<?php echo base_url('study/upload_file');?>" class="dropzone" id = "study_dropzone">
              <div class="fallback">
                <input name="file" type="file" accept="image/*" id = "file_video" />
              </div>
       </form >
       <div class="text-center">
	<button class="btn_blue" id = "upload_attachment_new" data-study-ids = ""><i class="fa fa-upload"></i> Upload New ThumbNail</button>
	</div>
       <br><br>
</div>
		<textarea placeholder="Details" id = "study_desc"></textarea>
		<input type="submit" value="Update" id="update_study">
		</div>

      </div>
      <div class="modal-footer">

        <button type="button" class="btn btn_green" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>


<div id="study_delete_modal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-sm">
   
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Delete study</h4>
      </div>
      <div class="modal-body">
       <p>Are you sure you want to delete this?</p>
      </div>
      <div class="modal-footer">
      <button type="button" class="btn btn_red" id = "yes_delete"  data-study-modal-id = ""><i class="fa fa-trash"></i> Delete</button>
        <button type="button" class="btn btn_green" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

<script type="text/javascript">Dropzone.autoDiscover = false;

			CKEDITOR.replace('study_desc');
</script>