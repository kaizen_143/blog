	<div class="container">
	<div id="register">
		<a class="btn_blue" href="<?php echo base_url('study/')?>"><i class="fa fa-arrow-left"></i> Back</a>
		<div id="title_tag " class="text-center">Add New Study</div><br><br>

		Post status:<br>
		<select name="" id="status">			
			<option value="1">Publish Now</option>
			<option value="0">Draft</option>
		</select>
		<input type="text" placeholder="Title" id = "t_post"  ><br>		
		<input type="text" placeholder="Link" id = "link_post" >
	
		<p>Attach Thumbnail:</p>
			<form action="<?php echo base_url('post/upload');?>" class="dropzone" id = "study_dropzone">
			<div class="fallback">
			<input name="file" type="file" accept="image/*" />
			</div>
	       </form ><br><br>
		<textarea placeholder="Details" id = "study_desc" required></textarea>
		<input type="submit" value="Post" id = "submit_study" >
		<!-- <input type="reset" value="Clear All" id = "clear_post"> -->
	
	</div>
	</div>

	<script>
	Dropzone.autoDiscover = false;
	CKEDITOR.replace('study_desc');

	
	</script>


