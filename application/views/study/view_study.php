

	<div id="main_2">
<?php if (!empty($post)): ?>
	<?php foreach ($post as $row):
		$path = base_url().'uploads/study/'.$row->path_file;
		
		if (empty($row->profile_pic)) {
					$profile_pic = 'user_icon.png';
		}else{
			$profile_pic = $row->profile_pic;
		}?>
			    <a href="<?php echo $row->blog_link?>" target = "_blank">
			<div id="new" class="post_data" >			
				<div id="post" style = "background:url('<?php echo $path;?>') no-repeat;background-size:cover">			
				<div id="title_post" style=" color: #fff;  font-size: 15px; width: 70%;white-space: nowrap; overflow: hidden; text-overflow: ellipsis;">
				<?php echo $row->blog_title ?><br>
				<span style="font-size:15px"><?php echo $row->date ?></span><br>
				<i  data-post ="<?php echo $row->blog_id?>" data-title = "<?php echo $row->blog_title?>" class = " study_desc" >View Description</i>
				</div>

				<img src="<?php echo base_url().'profile_pic/'.$profile_pic?>">
				</div>	
						
			</div></a><br><br>
			
	<?php endforeach ?>	
<?php endif ?>

	</div>



<div id="study_desc_modal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" id = "study_title_modal"></h4>
      </div>
      <div class="modal-body">
	<p id = "body_study_desc"></p>
	     
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
	
