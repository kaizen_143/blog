

<div class="col-md-10 pull-right">
	<div id="star_register">
		<input type="hidden" value="<?php echo $type ?>" id = "type_star">
				<br>		<br>
		<div class="col-md-8">
			<div class="input-group">
				<span class="input-group-addon" id="basic-addon1">Title:</span>
				<input type="text" class="form-control" id="st_title" aria-describedby="basic-addon1">
			</div>
		<br>
			<div class="input-group">
				<span class="input-group-addon" id="basic-addon1">Author:</span>
				<input type="text" class="form-control" id="st_author" aria-describedby="basic-addon1">
			</div>

			<label for="">Description:</label>
			<textarea placeholder="Details" id = "star_desc"></textarea> 
		</div>
		<div class="col-md-4">
			<div class="input-group">
				<span class="input-group-addon" id="basic-addon1">Post Status:</span>
				<select name="" id="status" class="form-control">
					<option value="1" >Publish Now</option>
					<option value="0">Draft</option>
				</select>
			</div>
		<br>
			<div class="input-group">
				<span class="input-group-addon" id="basic-addon1">Video Link:</span>
				<input type="text" class="form-control" id="star_link" aria-describedby="basic-addon1">
				
			</div>

			<h5>Thumbnail:</h5>
			<form action="<?php echo base_url('star/upload_file');?>" class="dropzone" id = "star_videos">
				<div class="fallback">
					<input name="file" type="file" accept="image/*" id = "file_video" />
				</div>
			</form >
			<br>
			<input type="submit" value="Post" id="submit_star">
		
		</div>

	</div>
</div>


<script type="text/javascript">Dropzone.autoDiscover = false;

	CKEDITOR.replace('star_desc',{
		filebrowserBrowseUrl : window.App.baseUrl+'/blog/browse/',
		filebrowserUploadUrl : window.App.baseUrl+'/post/upload_blog/',

		filebrowserWindowWidth : '640',
		filebrowserWindowHeight : '480',
		on: {
			instanceReady: function() {
				this.dataProcessor.htmlFilter.addRules({
					elements: {
						img: function( el ) {
							el.addClass( 'img-responsive' );
						}
					}
				});            
			}
		}
	});
</script>