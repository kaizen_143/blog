<div class="col-md-10 pull-right">

	<h3><?php echo $page_title ?></h3>
	<a href="<?php echo base_url('/star/add_').strtolower($type)?>" class="btn btn_blue"><i class="fa fa-plus"></i> Add</a>
	<a href="<?php echo base_url('/blog/star')?>" class="btn btn_blue"><i class="fa fa-eye"></i> Preview</a>
	<div class="table-responsive">
		<table class="table table-hover ">
			<thead>
				<tr>
					<th class="text-center">#</th>
					<!-- <th class="text-center">Type</th> -->
					<th class="text-center">Content</th>			
					<th class="text-center">Date Save</th>
					<th class="text-center">Status</th>
					<th class="text-center">Publish Date</th>
					<th class="text-center">Order</th>			
					<th class="text-center">Action</th>
				</tr>
			</thead>    
			<tbody>
				<?php 
				if(!empty($info)){

					foreach ($info as $row) { ?>
					<tr>
						<td class = "text-center"><?php echo $row->id; ?></td>			
						<!-- <td class = "text-center"><?php echo $row->star_type;?></td> -->
						<td class = "text-center"><?php //echo  $row->star_desc; ?>
							<button class="btn btn_blue btn-sm show_star form-control"  star-id = "<?php echo $row->id?>">View Details</button>
						</td>
						<td class = "text-center"><?php echo  $row->date;?></td>
						<td class = "text-center">
							<?php if($row->status == 1){
								$string = $row->date_released;
								?>

								Published

								<!-- <option value="2">Schedule Post</option> -->
								<?php }elseif($row->status == 0){
									$string = "Not yet Published"?>

									Draft
									<!-- <option value="2">Schedule Post</option> -->
									<?php }else{
										$string = $row->date_released;
										?>					
										Draft

										<?php }?>




									</td>
									<td class="text-center" publish-date = "<?php echo $row->id?>">		


										<?php if($row->status == 1){
											echo  $row->date_released;
										}
										if($row->status == 0){
											echo $row->date_released;}
											if($row->status == 2){
												echo  $row->date_released;
											}

											?>

										</td>
										<td class = "text-center">

											<select name="" class ="form-control order_by" data-id-order-by="<?php echo $row->id ?>" >
												<option value="<?php echo $row->order?>"><?php echo $row->order?></option>
												<?php  $x =1;
												while($x <= $count_data) {?>
												<option value="<?php echo $x?>"><?php echo $x?></option>
												<?php  $x++;
											} ?>

										</select>
									</td>
									<td class = "text-center">
										<button class='btn btn-danger btn_remove' data-path-delete = "<?php 
										if (!empty($path_attach[$row->id])) {
											echo $path_attach[$row->id];
										}?>" data-delete-id = "<?php echo $row->id?>"><i class='fa fa-trash'></i> </button>
									</td>
								</tr>
								<?php }} ?>
							</tbody>
						</table>
						<?php echo $pager;?>
					</div>
				</div>
				<div id="show_star" class="modal fade" role="dialog">
					<div class="modal-dialog">

						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal">&times;</button>
								<h4 class="modal-title" id = "star_title"></h4>
							</div>
							<div class="modal-body">
								<label for="">Status</label>
								<select name="" id="status_modal" class=" form-control"  publish-status = "">
									<option value="">Change Status</option>
									<option value="1">Published</option>
									<option value="0">Draft</option>

								</select>
								<label for="">Type</label>
								<select class="form-control" id = "view_star">
									<option value="" id = "star_type"></option>
									<option value="Student">Star Student</option>
									<option value="Parent">Star Parent</option>
									<option value="School">Star School</option>
								</select>

								<label for="">Title:</label>
								<input type="text" class="form-control" id="st_title" >


								<label for="">Author:</label>
								<input type="text" class="form-control" id="st_author" >

								
								

								<label for="">Video Link</label>
								<input type="text" class="form-control" id="vid_link">

								<label for="">Content:</label>
								<textarea id = "star_desc"></textarea><br>

								<!-- 	<img src="" id = "path" alt=""> -->
								<div class="row" id = "video_show">
									<div class="col-md-12">

										<button class="btn btn-primary" id = "change_video_modal"  data-path = "" data-id = "" ><i class="fa fa-camera"></i> Change Thumbnail</button>
									</div>

									<div class="col-md-12" style = "width:100%" >
										<img src="" id = "path" alt="" class="img-responsive">
		<!-- <video autoplay  id = "path" src="" type="video/mp4" style = "width:100%">
			<!-- <source id = "path" src="" type="video/mp4" >		
		</video> -->	

	</div>	

</div>

<form class="dropzone" action="upload" id = "add_video_star">			
</form>
<div class="row"><div class="col-md-12"><button class="btn btn-primary pull-right" id = "upload_new_video" video-id = ""><i class="fa fa-upload"></i>Upload</button></div>
</div>
</div>
<div class="modal-footer">
	<input type="hidden" id = "update_star_id" value="">

	<button type="button"  class="btn btn-success" id = "update_post"  ><i class="fa fa-edit"></i>Update</button>
	<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
</div>
</div>

</div>
</div>
<div id="schedule_post" class="modal fade" role="dialog">
	<div class="modal-dialog modal-sm">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Schedule Post</h4>
			</div>
			<div class="modal-body">

				<input type="text" class = "form-control " id = "schedule_post_input">
			</div>
			<div class="modal-footer">

				<button type="button" class="btn btn-default" id = "publish_date_modal" data-id-modal = "">Save</button>
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>

	</div>
</div>

<div id="delete_video_modal" class="modal fade" role="dialog">
	<div class="modal-dialog modal-sm">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Change Video Thumbnail</h4>
			</div>
			<div class="modal-body">

				<p>Are you sure you want to change the video thumbnail?</p>
			</div>
			<div class="modal-footer">
				<input type="hidden" id = "id_star_modal">
				<input type="hidden" id = "path_modal">
				<button type="button" class="btn btn-default" id = "yes_video_change">Yes</button>
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>

	</div>
</div>

<div id="delete_star_modal_post" class="modal fade" role="dialog">
	<div class="modal-dialog modal-sm">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Delete Post</h4>
			</div>
			<div class="modal-body">

				<p>Are you sure you want to remove this post?</p>
			</div>
			<div class="modal-footer">
				<input type="hidden" id = "id_delete_modal">
				<input type="text" id = "file_path_delete_post">

				<button type="button" class="btn btn-default" id = "yes_delete_star_modal">Yes</button>
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>

	</div>
</div>



<script>
	CKEDITOR.replace('star_desc',{
		filebrowserBrowseUrl : window.App.baseUrl+'/blog/browse/',
		filebrowserUploadUrl : window.App.baseUrl+'/post/upload_blog/',

		filebrowserWindowWidth : '640',
		filebrowserWindowHeight : '480',
		on: {
			instanceReady: function() {
				this.dataProcessor.htmlFilter.addRules({
					elements: {
						img: function( el ) {
							el.addClass( 'img-responsive' );
						}
					}
				});            
			}
		}
	});
</script>




