
</head>
<body>

	<div id="back_2">	
		<div id="exit">
			<span class="fa fa-close fa-lg	"></span>
		</div>
	</div>
	
	<video width="320" height="240" controls autoplay="autoplay">
		<source src="#" type="video/mp4">
		</video>

		<div id="wrap">	
			<div id="nav_ban">
				<div id="ban" style="background:#77d0ee">
					<br>
					<span style="font-size:25px">U明星</span>
				</div>

				<div id="nav">
					<table cellspacing="0" style="width:100% !important;border-radius:5px !important;background:#f1f1f1">
						<tr>
							<td><input type="submit" value="明星学员" style="font-weight:bold" class="active_nav" id="view_student">
								<td><input type="submit" value="明星家长" style="font-weight:bold" id="view_parent">
									<td><input type="submit" value="明星分校" style="font-weight:bold" id="view_school">
									</table>
								</div>
							</div>
							
							<input type="text" hidden id="page_view_star" value="1" >


							<!-- <button hidden id = "next_star_view" >Next</button> -->

							<div id="main_view">
								<div style="position:fixed;text-align:center;width:100%;z-index:11;margin-top:30px">
									
								</div>
								<?php 
								$student_count = 0;
								$perent_count = 0;
								$school_count = 0; ?>
								<div id="main_star_view" class="stack">

								</div>

								<div id="main_parent_view" class="stack">

								</div>

								<div id="main_school_view" class="stack">


								</div>
								<input type="text" hidden id="school_count" value="<?php echo $school_count ?>" >
							</div>
						</div>
						<script src = "<?php echo base_url('assets/js/jquery.min.js') ?>"></script>
						<script>
							$("#student:nth-child(2),#parent:nth-child(2),#school:nth-child(2),#parent:nth-child(2),#school:nth-child(2)").delay(300).animate({margin:"50px 0px 0px 0px"});
							$("#student:nth-child(3),#parent:nth-child(3),#school:nth-child(3),#parent:nth-child(3),#school:nth-child(3)").delay(600).animate({margin:"100px 0px 0px 0px"});

							var x=1;
							$(".stack #student").click(function(){
								
								$("#student:nth-child("+ x +")").stop().css({"animation":"card_stack 0.5s","display":"none","opacity":"0"});
								$("#student:nth-child("+ x +") video").trigger("pause");
								x++;
								$("#student:nth-child("+ x +")").stop().css({"animation":"card_stack_next 0.5s","transform":"scale(1)"}).animate({margin:"0px"});
								var y= x + 1;
								$("#student:nth-child("+ y +")").stop().css({"animation":"card_stack_last 0.5s","transform":"scale(0.9)"}).animate({margin:"50px 0px 0px 0px"});
								var student_count =  parseInt($('#student_count').val())+1

								if(x==student_count){
									x=1;
									$("*#student").stop().css({"display":"block","opacity":"1"});
									$("#student:nth-child(2)").stop().css({"-webkit-transform":"scale(0.9)"}).delay(300).animate({margin:"50px 0px 0px 0px"});
									$("#student:nth-child(3)").stop().css({"-webkit-transform":"scale(0.8)"}).delay(600).animate({margin:"100px 0px 0px 0px"});
									$('#next_star_view').click();
								}
							});

							var a=1;
							$(".stack #parent").click(function(){ 		

								$("#parent:nth-child("+ a +")").stop().css({"animation":"card_stack 0.5s","display":"none","opacity":"0"});
								$("#parent:nth-child("+ a +") video").trigger("pause");
								a++;
								$("#parent:nth-child("+ a +")").stop().css({"animation":"card_stack_next 0.5s","transform":"scale(1)"}).animate({margin:"0px"});
								var y= a + 1;
								$("#parent:nth-child("+ y +")").stop().css({"animation":"card_stack_last 0.5s","transform":"scale(0.9)"}).animate({margin:"50px 0px 0px 0px"});
								var perent_count =  parseInt($('#perent_count').val())+1

								if(a==perent_count){
									a=1;
									
									$("*#parent").stop().css({"display":"block","opacity":"1"});
									$("#parent:nth-child(2)").stop().css({"-webkit-transform":"scale(0.9)"}).delay(300).animate({margin:"50px 0px 0px 0px"});
									$("#parent:nth-child(3)").stop().css({"-webkit-transform":"scale(0.8)"}).delay(600).animate({margin:"100px 0px 0px 0px"});
									$('#next_star_view').click();

								}
							});


							$("video").bind("ended",function(){

								$("#student:nth-child("+ x +"),#parent:nth-child("+ x +"),#school:nth-child("+ x +")").stop().css({"animation":"card_stack 0.5s","display":"none","opacity":"0"});
								$("#student:nth-child("+ x +"),#parent:nth-child("+ x +"),#school:nth-child("+ x +") video").trigger("pause");
								x++;
								$("#student:nth-child("+ x +"),#parent:nth-child("+ x +"),#school:nth-child("+ x +")").stop().css({"animation":"card_stack_next 0.5s","transform":"scale(1)"}).animate({margin:"0px"});
								var y= x + 1;
								$("#student:nth-child("+ y +"),#parent:nth-child("+ y +"),#school:nth-child("+ y +")").stop().css({"animation":"card_stack_last 0.5s","transform":"scale(0.9)"}).animate({margin:"50px 0px 0px 0px"});
								var count_div =  parseInt($('#count_slide').val())+1

								if(x==count_div){
									x=1;
									$("*#student").stop().css({"display":"block","opacity":"1"});
									$("#student:nth-child(2),#parent:nth-child(2),#school:nth-child(2)").stop().css({"-webkit-transform":"scale(0.9)"}).delay(300).animate({margin:"50px 0px 0px 0px"});
									$("#student:nth-child(3),#parent:nth-child(3),#school:nth-child(3)").stop().css({"-webkit-transform":"scale(0.8)"}).delay(600).animate({margin:"100px 0px 0px 0px"});
 				// $('#next_slide').click();

 			}
 		});


 		// video play 
 		$("video,#exit").hide();
 		$("*#video").click(function(){
 			var x= $(this).siblings(".video").val();

 			$("#back_2").fadeIn(function(){
 				$("video").fadeIn(function(){
 					$("#exit").show();
 				}).attr("src",x);
 			});
 		});

 		$("#exit,#back_2").click(function(){
 			$("#exit").hide();
 			$("video").attr("src","#").fadeOut(function(){
 				$("#back_2").fadeOut();
 			});
 		});

 		$('video').bind('webkitfullscreenchange mozfullscreenchange fullscreenchange', function(e) {
 			var state = document.fullScreen || document.mozFullScreen || document.webkitIsFullScreen;
 			var event = state ? 'FullscreenOn' : 'FullscreenOff';
 			if(event=="FullscreenOff"){
 				$("#exit").hide();
 				$("video").attr("src","#").fadeOut(10,function(){
 					$("#back_2").fadeOut(10);
 				});
 			}  
 		});
 		$("video").bind("ended",function(){
 			document.getElementsByTagName('video')[0].webkitExitFullscreen();
 			var state = document.fullScreen || document.mozFullScreen || document.webkitIsFullScreen;
 			var event = state ? 'FullscreenOn' : 'FullscreenOff';
 			if(event == "FullscreenOn"){
 				$("#exit").hide();
 				$("video").attr("src","#").fadeOut(10,function(){
 					$("#back_2").fadeOut(10);
 				});	
 			}	
 			else if(event == "FullscreenOff"){
 				$("#exit").hide();
 				$("video").attr("src","#").fadeOut(function(){
 					$("#back_2").fadeOut();
 				});	
 			}		
 		});
		// phone validation
		if(navigator.userAgent.match(/(iPod|iPhone|iPad)/)){
			$("video").bind("pause",function(){
				document.getElementsByTagName('video')[0].webkitExitFullscreen();
				var state = document.fullScreen || document.mozFullScreen || document.webkitIsFullScreen;
				var event = state ? 'FullscreenOn' : 'FullscreenOff';
				if(event == "FullscreenOn"){
					$("#exit").hide();
					$("video").attr("src","#").fadeOut(10,function(){
						$("#back_2").fadeOut(10);
					});	
				}	
				else if(event == "FullscreenOff"){
					$("#exit").hide();
					$("video").attr("src","#").fadeOut(function(){
						$("#back_2").fadeOut();
					});	
				}		
			});
		}</script>