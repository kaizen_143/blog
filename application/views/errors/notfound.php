<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Page Not Found</title>
	<style>
	.error_mes{
		
		width:30%;
		margin:auto;
	}
img{
		margin:auto;
		width:100%;
	}
	.text-center{
		    font-size: 40px;
    font-family: 'calibri';
    text-transform: uppercase;
    margin-top: 5px;
    text-align: center;
    color: #23527c;
	}

		</style>
</head>
<body>
	<div class="error_mes" >
      <img src="<?php echo base_url('images/404.png') ?>">
      <!-- <h1>ERROR 404</h1> -->
      <h3 class="text-center">
      <!-- <span>Error 404 |</span>  -->
      	  Page Not Found
      </h3>
    </div>
</body>
</html>