<div class="col-md-12">
<div class="row">
	<h2>Upload Banner</h2>
	
		  <form action="<?php echo base_url('post/upload');?>" class="dropzone" id = "banner">
	              <div class="fallback">
	                <input name="file" type="file" accept="image/*" />
	              </div>
	   </form >
</div>

<div class="row">
	<h2>Banner List</h2>
	<button class="btn btn-default pull-right order"  style = "margin-bottom: 10px;" >Banner Display Order</button>
<table class="table table-striped table-bordered" cellspacing="0" width="100%">
					<thead>
					<tr>
						<th class="text-center">ID</th>
						<th class="text-center" style="width:350px">Banner</th>
						<th class="text-center">Display</th>
						<th class="text-center">Date Added</th>
						<th class="text-center">Action</th>
					</tr>
					</thead>
					<tbody>
					
					<?php
					$count_active = 0;
					$disabled = '';
						$banner_flag = array_column($banner,'display');
						foreach ($banner_flag as $key) {
							if($key == 1)
								$count_active++;
						}


						
					 foreach ($banner as $key => $row) {
					 	if($count_active >= 3 && $row['display'] != 1){
					 		$disabled = 'disabled';
					 	}else{
					 		$disabled='';
					 	}
						?>
						<tr id="tr_c<?php echo $row['id'] ?>">
							<td class="text-center"><?php echo $row['id'];?></td>
							<td class="image text-center" >
									<a class="image-popup-no-margins" href="<?php echo base_url('uploads/banners/'.$row['file_name']);?>">
										<img src="<?php echo base_url('uploads/banners/'.$row['file_name']);?>" height="150">
									</a>
							</td>
							<td class="display text-center" style="vertical-align:middle">
								<span class='hidden'><?php echo $row['id'] ?></span>
								<input type="checkbox"  <?php echo $disabled; ?> class="display" <?php echo ($row['display'] == 1)?"checked='checked'":''; ?>/>
							</td>
							<td class="date_created text-center" style="vertical-align:middle"><?php echo $row['date_added'];?></td>
							<td class="text-center" style="vertical-align:middle">
								<span class='hidden'><?php echo $row['id'] ?></span>
								<button class='btn btn-default btn_remove' ><i class='fa fa-remove'></i></button>
							</td>
						</tr>
						<?php
					}
					?>
					</tbody>
				</table>
				<?php echo $pager?>


			
</div>	   
</div>


<div class="modal fade" id='modal_warning' tabindex="-1" role="dialog" aria-hidden="true" >
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title text-center">Warning</h4>
			</div>
			<div class="modal-body text-center" >

				<p>This banner will be permanently deleted. Continue?</p>

				<div class="clearfix"></div>
			</div>
			<div class="modal-footer">
				<div class="col-xs-6 padding0">
					<button type="button" id="btn_remove_confirm" class="btn btn-primary btn-sm col-xs-12 border_r_right_none" >Continue</button>
				</div>
				<div class="col-xs-6 padding0">
					<button type="button" class="btn btn-danger btn-sm col-xs-12 border_r_left_none" data-dismiss="modal">Cancel</button>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- Banner Order Modal -->
<div class="modal fade" id='order_modal' tabindex="-1" role="dialog" aria-hidden="true" >
	<div class="modal-dialog ">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title text-center">Banner Display Order</h4>
			</div>
			<div class="modal-body text-center order_display">
				<table class="table table-bordered">
					<tbody class="sortable">
	
					</tbody>
				</table>
			</div>
			<div class="modal-footer">
				<button type="button" id="save_order" class="btn btn-primary">Save</button>
				<button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
			</div>
		</div>
	</div>
</div>

<input id="get_did" class="hidden" type="hidden" />

<script>  Dropzone.autoDiscover = false;</script>