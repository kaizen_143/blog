<div class="col-md-10 pull-right">
	<div class="row">
		<h3><?php echo $page_title ?></h3>
		<a href="<?php echo base_url().$link ?>" class="btn_blue"><i class="fa fa-plus"></i> Add</a>
		<a href="<?php echo base_url('blog/download_center') ?>" class="btn_blue"><i class="fa fa-eye"></i> Preview</a>
	</div>
	<div class="row">
		<table class="table">
			<thead>
				<th class="text-center">ID</th>
				<!-- <th class="text-center">Title</th> -->
				<th class="text-center">Content</th>
				<th class="text-center">Date Save</th>
				<th class="text-center">Status</th>
				<th class="text-center">Publish Date</th>
				<th class="text-center">Order</th>
				<th class="text-center">Action</th>
			</thead>

			<tbody>
				<?php if (!empty($download_center)): ?>					
					<?php foreach ($download_center as $row): ?>
						<tr data-id = "<?php echo $row->id?>">
						<td class = "text-center"><?php echo $row->id ?></td>
						<!-- <td class = "text-center"><?php echo $row->blog_title ?></td> -->
						<td class = "text-center"><button data-download-id = "<?php echo $row->id ?>" class="view_post_details btn_blue form-control">Edit Post</button></td>
						<td class = "text-center"><?php echo $row->date ?></td>
						<td class = "text-center">
						<?php if ($row->status == 1) {
							$status = "Published";
							}
							if ($row->status==0) {
								$status = "Draft";
							}
							echo $status;

						 ?>
						 </td>
						<td class = "text-center"><?php echo $row->date_released ?></td>
						<td class = "text-center">
						<select name=""  class="order_by form-control">
							<?php if (!empty($row->order)){
								$order =$row->order;
								}else{
									$order = '0';
									}?>
							<option value="<?php echo $order ?>"><?php echo $order ?></option>
						<?php  $x =1;
						while($x <= $count_data) {?>
						<option value="<?php echo $x?>"><?php echo $x?></option>
						<?php  $x++;
					} ?>
							</select></td>
						<td class = "text-center"><button data-download-id = "<?php echo $row->id ?>" class="btn btn-danger open_confirm_delete_modal"><i class="fa fa-trash"></i></button></td>
						</tr>
					<?php endforeach ?>					
				
				<?php endif ?>
			</tbody>
		</table>
		<?php echo $pager?>
	</div>
</div>

 <div class="modal fade" id='confirm_delete_post' tabindex="-1" role="dialog" aria-hidden="true" >
	<form method="post" id="form-update" class="form-horizontal">
	  <div class="modal-dialog modal-sm">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        <h4 class="modal-title text-center" style="font-weight: bold;">Delete Post</h4>
	      </div>
	      <div class="modal-body">
					        <p>Are you sure you want to delete this post? all of the content will be remove.</p>
	      </div>
	      <div class="modal-footer">
	      <div class="btn-group ">
				<button type="submit" class="btn_green" data-dismiss="modal" id = "btn_delete_modal" data-id-delete = "">Yes</button>
				<button type="button" class="btn_red" data-dismiss="modal">Close</button>
		  </div>		
	      </div>
	    </div>
	  </div>
	</form>
</div>



<div id='modal_update' class="modal fade" role='dialog' tabindex='-1' aria-labelledby="myModalLabel">
  <form method="post" id="form-update" class="form-horizontal">
    <div class="modal-dialog modal-lg " role="document">
      <div class="modal-content">
        <div class="modal-header">
			<button type='button' class='close' data-dismiss='modal'><span aria-hidden='true'>&times; </span></button>
		<h4 class="modal-title"> Post Status: <span id = "post_status_title"></span></h4>  
        </div>
        <div class="modal-body">
		<div class="col-sm-12">
        	<div class="form-group ">
        	<select name="" id="post_status" class="form-control" data-id-status = "">
        		<option value="">Change Status</option>
        		<option value="0">Draft</option>
        		<option value="1">Publish Now</option>
        	</select>
        	<label for="">Post Title</label>
        	<input type="text" id = "post_title" class="form-control">
        		<label for="">Post Sub Title</label>
        		<input type="text" id = "post_sub_title" class="form-control">
        		<label for="">Author</label>        	
        		<input type="text" id = "post_link" class="form-control">
				<textarea name="" id="blog_desc" cols="30" rows="10"></textarea><br>

				<div class="text-center" id = "no_dropzone"  style = "display:none" >
					<button data-file-id = "" class="btn btn-primary" id = "upload_new_thumb"> <i class="fa fa-camera"></i> Upload New Post Thumbnails</button>
				</div>
 
				

					
				<div id="thumnail_edit">					
				</div><br>
		<div id="attach_thumb">
			<p>Attach thumbnail:</p>
			<div action="<?php echo base_url('post/upload');?>" class="dropzone" id = "thumb">
			<div class="fallback">
			<input name="file" type="file" accept="image/*" />
			</div>
	       </div >
	       <button class="btn btn-primary" id="thumb_upload" data-blog-id = "">Upload Thumb</button>
		</div>

			<div id="attach_section">
	 			<label for="">Attach File</label>
				<div action="<?php echo base_url('post/upload');?>" class="dropzone" id = "attach">
				<div class="fallback">
				<input name="file" type="file" accept="image/*" />
				</div>
		       </div >
		       <div class="text-center"><br>
				<button class="btn_green" id = "add_any_file" data-file-ids = "">Add files</button>	
				</div>
	       </div>				
				<!-- <label for="">Files: <span id="count"></span> </label> <br> -->
				<div id = "download_section"></div>
				
				<br>

			</div>
			</div>
        </div>
        <div class='modal-footer'>
			<!-- <input type="submit" class="btn btn-success update" name="update" value="Update">			 -->
			<button  class="btn_blue " id="update_post_modal" data-id-update-post = "" > Update</button>
			<button  class="btn_red  " data-dismiss='modal'> Cancel</button>
		
			
        </div>
      </div>
    </div>
  </form>
</div>

<script>
	Dropzone.autoDiscover = false;	
	CKEDITOR.replace('blog_desc',{
		filebrowserBrowseUrl : window.App.baseUrl+'/blog/browse/',
		filebrowserUploadUrl : window.App.baseUrl+'/post/upload_blog/',

		filebrowserWindowWidth : '640',
        filebrowserWindowHeight : '480',
        		    on: {
		        instanceReady: function() {
		            this.dataProcessor.htmlFilter.addRules({
		                elements: {
		                    img: function( el ) {
		                        el.addClass( 'img-responsive' );
		                    }
		                }
		            });            
		        }
		    }
	});
	</script>
