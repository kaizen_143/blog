

	<div class="col-md-10 pull-right">
		<div class="row">
		<h2 id="title_tag " class="text-center">Add Download Post</h2>
			<a class="btn_blue" href="<?php echo base_url().$link?>"><i class="fa fa-arrow-left"></i> Back</a>
		</div>

		<div class="col-md-8">
			<br>	
	<div class="input-group">
		<span class="input-group-addon" id="basic-addon1" >Title:</span>
		<input type="text" class="form-control" id = "t_post" aria-describedby="basic-addon1">
	</div>	
	<br>
	<div class="input-group">
		<span class="input-group-addon" id="basic-addon1">Sub Title:</span>
		<input type="text" class="form-control"  id = "sub_title" aria-describedby="basic-addon1">
	</div>	
<br>
	<div class="input-group">
		<span class="input-group-addon" id="basic-addon1">Author:</span>
		<input type="text" class="form-control" id = "link_post" aria-describedby="basic-addon1">
	</div>	
	<br>
	<textarea placeholder="Details" id = "article_desc" required></textarea>

		</div>
		<div class="col-md-4">
			<div class="input-group">
				<span class="input-group-addon" id="basic-addon1" >Post Status:</span>	
					<select name="" id="status" class="form-control">			
						<option value="1">Publish Now</option>
						<option value="0">Draft</option>
				</select>
			</div>	
			       <p>Attach thumbnail:</p>
			<form action="<?php echo base_url('post/upload');?>" class="dropzone" id = "thumb">
			<div class="fallback">
			<input name="file" type="file" accept="image/*" />
			</div>
	       </form >
<br>
			<p>Attachment:</p>
			<form action="<?php echo base_url('post/upload');?>" class="dropzone" id = "article_dropzone">
			<div class="fallback">
			<input name="file" type="file" accept="image/*" />
			</div>
	       </form >	


<br>
	       <input type="submit" value="Post" id = "submit_article" >		
		</div>


	
	</div>

	<script>
	Dropzone.autoDiscover = false;
CKEDITOR.replace('article_desc',{
		filebrowserBrowseUrl : window.App.baseUrl+'/blog/browse/',
		filebrowserUploadUrl : window.App.baseUrl+'/post/upload_blog/',

		filebrowserWindowWidth : '640',
        filebrowserWindowHeight : '480',
        		    on: {
		        instanceReady: function() {
		            this.dataProcessor.htmlFilter.addRules({
		                elements: {
		                    img: function( el ) {
		                        el.addClass( 'img-responsive' );
		                    }
		                }
		            });            
		        }
		    }
	});
	
	</script>





