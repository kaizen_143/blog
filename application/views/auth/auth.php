<!DOCTYPE html>
<html>
<head>
<title>Welcome to UTalk
</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" href="<?php echo base_url('assets/css/style.css')?>">

	<?php if (isset($css)): ?>
		<?php if (is_array($css)): ?>
			<?php foreach ($css as $stylesheet): ?>
				<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/' . $stylesheet); ?>">
			<?php endforeach; ?>
		<?php else: ?>
			<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/' . $css); ?>">
		<?php endif; ?>
	<?php endif; ?>
<script type="text/javascript">
		window.App = {
				"baseUrl" : "<?php echo base_url(); ?>",
				"removeDOM" : "",
		};
	</script>

<style>
body{
	padding:10px;
	background:url("<?php echo base_url('images/background.jpg')?>") no-repeat;
	background-size:cover;
	color:white;
	font-family:century gothic;
}
::-webkit-input-placeholder {
  color: white;
}
::-moz-input-placeholder {
  color: white;
}
::-o-input-placeholder {
  color: white;
}
::input-placeholder {
  color: white;
}
</style>
</head>
<body style="">
<div id="wrap">
	<center>
	<div id="head_logo">
	<img src="<?php echo base_url('images/logo_icon.png')?>" style="width:20%;margin-bottom:-15px" id="logo"><br>
	UTalk
	<div style="font-size:12px;margin-top:-5px;">
	<span style="border-top:1px solid white;padding:5px">MOBILE WEBSITE</span>
	</div>
	</div>
	</center>
	<div id="login_design" class="log">
		<input type="text"  id="username" placeholder="Username" id="username"><br>
		<input type="password" id="password" placeholder="Password" id="password"><br><br>
		<input type="submit" value="LOGIN" id = "login">
	</div>



	<center>
	<div id="upload_img">
	<label for="upload"><img src="<?php echo base_url('images/camera.png')?>" id="img_icon"></label>
<!-- 	<img src="<?php echo base_url('images/logo_icon.png')?>" style="width:20%;margin-bottom:-15px" id="logo"><br>
		UTalk
	<div style="font-size:12px;margin-top:-5px">
	<span style="border-top:1px solid white">TUTORIAL SERVICES</span>
	</div> -->
	</div>
	</center>
	<div id="login_design" class="signup">
		<input type="file" id="upload" accept="image/png" id="upload" style="display:none">
		<input type="text" placeholder="Firstname" id="first_name"><br>
		<input type="text" placeholder="Lastname" id="last_name"><br>
		<input type="email" placeholder="Email" id="email"><br>
		<input type="text" placeholder="Username" id="user"><br>
		<input type="password" placeholder="Password" id="pass"><br>
		<input type="password" placeholder="Confirm Password" id="con_pass" ><br><br>
		<input type="submit" value="SIGN UP" id="user_add">
	</div>

	<div id="login_foot">
	 &copy; UTalk Tutorial Services
	</div>
</div>
