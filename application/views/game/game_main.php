<div class="col-md-10 pull-right">
<h3>小游戏</h3>
<a class="btn btn_blue" href="<?php echo base_url('game/add_game')?>"><i class="fa fa-plus"></i> Add</a>
<a class="btn btn_blue" href="<?php echo base_url('blog')?>"><i class="fa fa-eye"></i> Preview</a>
<table class="table">
	<thead >
		<th class="text-center" >ID</th>
		
		<th class="text-center" >Content</th>
		<th class="text-center" >Date Save</th>
		<th class="text-center" >Status</th>		
		<th class="text-center" >Publish Date</th>
		<th class="text-center" >Order</th>
		<th class="text-center" >Action</th>
	</thead>
	<tbody>
	<?php if (!empty($game_info)): ?>
		

	<?php foreach ($game_info as $row): ?>	
	
		<tr data-tr-id = "<?php echo $row->id?>"  >
			<td class="text-center" ><?php echo $row->id?></td>
			
			<td class="text-center" > <button data-game-id = "<?php echo $row->id?>" class = "btn_blue form-control view_details_game" >View Details</button> </td> 
			<td class="text-center" ><?php echo $row->date?></td>
			<td class="text-center" >
				<?php 
				if ($row->status == 1) {
					$status = "Published";
				}
				if ($row->status == 0 ) {
					$status = "Draft";
				}
				echo $status;
				 ?>
			</td>
	
			<td class="text-center" >
				<?php $status = $row->status;
				if ($status == 1) {
					$date_publish = $row->date_released;
				}
				if ($status == 0 ) {
					$date_publish =$row->date_released;
				}
				echo $date_publish;
				 ?>
			</td>
									<td>

						<select name="" data-id = "<?php echo $row->id ?>" class="order_by form-control">
							<?php if (!empty($row->order)){
								$order =$row->order;
								}else{
									$order = '0';
									}?>
							<option value="<?php echo $order ?>"><?php echo $order ?></option>
							<?php 
								  $number_of_post = count($game_info);
								  for ($x = 1; $x<=$number_of_post; $x++) {
								  	echo "<option value='".$x."'>".$x."</option>";
								  }
							?>
							</select>
						</td>		
			<td class="text-center" ><button id = "" class="btn_red game_delete" data-game-id = "<?php echo $row->id?>"  ><i class="fa fa-trash"></i> </button></td>
			</tr>
<?php endforeach ?>
	<?php endif ?>
	</tbody>
</table>
<div>
	<?php echo $pager; ?>
</div>
</div>



<div id="game_edit_modal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg ">
   
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Page Post: <span id = "modal_status"></span></h4><input type="hidden" id = "game_id_post">
        <input type="hidden" id = "thumb_check">
      </div>
      <div class="modal-body">
     
<div id="star_register">
		
		Status <br>
		<select name="" id="status_game" class="form-control">
			<option value="">Change Status</option>
			<option value="1" >Publish Now</option>
			<option value="0">Draft</option>
		</select>
<br>

Title <br>
<input type="text" id = "game_title" class="form-control"><br>
Game Link <br>
<input type="text" id = "game_link" class="form-control"><br>
<!-- <a href="" class="btn btn-primary" id = "view_game_article" target="_blank">View Blog</a> -->
<!-- <textarea placeholder="Details" id = "game_desc"></textarea> -->
<div id="thumb_change" class="text-center" style = "display:none">
Are you sure you want to change the thumbnails <br>
	
	<button class="btn_red" data-game-id = "" id = "yes_change_thumb">Yes</button>
	<button class="btn_green " id = "no_change_thumb">No</button>
	
</div> 
<div id="thumb_view" style="display:none">
	<button class="btn_blue" id = "chang_the_thumb">Change Thumbnail</button>
	<img src="" id = "thum_prev" alt="thumbnails" style="width:100% !important">
</div>

<div id="thumb_image_modal" style = "display:none">
		
		Thumbnail Image:<br>
		 <form action="<?php echo base_url('game/upload_file');?>" class="dropzone" id = "game_dropzone">
              <div class="fallback">
                <input name="file" type="file" accept="image/*" id = "file_video" />
              </div>
       </form >
       <div class="text-center">
	<button class="btn_blue" id = "upload_attachment_new" data-game-ids = ""><i class="fa fa-upload"></i> Upload New ThumbNail</button>
	</div>
       <br><br>
</div>
		
		<!-- <input type="submit" value="Update" id="update_game"> -->
		
	</div>
	

      </div>
      <div class="modal-footer">
	
        <button type="button" class="btn btn_blue" id="update_game" >Update</button>
        <button type="button" class="btn btn_green" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>


<div id="game_delete_modal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-sm">
   
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Delete Game</h4>
      </div>
      <div class="modal-body">
       <p>Are you sure you want to delete this?</p>
      </div>
      <div class="modal-footer">
      <button type="button" class="btn btn_red" id = "yes_delete"  data-game-modal-id = ""><i class="fa fa-trash"></i> Delete</button>
        <button type="button" class="btn btn_green" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

<script type="text/javascript">Dropzone.autoDiscover = false;

	
	// 			CKEDITOR.replace('game_desc',{
	// 	filebrowserBrowseUrl : window.App.baseUrl+'/blog/browse/',
	// 	filebrowserUploadUrl : window.App.baseUrl+'/post/upload_blog/',

	// 	filebrowserWindowWidth : '640',
 //        filebrowserWindowHeight : '480',
 //        		    on: {
	// 	        instanceReady: function() {
	// 	            this.dataProcessor.htmlFilter.addRules({
	// 	                elements: {
	// 	                    img: function( el ) {
	// 	                        el.addClass( 'img-responsive' );
	// 	                    }
	// 	                }
	// 	            });            
	// 	        }
	// 	    }
	// });
</script>