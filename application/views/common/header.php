<!DOCTYPE html>
<html>
<head>
<title>
</title>
<meta charset="UTF-8">

<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
<link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap.min.css');?>">
<link rel="stylesheet" href="<?php echo base_url('assets/css/jquery-ui.css');?>">
<link rel="stylesheet" href="<?php echo base_url('assets/css/pnotify.css');?>">
<link rel="stylesheet" href="<?php echo base_url('assets/css/basic.min.css');?>">
<link rel="stylesheet" href="<?php echo base_url('assets/css/basic.min.css');?>">
<link rel="stylesheet" href="<?php echo base_url('assets/css/dropzone.min.css');?>">
<link rel="stylesheet" href="<?php echo base_url('assets/css/font-awesome.min.css');?>">
<link rel="stylesheet" href="<?php echo base_url('assets/css/style.css');?>">
<link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap-datepicker.css');?>">

<script type="text/javascript" src="<?php echo base_url('assets/js/dropzone.min.js');?>"></script> 
<script type="text/javascript" src="<?php echo base_url('assets/ckeditor/ckeditor.js');?>"></script>

	<?php if (isset($css)): ?>
		<?php if (is_array($css)): ?>
			<?php foreach ($css as $stylesheet): ?>
				<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/' . $stylesheet); ?>">
			<?php endforeach; ?>
		<?php else: ?>
			<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/' . $css); ?>">
		<?php endif; ?>
	<?php endif; ?>
	<script type="text/javascript">
		window.App = {
				"baseUrl" : "<?php echo base_url(); ?>",
				"removeDOM" : "",
		};
	</script>
</head>
<body style="padding:10px">
<div id="back"></div>
