 <script type="text/javascript" src="<?php echo base_url('assets/js/jquery.min.js');?>"></script>
 <script type="text/javascript" src="<?php echo base_url('assets/js/jquery-ui.js');?>"></script>

 <script type="text/javascript" src="<?php echo base_url('assets/js/bootstrap.min.js');?>"></script>
 <script type="text/javascript" src="<?php echo base_url('assets/js/pnotify.js');?>"></script>
 <script type="text/javascript" src="<?php echo base_url('assets/js/bootstrap-datepicker.js');?>"></script>
 <script type="text/javascript" src="<?php echo base_url('assets/js/jquery.touchswipe.min.js');?>"></script>


	<?php if (isset($js)): ?>
		<?php if (is_array($js)): ?>
			<?php foreach ($js as $j): ?>
				<script type="text/javascript" src="<?php echo base_url('assets/js/' . $j); ?>"></script>
			<?php endforeach; ?>
		<?php else: ?>
			<script type="text/javascript" src="<?php echo base_url('assets/js/' . $js); ?>"></script>
		<?php endif; ?>
	<?php endif; ?>
 <script>
 $(document).ready(function(){
  	$(".admin_nav").click(function(){
  		$("#back").fadeIn(function(){
  			$("#admin_nav").animate({ left : "0px" });  			
  		});
  	});
	$("#close_admin_nav,#back").click(function(){
  		$("#admin_nav").animate({ left : "-100%" }, function(){
  			$("#back").fadeOut();
  		});
  	});

$(".fa-cog").click(function(){
      $("#accnt_set").toggle();
    });

  $("#main_icon,#back").click(function(){
    $("#back").stop().fadeToggle();
    $("#list_icons").stop().fadeToggle();

    if($("#main_icon .icon_of_list").hasClass("fa-minus")){
      $("#main_icon .icon_of_list").delay(1000).removeClass("fa-minus").addClass("fa-plus"); 
    }
    else{
      $("#main_icon .icon_of_list").delay(1000).removeClass("fa-plus").addClass("fa-minus"); 
    }  

  });

  });
 
  </script>

</body>
</html>