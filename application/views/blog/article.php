<style>
img{
	width:100% !important;
	height:auto !important;
	padding:10px;
	border:1px solid #999;
	background:white;
}
</style>
<div class="container">
<?php if (!empty($blog)): ?>
	<div class="row" style="margin-top:-30px;border-bottom:1px solid #c1c1c1">
		<h3 style="font-weight:bold"><?php echo $blog->blog_title ?></h3>	

	</div>	
	<div class="row">
		<div style="color:#c1c1c1">
			<em><?php echo   $blog->date_released  ."&nbsp;". $blog->blog_author ?></em>
		</div>
	</div>
	<br>
	
<!-- 	<h3><?php echo $blog->blog_sub_title ?></h3> -->
	

	<?php echo $blog->blog_desc ?>
	<?php if (!empty($file_url)): 

	?>
	
<div class="text-center"><a class="btn btn-primary" href="<?php echo $file_url->path_file?>" download><i class="fa fa-download"></i> Download File</a></div>	
	<?php endif ?>
<?php endif ?>

<?php if (empty($blog)): ?>
	<h1 class="text-center">Sorry, this page doesnt exist! :(</h1>
<?php endif ?>
	
	
</div>
