<div id="wrap">	
	<div id="nav_ban">
		<div id="ban">
		<span style="font-size:25px">U活动</span>
		</div>

		<div id="nav">
			<table cellspacing="0" style="width:100% !important;border-radius:5px !important;background:#f1f1f1">
				<tr><td style="width:50%"><a href="#"><input id="click_post" type="submit" value="精选活动" style="font-weight:bold" class="active_nav"></a>
				<td style="width:50%"><a href="#"><input id="click_game" type="submit" value="小游戏" style="font-weight:bold"></a>
			</table>
		</div>
	</div>
  <?php if (!empty($banner)): ?>
	  <div id="myCarousel" class="carousel slide" data-ride="carousel" style="margin:auto;margin-top:5px !important;height:180px;overflow:hidden;margin-top:10px;width:95%;margin-bottom:-30px">
    <!-- Indicators -->
    <ol class="carousel-indicators">
   
        <?php
        $count = 1;
         foreach ($banner as $row):
         $count = $count + 1;?>
      <li data-target="#myCarousel" data-slide-to="<?php echo $count?>" class= "active"></li>      
        	 <?php endforeach ?>    

    </ol>

    <!-- Wrapper for slides -->

    <div class="carousel-inner" role="listbox">
    <?php foreach ($banner as $key => $row): ?>
       <div class="item  <?= $key === 0 ? "active" : ""?>">
        <img src="<?php echo base_url().'uploads/banners/'.$row->file_name?>" alt="<?php $row->org_file_name?>" width="460" height="345">
           </div>
    <?php endforeach ?>
       	   



    </div>

    <!-- Left and right controls -->
    <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev" style="display:none">
      <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next" style="display:none">
      <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>
    <?php endif ?>

	<input type="hidden" id = "current_page" value="post">
	<div id="main" class = "blog_index_page">


    <div id="blog_post" style = "display:none"></div>
    <div id="game_post" style = "display:none"></div>
    <div id="end_of_file">	
    </div>


	</div>
</div>


<div id="post_details" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" id = "view_post_title"></h4>
      </div>
      <div class="modal-body">
	      <div class="form-group">
	       <label for="">Description:</label>
	      
	       <p id = "view_blog_desc"></p>

		
	       <!-- <label for="">Path:</label>
	       <span class="label label-primary"><a target="_blank" style = "color:white" href="" id = "view_blog_path" ></a></span> -->
	       <!-- <img src="" id = "path" alt="" style="width:100%" > -->
	        </div>
	     
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

<div id="video_details" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" id = "view_video_title">Description</h4>
      </div>
      <div class="modal-body">
	      <div class="form-group">
	       <label for="">Description:</label>
	      
	       <p id = "view_video_desc"></p>		
	    
	       <span class="label label-primary"><a target="_blank" style = "color:white" href="" id = "view_video_path" ></a></span>
	       <!-- <img src="" id = "path" alt="" style="width:100%" > -->
	        </div>
	     
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>