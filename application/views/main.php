<!DOCTYPE html>
<html>
<head>
<title>Welcome to UTalk
</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" href="<?php echo base_url('assets/css/style.css')?>">
<link rel="stylesheet" href="<?php echo base_url('assets/css/font-awesome.min.css')?>">
<link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap.min.css')?>">
<script src = "<?php echo base_url('assets/js/dropzone.min.js')?>"></script>


	<?php if (isset($css)): ?>
		<?php if (is_array($css)): ?>
			<?php foreach ($css as $stylesheet): ?>
				<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/' . $stylesheet); ?>">
			<?php endforeach; ?>
		<?php else: ?>
			<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/' . $css); ?>">
		<?php endif; ?>
	<?php endif; ?>
<script type="text/javascript">
		window.App = {
				"baseUrl" : "<?php echo base_url(); ?>",
				"removeDOM" : "",
		};
	</script>


<style>
body{
	padding:10px;
	background:url("<?php echo base_url('images/background.jpg')?>") no-repeat;
	background-size:cover;
	color:white;
	font-family:century gothic;
}
::-webkit-input-placeholder {
  color: white;
}
::-moz-input-placeholder {
  color: white;
}
::-o-input-placeholder {
  color: white;
}
::input-placeholder {
  color: white;
}

.dz-default span{
	color:black !important;
}
</style>
</head>
<body style="">
<div id="wrap">

<div id="main_page">
<!-- <center>
	<table cellspacing="0">
	<tr><td align="center">
	<?php if (!empty($userinfo->profile_pic)){ ?>
		<img src="<?php echo base_url()."/profile_pic/". $userinfo->profile_pic?>" alt = "profile_pic" id="log_user">
	<?php }else{?>
<img src="<?php echo base_url('images/username.png')?>" id="log_user">
	<?php } ?> 

	<br>
	<tr><td align="center"><?php echo $this->session->userdata('username');?>
	</table>
</center> -->
<div id="btn_links">	
		<ul>
			<li><a href="#"><div>?????</div></a></li>
			<li><a href="#"><div>?????</div></a></li>
			<li><a href="<?php echo base_url('admin/post');?>"><div>Post Settings</div></a></li>
			<li><a href="<?php echo base_url('admin/banner');?>"><div>Banner Settings</div></a></li>
			<li><a href="<?php echo base_url('admin/download');?>"><div>Download Settings</div></a></li>
			<li><a href="<?php echo base_url('admin/star');?>"><div>Star Settings</div></a></li>
		</ul>
</div>
</div>


<div id="login_design"  style="display:none" class="edit_account">
<span style= "position:fixed;left:0;top:10px" class="fa fa-angle-left fa-2x" id="back_to_main"></span>
<center>
	<table cellspacing="0">
	<tr><td align="center">
	<?php if (!empty($userinfo->profile_pic)){ ?>
		<img src="<?php echo base_url()."/profile_pic/". $userinfo->profile_pic?>" alt = "profile_pic" id="log_user">

	<?php }else{?>
<img src="<?php echo base_url('images/username.png')?>" id="log_user">

	<?php } ?> 

	<br>
	<tr><td align="center">	<input type="file" style = "display:none" value = "change profile" name="" id="file_input"></br><button id="change_profile" class="btn_blue">Change Profile</button></br><?php echo $this->session->userdata('username');?>
	</table>
</center>
<?php 
$u = $userinfo;
 ?>
 	<input value = "<?php echo $u->username ?>" type="text" placeholder="Username" id="user" hidden><br>
	<input value = "<?php  ?>" type="file" id="upload" accept="image/png" id="upload" style="display:none">
	<input value = "<?php echo $u->first_name ?>" type="text" placeholder="Firstname" id="first_name"><br>
	<input value = "<?php echo $u->last_name ?>" type="text" placeholder="Lastname" id="last_name"><br>
	<input value = "<?php echo $u->email ?>" type="email" placeholder="Email" id="email"><br>
	
	<input value = "" type="password" placeholder="Password" id="pass"><br>
	<input value = "" type="password" placeholder="Confirm Password" id="con_pass" ><br><br>
	<input  type="submit" value="UPDATE ACCOUNT" id="edit_user"><br>
</div>


</div>

<div id="change_profile_modal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title text-center" style="color:black">Change Profile</h4>

      </div>
      <div class="modal-body">
        <form action="<?php echo base_url('post/upload');?>" class="dropzone" id = "profile_picture">
	              <div class="fallback">
	                <input name="file" type="file" accept="image/*" />
	              </div>
	       </form >
      </div>
      <div class="modal-footer">
      <button type="button" class="btn btn_blue" id = "change_profile_btn">Change Profile Picture</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>


	<div id="login_foot">
	 &copy; UTalk Tutorial Services </b>
	</div>

<script>Dropzone.autoDiscover = false;</script>
</body>

</html>