<!DOCTYPE html> 
<html>
<head>
<title>Operation Guide</title>
<meta charset="UTF-8">
<meta name="apple-mobile-web-app-capable" content="yes" />
<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent" />
<link rel="stylesheet" href="<?php echo base_url('assets/css/style_micro.css')?>">
<script src="<?php echo base_url('assets/js/jquery.min.js')?>"></script>
<script src="<?php echo base_url('assets/js/jquery.touchswipe.min.js')?>"></script>
<link rel="stylesheet" href="<?php echo base_url('assets/css/font-awesome.min.css')?>">

<script>
	$(document).ready(function(){
		// video play
		$("video,#exit").hide();
		$("*#video").click(function(){
			var x= $(this).siblings(".video").val();

			$("#back").fadeIn(function(){
			$("video").fadeIn(function(){
				$("#exit").show();
			}).attr("src",x);
		});
		});

		$("#exit,#back").click(function(){
			$("#exit").hide();
			$("video").attr("src","#").fadeOut(function(){
				$("#back").fadeOut();
			});
		});

		$('video').bind('webkitfullscreenchange mozfullscreenchange fullscreenchange', function(e) {
		   	var state = document.fullScreen || document.mozFullScreen || document.webkitIsFullScreen;
		    var event = state ? 'FullscreenOn' : 'FullscreenOff';
		    if(event=="FullscreenOff"){
		    	$("#exit").hide();
				$("video").attr("src","#").fadeOut(10,function(){
					$("#back").fadeOut(10);
				});
		    }  
		});
		$("video").bind("ended",function(){
    		document.getElementsByTagName('video')[0].webkitExitFullscreen();
		   	var state = document.fullScreen || document.mozFullScreen || document.webkitIsFullScreen;
		    var event = state ? 'FullscreenOn' : 'FullscreenOff';
		    if(event == "FullscreenOn"){
	    	$("#exit").hide();
			$("video").attr("src","#").fadeOut(10,function(){
				$("#back").fadeOut(10);
			});	
			}	
			else if(event == "FullscreenOff"){
	    	$("#exit").hide();
			$("video").attr("src","#").fadeOut(function(){
				$("#back").fadeOut();
			});	
			}		
		});
		// phone validation
		 if(navigator.userAgent.match(/(iPod|iPhone|iPad)/)){
	  	$("video").bind("pause",function(){
    		document.getElementsByTagName('video')[0].webkitExitFullscreen();
		   	var state = document.fullScreen || document.mozFullScreen || document.webkitIsFullScreen;
		    var event = state ? 'FullscreenOn' : 'FullscreenOff';
		    if(event == "FullscreenOn"){
	    	$("#exit").hide();
			$("video").attr("src","#").fadeOut(10,function(){
				$("#back").fadeOut(10);
			});	
			}	
			else if(event == "FullscreenOff"){
	    	$("#exit").hide();
			$("video").attr("src","#").fadeOut(function(){
				$("#back").fadeOut();
			});	
			}		
		});
	  }
	// circle animation
	$('#animate').off('scroll mousewheel touchmove');
	$(window).on("scroll mousewheel touchmove",function(){
		var a=$(window).scrollTop();
		var b=$(window).width();
		// $("#num").text(a);
		// $("#body").fadeIn();
		// alert(b);
		if(b<1100){
		if(a>0){
			$("*#circle3").addClass("hide")
			.css({"animation":"resize 0.3s ease-out","transform":"scale(0.7) translate(590px,80px)"})
			.html("<div style='margin-top:25px;font-size:50px'>立即<br>注册</div>");
			$("#body").css({ "margin-top" : "-40px"});
		}
		else {
			if($('*#circle3').hasClass('hide')){
				$("*#circle3").removeClass("hide")
				.css({"animation":"desize 0.3s ease-out","transform":"scale(1)"})
				.html("<div style='margin-top:15px'><br>立即注册</div>");
			$("#body").css({ "margin-top" : "190px"});
			// if(b<1000){
			// 	$("#body").fadeOut();
			// }
			}
		}
	}
	else{
	if(a>0){
			$("*#circle3").addClass("hide")
			.css({"animation":"resize2 0.3s ease-out","transform":"scale(0.3) translate(1900px,80px)"})
			.html("<div style='margin-top:25px;font-size:50px'>立即<br>注册</div>");
			$("#body").css({ "margin-top" : "-40px"});
		}
		else {
			if($('*#circle3').hasClass('hide')){
				$("*#circle3").removeClass("hide")
				.css({"animation":"desize2 0.3s ease-out","transform":"scale(0.6)"})
				.html("<div style='margin-top:15px'><br>立即注册</div>");
			$("#body").css({ "margin-top" : "190px"});
			// if(b<1000){
			// 	$("#body").fadeOut();
			// }
			}
		}
	}

	});
	// slideshow
	var current = 0;
	var swipenum=1;
	// auto slide
	setInterval(function(){
		if (current === 0) {
			$("#animate").stop().animate({scrollLeft: current});
			$("#page1").addClass("active");
			$("#page2,#page3").removeClass("active");
			swipenum=1;
			current = 898;
		} 
		else if (current === 898) {
			$("#animate").stop().animate({scrollLeft: current});
			$("#page2").addClass("active");
			$("#page1,#page3").removeClass("active");
			current = 1755;
			swipenum=2;

		} 
		else if (current === 1755) {
			$("#animate").stop().animate({scrollLeft: current});
			$("#page3").addClass("active");
			$("#page2,#page1").removeClass("active");
			current = 0;
			swipenum=3;

		}


	$("#num").text(swipenum);
	},5000);

	// swipping part
	$("#animate").swipe({
		swipeRight:function(){
		swipenum=swipenum - 1;
		if(swipenum<1){
			swipenum=3;
		}
	
	if(swipenum==1){
		$("#animate").animate({scrollLeft: "0" },800);
		$("#page1").addClass("active");
		$("#page2,#page3").removeClass("active");
		current=0;
	}
	else if(swipenum==2){
		$("#animate").animate({scrollLeft: "898" },800);
		$("#page2").addClass("active");
		$("#page1,#page3").removeClass("active");
			current = 898;
	}
	else if(swipenum==3){
		$("#animate").animate({scrollLeft: "1755" },800);
		$("#page3").addClass("active");
		$("#page2,#page1").removeClass("active");
			current = 1755;
	}
	$("#num").text(swipenum);
}
	});

	$("#animate").swipe({
		swipeLeft:function(){
		swipenum=swipenum + 1;
		if(swipenum>3){
			swipenum=1;
		}

	if(swipenum==1){
		$("#animate").animate({scrollLeft: "0" },800);
		$("#page1").addClass("active");
		$("#page2,#page3").removeClass("active");
		current=0;
	}
	else if(swipenum==2){
		$("#animate").animate({scrollLeft: "898" },800);
		$("#page2").addClass("active");
		$("#page1,#page3").removeClass("active");
			current = 898;
	}
	else if(swipenum==3){
		$("#animate").animate({scrollLeft: "1755" },800);
		$("#page3").addClass("active");
		$("#page2,#page1").removeClass("active");
			current = 1755;
	}
	$("#num").text(swipenum);
}
	});

	// clicking part
	$("#page1").click(function(){

		$("#animate").animate({scrollLeft: "0" },800);
		$(this).addClass("active");
		$("#page2,#page3").removeClass("active");
		current=0;
	});

	$("#page2").click(function(){
		$("#animate").animate({scrollLeft: "898" },800);
		$(this).addClass("active");
		$("#page1,#page3").removeClass("active");
			current = 898;
	});

	$("#page3").click(function(){
		$("#animate").animate({scrollLeft: "1755" },800);
		$(this).addClass("active");
		$("#page2,#page1").removeClass("active");
			current = 1755;
	});



	});

</script>
</head>
<body>
<div id="back">	
	<div id="exit">
	<img src="../images/micro/close.png" style="height:40px">
	</div>
	</div>
	
	<video width="320" height="240" controls autoplay="autoplay">
	  <source src="#" type="video/mp4">
	</video>

		<a href="http://edu.helputalk.com/app/index.php?i=3&j=5&c=auth&a=login">
		<div id="circle3"><br>
			<div style="margin-top:15px">立即注册</div>
		</div>
		</a>

<div id="wrap">
	<!-- <div id="num" style="position:fixed;z-index:99;bottom:20px;font-size:30px"></div> -->
	<div id="nav">
		<br><br><br><br><br>
		<center style="color:#888" id="animate">
		<table class="img" style="width:2700px;margin-left:-30px" cellspacing="110">
			<tr><td><div style="border:10px solid #1f7afe;margin-top:-15px">
				<img src="../images/micro/banner1.jpg" style="clear:both;width:100%;">
				</div>
				<div style="font-size:35px;margin-top:20px">
				海量外教随心选，<i style="font-size:70px;color:#1f7afe;font-weight:bold">100%</i> 只关注一个孩子<br>
				浸入式语言环境，全英文互动练习
			</div>
			<td><div style="border:10px solid #1f7afe;margin-top:-45px">
				<img src="../images/micro/banner2.png" style="clear:both;width:110%;margin:80px 0px 80px -50px">
				</div>
				<div style="font-size:35px;margin-top:20px">
				移动端学习，不受地域限制<br>
				高效利用碎片时间，让学习更高效、更方便
			</div>
			<td><div style="border:10px solid #1f7afe;margin-top:0px">
				<img src="../images/micro/banner3.png" style="clear:both;width:110%;margin:80px 0px 50px -20px">
				</div>
				<div style="font-size:35px;margin-top:20px">
				一套教材 学精学透<br>
				线上+线下同步学习 英语综合能力提升快

			</div>
		</table>

		<div id="navigation">
		<a href="http://edu.helputalk.com/app/index.php?i=3&j=5&c=auth&a=login" style="float:right;text-decoration:none;color:#333">
		<table style="margin-right:30px;margin-top:30px">
		<tr><td><img src="../images/micro/banner_icon2.png" style="width:80px;cursor:pointer"><td style="font-size:30px">登陆
		</table>
		</a>
		<img src="../images/micro/banner_icon.png" id="nav_img">
		</div>

		</center>

		<div id="controls">
		<ul>
			<li><div id="page1" class="active"></div>
			<li><div id="page2"></div>
			<li><div id="page3"></div>
		</ul>
		</div>

		<a href="http://edu.helputalk.com/app/index.php?i=3&j=5&c=auth&a=login" id="hide_this">
		<div id="circle3"><br>
			<div style="margin-top:15px">立即注册</div>
		</div>
		</a>
	</div>

	<div id="body" style="margin-top:190px">
	<div id="head">
		<div id="title_head">我们希望</div>
		<img src="../images/micro/divider.png">
		<div id="title_desc">每一个孩子都可以享受平等的教育</div>
	</div>

	<div id="img"> 
		<img src="../images/micro/1.jpg" id="video">
		<input type="submit" value="http://source.helputalk.com/Uwebsite1min.mp4 " class="video" hidden>
		<div style="padding:10px;text-align:center;color:white;background:#62a2fe;margin-top:-5px;font-size:30px;">
			新华网大国教育之声高端访谈
		</div>
	</div>

	<div id="img">
		<img src="../images/micro/2.jpg" id="video">
		<input type="submit" value="http://source.helputalk.com/Uwebsite2min.mp4 " class="video" hidden>
		<div style="padding:10px;text-align:center;color:white;background:#62a2fe;margin-top:-5px;font-size:30px;">
			爱上帮你说的她
		</div>
		
	</div>

	<div id="head">
		<div id="title_head">爱上帮你说的理由</div>
		<img src="../images/micro/divider2.png">
	</div>

	<div id="cont">
		<div id="circle2"><div style="margin-top:60px">特色介绍</div></div>
		<img src="../images/micro/girl.png" style="width:120%;margin-top:-200px;margin-left:-80px">
		<br><br><br>
		<div id="cont_inside">
		<table style="text-align:left;margin-left:30px" cellspacing="30">
			<tr><td rowspan="3"><img src="../images/micro/icon1.png" style="height:160px">
				<td><img src="../images/micro/icondesc1.png" style="height:50px">
			<tr><td style="font-size:27px;color:#888;line-height:40px">完全同步线下实体课堂学习内容同步练习、<br>
					同步复习、同步测试、同步呈现
		</table>
		</div>

		<br><br><br>
		<div id="cont_inside">
		<table style="text-align:left;margin-left:30px" cellspacing="30">
			<tr><td rowspan="3"><img src="../images/micro/icon2.png" style="height:160px">
				<td><img src="../images/micro/icondesc2.png" style="height:50px">
			<tr><td style="font-size:27px;color:#888;line-height:40px">可汗学院翻转课堂理念<br>
				让学员100%的注意力都在学习内容本身

		</table>
		</div>

		<br><br><br>
		<div id="cont_inside">
		<table style="text-align:left;margin-left:30px" cellspacing="30">
			<tr><td rowspan="3"><img src="../images/micro/icon3.png" style="height:160px">
				<td style="font-size:50px;font-weight:bold;color:#1f7afe">深度教育
			<tr><td style="font-size:27px;color:#888;line-height:40px">以教育本质为核心，<br>将深度教育理念注入至互联网。

		</table>
		</div>

		<br><br><br>
		<div id="cont_inside">
		<table style="text-align:left;margin-left:30px" cellspacing="30">
			<tr><td rowspan="3"><img src="../images/micro/icon4.png" style="height:160px">
				<td style="font-size:50px;font-weight:bold;color:#1f7afe">固定习惯
			<tr><td style="font-size:27px;color:#888;line-height:40px">相同的时间，熟悉的外教，<br>养成良性学习习惯。
		</table>
		</div>

		<br><br><br>
		<div id="cont_inside">
		<table style="text-align:left;margin-left:30px" cellspacing="30">
			<tr><td rowspan="3"><img src="../images/micro/icon5.png" style="height:160px">
				<td><img src="../images/micro/icondesc5.png" style="height:50px">
			<tr><td style="font-size:27px;color:#888;line-height:40px">只需一部智能移动设备，即可轻松上课<br>
				不受地理位置限制，随时随地，想学就学
		</table>
		</div>
		<br><br>
	</div>

	<br><br><br><br><br>
	<div id="head">
		<div id="title_head">三大中心为学习保驾护航</div>
		<img src="../images/micro/divider2.png">
	</div>

	<div id="title_photo">
		教学中心
	</div>

	<img src="../images/micro/3.jpg" id="img2">
	<div id="desc">
		每一位外教都经过极其苛刻的筛选，具备多年教学<br>
		经验及相关资格认证，全职服务于帮你说
	</div>

	<div id="title_photo">
		研发中心
	</div>

	<img src="../images/micro/4.jpg" id="img2">
	<div id="desc" style="text-align:justify">
		●  国内外资深专业教研顾问、一线英语教研员为<br>
		教学内容提供优质保障<br>
		●  国内外优秀技术研发人员，均具备国内外一线<br>
		互联网公司的工作经验，操作界面更贴合用户使<br>
		用习惯

	</div>


	<div id="title_photo">
		研发中心
	</div>

	<img src="../images/micro/5.jpg" id="img2">
	<div id="desc">
		7X14小时在线客服支持，全程一对一跟踪、<br>
		指导课程进度，选课上课完全无忧

	</div>


	<div id="head">
		<div id="title_head">企业荣誉</div>
		<img src="../images/micro/divider.png">
	</div>

	<div id="desc">新华网“大国教育之声”</div>
	<img src="../images/micro/6.jpg" id="img2" style="border:0;margin-bottom:30px;margin-top:-30px">
	<div id="desc">2016 互联网+教育线上英语教学创新品牌</div>

	<div id="head">
		<div id="title_head">媒体关注</div>
		<img src="../images/micro/divider.png">
	</div>

	<img src="../images/micro/7.jpg" style="width:90%">
	<table style="width:100%" id="tbl_img">
		<tr><td><img src="../images/micro/1.png">
			<td><img src="../images/micro/2.png">
			<td><img src="../images/micro/3.png">
	</table>
	<table style="width:100%" id="tbl_img">
		<tr><td><img src="../images/micro/4.png">
			<td><img src="../images/micro/5.png">
			<td><img src="../images/micro/6.png">
	</table><br><br><br><br>

	<div id="head">
		<div id="title_head">合作机构</div>
		<img src="../images/micro/divider.png">
	</div>

	<table style="width:100%" id="tbl_img">
		<tr><td><img src="../images/micro/7.png">
			<td><img src="../images/micro/8.png">
	</table>
	<table style="width:100%" id="tbl_img">
		<tr><td><img src="../images/micro/9.png">
			<td><img src="../images/micro/10.png">
	</table>

	<div id="foot">
	客服热线：400-000-7075<br>
	<div style="font-size:35px;margin-top:30px">咨询时间：周一至周五 9:00—22:00<br>
     <div style="margin-left:160px;margin-bottom:20px"> 周六至周日 8:00—22:00</div></div>
	</div>
	</div>
</div>
</body>
</html>
 <script>
 $(document).ready(function(){
  	$(".admin_nav").click(function(){
  		$("#back").fadeIn(function(){
  			$("#admin_nav").animate({ left : "0px" });  			
  		});
  	});
	$("#close_admin_nav,#back").click(function(){
  		$("#admin_nav").animate({ left : "-100%" }, function(){
  			$("#back").fadeOut();
  		});
  	});

$(".fa-cog").click(function(){
      $("#accnt_set").toggle();
    });

  $("#main_icon,#back").click(function(){
    $("#back").stop().fadeToggle();
    $("#list_icons").stop().fadeToggle();

    if($("#main_icon .icon_of_list").hasClass("fa-minus")){
      $("#main_icon .icon_of_list").delay(1000).removeClass("fa-minus").addClass("fa-plus"); 
    }
    else{
      $("#main_icon .icon_of_list").delay(1000).removeClass("fa-plus").addClass("fa-minus"); 
    }  

  });

  });
 
  </script>
