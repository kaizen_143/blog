
<div class="">
	<div class="col-lg-10 pull-right">
		<h3>精选活动</h3>
		<a class="btn btn_blue" href="<?php echo base_url('admin/post/new_post')?>"><i class="fa fa-plus"></i> Add</a> <a class="btn btn_blue" href="<?php echo base_url('blog/')?>"><i class="fa fa-eye"></i> Preview </a> <a class="btn btn_blue" id = "open_banner" href="#"> <i class="fa fa-flag"></i> Banner Settings </a>
<!-- 		<form action="" method="GET">
		<div class="form-inline">
		<label for="">Search:</label>
		<input type="text" name = "blog_title" placeholder="Blog Title" class="form-control">
		<select class = "form-control" name="blog_status" id="view_publish_blog">
			<option value="">Search by Post</option>
			<option value="1">Publish</option>
			<option value="0">Draft</option>
		</select>
		
		<label for="">From:</label>
		<input type="text" name = "date_from" placeholder="YYYY-DD-MM" class="form-control">
		<label for="">To:</label>
		<input type="text" name = "date_to" placeholder="YYYY-DD-MM" class="form-control">
		<button type="submit" class="btn_blue"><i class="fa fa-search"></i></button>
		</div>
		</form> -->
		<div style="width:100%;overflow:auto">
		<table class="table" cellspacing="0" width="100%">
			<thead>
				<tr>
					<th class="text-center">ID</th>
					<!-- <th class="text-center">Title</th>					 -->
					<th class="text-center">Content</th>
					<th class="text-center">Date Save</th>
					<th class="text-center">Status</th>
					<th class="text-center">Publish Date</th>
					<th class="text-center">Order</th>
					<th class="text-center">Action</th>
				</tr>
			</thead>
			<tbody>
			<?php if (!empty($posts)): ?>
				
			
				<?php foreach ($posts as $row): ?>
					<tr data-id = "<?php echo $row->id?>">
						<td class = "text-center"><?php echo $row->id?></td>
						<!-- <td class = "text-center"><?php echo $row->blog_title ?></td> -->
						<td ><button status-id = "<?php echo $row->id?>"   class="btn_blue form-control view_post_details"  >Edit Post</button></td>
						<td class="text-center"><?php echo $row->date?></td>
						<td class="text-center">
							<!-- <select name="" id="" class="form-control post_status"> -->
								<?php if ($row->status == 0): ?>
									Draft 
									<!-- <option value="2">Schedule Post</option> -->
								<?php endif ?>
								<?php if ($row->status == 1): ?>
									Publish 								
									<!-- <option value="2">Schedule Post</option> -->
								<?php endif ?>

						</td>
						<td class="text-center" data-publish = "<?php echo $row->id ?>"><?php echo $row->date_released?></td>
						<td class = "text-center">

						<select name=""  class="order_by form-control">
							<?php if (!empty($row->order)){
								$order =$row->order;
								}else{
									$order = '0';
									}?>
							<option value="<?php echo $order ?>"><?php echo $order ?></option>
							<?php 
								  $number_of_post = count($posts);
								  for ($x = 1; $x<=$number_of_post; $x++) {
								  	echo "<option value='".$x."'>".$x."</option>";
								  }
							?>
							</select>
						</td>
						<td class=" text-center"><button class="btn_red open_confirm_delete_modal"><i class="fa fa-trash"></i></button></td>
					</tr>
				<?php endforeach ?>
				<?php endif ?>
			</tbody>
		</table>

		<?php echo $pager?>

		</div>

	</div>
</div>


 <div class="modal fade" id='confirm_delete_post' tabindex="-1" role="dialog" aria-hidden="true" >
	<form method="post" id="form-update" class="form-horizontal">
	  <div class="modal-dialog modal-sm">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        <h4 class="modal-title text-center" style="font-weight: bold;">Delete Post</h4>
	      </div>
	      <div class="modal-body">
					        <p>Are you sure you want to delete this post? all of the content will be remove.</p>
	      </div>
	      <div class="modal-footer">
	      <div class="btn-group ">
				<button type="submit" class="btn_green" data-dismiss="modal" id = "btn_delete_modal" data-id-delete = "">Yes</button>
				<button type="button" class="btn_red" data-dismiss="modal">Close</button>
		  </div>		
	      </div>
	    </div>
	  </div>
	</form>
</div>


<div class="modal fade" id='schedule_post' tabindex="-1" role="dialog" aria-hidden="true" >
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title text-center" id="title_head" style="font-weight: bold;">Schedule Post</h4>
      </div>
      <div class="modal-body">
		<input type="text" class = "form-control" id="schedule_post_input">
      </div>
      <div class="modal-footer">
    

      	<button type="button" class="btn btn-default" data-id-modal = ""  id = "save_schedule_post" >Save</button>
			<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>


<div id='modal_update' class="modal fade" role='dialog' tabindex='-1' aria-labelledby="myModalLabel">
  <form method="post" id="form-update" class="form-horizontal">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
			<button type='button' class='close' data-dismiss='modal'><span aria-hidden='true'>&times; </span></button>
		<h4 class="modal-title"> Post Status: <span id = "post_status_title"></span></h4>  
        </div>
        <div class="modal-body">
		<div class="col-sm-12">
        	<div class="form-group ">
        	<select name="" id="post_status" class="form-control" data-id-status = "">
        		<option value="">Change Status</option>
        		<option value="0">Draft</option>
        		<option value="1">Publish Now</option>
        	</select>
        	<label for="">Post Title</label>
        	<input type="text" id = "post_title" class="form-control">
        		<!-- <label for="">Post Sub Title</label>
        		<input type="text" id = "post_sub_title" class="form-control"> -->
        		<label for="">Author</label>    		
        		<input type="text" id = "post_author" class="form-control"><br> 
		
        		<a href="" class = "btn btn_blue" id = "post_preview" target = "_blank">View Post</a>
        		<br>

				<textarea name="" id="blog_desc" cols="30" rows="10"></textarea><br>
				
<!-- 				<select name="" id="select_thumbnails" class="form-control">
					<option value="">Select Thumbnails</option>
					<option value="0">Video</option>
					<option value="1">Photo</option>
				</select>
 -->
				<div class="text-center" id = "no_dropzone"  style = "display:none" >
<!-- 					<div action="<?php echo base_url('star/upload_file');?>" class="dropzone" id = "star_videos">
						<div class="fallback">
						<input name="file" type="file" accept="image/*" id = "file_video" />
						</div>
					</div > -->

					<div action="<?php echo base_url('star/upload_file');?>" class="dropzone" id = "star_photo">
						<div class="fallback">
							<input name="file" type="file" accept="image/*" id = "file_video" />
						</div>
					</div >
					<button data-file-id = "" class="btn btn-primary" id = "upload_new_thumb"> <i class="fa fa-camera"></i> Upload New Post Thumbnails</button>
				</div>
			<!-- <label for="">Add Files to download</label>
					<div action="<?php echo base_url('star/upload_file');?>" class="dropzone" id = "any_file">
						<div class="fallback">
							<input name="file" type="file" accept="image/*" id = "file_video" />
					</div>
				</div >	
				<div class="text-center"><br>
				<button class="btn_green" id = "add_any_file" data-file-ids = "">Add files</button>	
				</div> -->

				<div id="thumnail_edit">
					
				</div><br>
				<!-- <label for="">Files: <span id="count"></span> </label> <br>
				<div id = "download_section"></div>
				
				<br> -->

			</div>
			</div>
        </div>
        <div class='modal-footer'>
			<!-- <input type="submit" class="btn btn-success update" name="update" value="Update">			 -->
			<button  class="btn_blue " id="update_post_modal" data-id-update-post = "" > Update</button>
			<button  class="btn_red  " data-dismiss='modal'> Cancel</button>
		
			
        </div>
      </div>
    </div>
  </form>
</div>


<div class="modal fade" id='show_download_modal' tabindex="-1" role="dialog" aria-hidden="true" >
  <div class="modal-dialog ">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title text-center">Image</h4>
      </div>
      <div class="modal-body text-center" >
	<div id = "modal_show_image"></div>
       
      </div>
      <div class="modal-footer">
        <div class="col-xs-6 padding">
          <button type="button" id="btn_remove_confirm" class="btn btn-primary btn-sm col-xs-12 border_r_right_none" >Delete File</button>
      </div>
        <div class="col-xs-6 padding0">
          <button type="button" class="btn btn-danger btn-sm col-xs-12 border_r_left_none" data-dismiss="modal">Cancel</button>
      </div>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id='banner_settings' tabindex="-1" role="dialog" aria-hidden="true" >
  <div class="modal-dialog modal-lg ">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title text-center" >Banner Settings</h4>
      </div>
      <div class="modal-body text-center" >
	<div id = "banner_body">
		
	</div>       
      </div>
      <div class="modal-footer">

      </div>
    </div>
  </div>
</div>

<div class="modal fade" id='confirm_change_thumbnail' tabindex="-1" role="dialog" aria-hidden="true" >
  <div class="modal-dialog ">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title text-center">Are you sure you want to change the thumbnails?</h4>
      </div>
      <div class="modal-body text-center" >
	
       
      </div>
      <div class="modal-footer">
        <div class="col-xs-6 padding">
          <button type="button" id="btn_remove_confirm" class="btn btn-primary btn-sm col-xs-12 border_r_right_none" >Delete File</button>
      </div>
        <div class="col-xs-6 padding0">
          <button type="button" class="btn_red btn-sm col-xs-12 border_r_left_none" data-dismiss="modal">Cancel</button>
      </div>
      </div>
    </div>
  </div>
</div>


  <script>
Dropzone.autoDiscover = false;
  
  	CKEDITOR.replace('blog_desc',{
		filebrowserBrowseUrl : window.App.baseUrl+'/blog/browse/',
		filebrowserUploadUrl : window.App.baseUrl+'/post/upload_blog/',

		filebrowserWindowWidth : '640',
        filebrowserWindowHeight : '480',
        		    on: {
		        instanceReady: function() {
		            this.dataProcessor.htmlFilter.addRules({
		                elements: {
		                    img: function( el ) {
		                        el.addClass( 'img-responsive' );
		                    }
		                }
		            });            
		        }
		    }
	});

  </script>