
<style>
  #download_article a,#download_study a,#download_view a{
    color:#333;
  }
  #download_article,#download_study,#download_view{
    margin-top:-25px;
  }
</style>
 
  <div id="nav_ban" style="position:absolute;top:0;left:0;width:100%">
    <table id="link" cellspacing="0">
      <tr><td id = "student_click" class="active_nav" style="background:#77d0ee">趣味学习
      <td  id = "article_click" class="" style="background:#ffb1af">美图美文
      <td  id = "download_click" class="" style="background:#76e3bc">下载中心   
    </table>
  </div>
  <br><br><br>
  <input type="text"  hidden id = "page_view" value="1">
 <button  hidden id = "next_view">Test</button>

<div id="main_view" style= "display:none">
<div id="download_view">
<?php if (!empty($study)): ?>
  

<?php foreach ($study as $row): ?>  
<?php if ($row->blog_type == 'study'): ?>
 
    <div id="view_infos" style = "height:100px">
     <a href="<?php echo base_url().'blog/download/'.$row->id ?>">
     <div class="col-xs-3"><img src="<?php echo base_url().'uploads/post/'.$row->thumbnail?>" class = "img-responsive" style="width:100px"></div>      
      <div class="col-xs-7"><span style="font-weight:bold"><?php echo $row->blog_title?></span><br><span style="font-size:11px;color:#999"><?php echo $row->blog_sub_title?></span></div>
      </a>
     <div class="col-xs-2">
     <?php if (!empty($row->path_file)): ?>
        <a href="<?php echo base_url().'uploads/post/'.$row->path_file ?>" download><button class="view_download_files" data-id = "<?php echo $row->id?>" style="background:transparent;border:none;color:#77e3bc"><i class="fa fa-download" style="font-size:25px"></i></button></a>
        <?php endif ?>
      </div>
</div>
<?php endif ?>  
<?php endforeach ?>
<?php endif ?>
</div>
<div id="download_article">
<?php if (!empty($article)): ?>  

<?php foreach ($article as $row): ?>  
<?php if ($row->blog_type == 'article'): ?>
    <div id="view_infos" style = "height:100px"><a href="<?php echo base_url().'blog/download/'.$row->id ?>">
     <div class="col-xs-3"><img src="<?php echo base_url().'uploads/post/'.$row->thumbnail?>" class = "img-responsive" style="width:100px"></div>      
      <div class="col-xs-7"><span style="font-weight:bold"><?php echo $row->blog_title?></span><br><span style="font-size:11px;color:#999"><?php echo $row->blog_sub_title?></span></div></a>
     <div class="col-xs-2">
     <?php if (!empty($row->path_file)): ?>
        <a href="<?php echo base_url().'uploads/post/'.$row->path_file ?>" download><button class="view_download_files" data-id = "<?php echo $row->id?>" style="background:transparent;border:none;color:#77e3bc"><i class="fa fa-download" style="font-size:25px"></i></button></a>
        <?php endif ?>
      </div>
</div>
<?php endif ?>  
<?php endforeach ?> 
<?php endif ?>
</div>

<div id="download_study">
<?php if (!empty($download)): ?>  

<?php foreach ($download as $row): ?>  
<?php if ($row->blog_type == 'download'): ?>
    <div id="view_infos" style = "height:100px">
      <!-- <table>
      <tr><td><b><?php echo $row->blog_title?></b>
      <td align="right"><a href="<?php echo base_url().'uploads/post/'.$row->path_file ?>" download><button class="view_download_files" data-id = "<?php echo $row->id?>" style="background:transparent;border:none;color:#77e3bc"><i class="fa fa-download" style="font-size:25px"></i></button></a>
      <tr><td><span style="font-size:11px;color:#999"><?php echo $row->blog_sub_title?></span>
    <div><?php echo $row->blog_desc?> </div>
    </table> -->
<a href="<?php echo base_url().'blog/download/'.$row->id ?>">
     <div class="col-xs-3"><img src="<?php echo base_url().'uploads/post/'.$row->thumbnail?>" style="width:100px"  class = "img-responsive"  ></div>      
      <div class="col-xs-7"><span style="font-weight:bold"><?php echo $row->blog_title?></span><br><span style="font-size:11px;color:#999"><?php echo $row->blog_sub_title?></span></div></a>
     <div class="col-xs-2">
     <?php if (!empty($row->path_file)): ?>
        <a href="<?php echo base_url().'uploads/post/'.$row->path_file ?>" download><button class="view_download_files" data-id = "<?php echo $row->id?>" style="background:transparent;border:none;color:#77e3bc"><i class="fa fa-download" style="font-size:25px"></i></button></a>
        <?php endif ?>
      </div>
    
   <!-- <img src="<?php echo base_url().'uploads/post/'.$row->thumbnail?>  class = "img-responsive"  " style="width:100px"> -->
</div>
<?php endif ?>  
<?php endforeach ?>
<?php endif ?>
</div>

</div>


<div id="view_downloads" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Download Files</h4>
      </div>
      <div class="modal-body">
     Total Files: <span id = "counts" class="badge"></span>
       <div id="download_sec"></div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn_red" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>