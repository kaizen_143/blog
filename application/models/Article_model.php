<?php
defined('BASEPATH') or exit('No direct script access allowed foo!');

class Article_model extends CI_Model
{

	public function insert_article($data){
		$result = $this->db->insert('blog',$data);
		return $result ? $this->db->insert_id() : NULL;
	}

	public function attach_path($data){
		$result = $this->db->insert('post_attach',$data);
		return $result ;
	}

	public function article_info($id,$per_page = FALSE, $segment = FALSE,$emp_type='')
	{
		$this->db->select('*');
		$this->db->from('blog');
		$this->db->limit($per_page, $segment);		
		$this->db->where('user_id',$id);
		$this->db->where('type','article');
		$query = $this->db->get();
		return $query->num_rows() ? $query->result() : NULL;
	}

	public function count_article($id){
		$this->db->select('count(*) as num');
		$this->db->from('blog');				
		$this->db->where('user_id',$id);
		$this->db->where('type','article');
		$query = $this->db->get();
		return $query->num_rows() ? $query->row()->num : NULL;
	}

	public function delete($id){
		$this->db->where('id',$id);
		$result = $this->db->delete('blog');
		$this->db->where('blog_id',$id);
		$result = $this->db->delete('post_attach');
		return $result;

	}

	public function articleInfo($id){
		$this->db->select('g.id as blog_ids, g.date,g.date_released,g.blog_desc,g.blog_title,pa.path_file,g.status,pa.blog_id');
		$this->db->from('blog g');
		$this->db->join('post_attach pa', 'pa.blog_id=g.id','left');
		$this->db->where('g.id',$id);
		$query = $this->db->get();
		return $query->num_rows() ? $query->row() : NULL;


	}

		public function change_thumb($id){
		$this->db->where('id',$id);
		$data = array('status' => 0, );
		$this->db->update('blog',$data);
		$this->db->where('blog_id',$id);
		$result = $this->db->delete('post_attach');
		return $result;

	}

	public function change_status($id,$data){
		$this->db->where('id',$id);
		$query = $this->db->update('blog',$data);
		return $this->db->affected_rows() ? TRUE : FALSE;
	}

	public function getpublishpost()
	{
		$this->db->select('u.profile_pic,u.username,g.id as blog_id, g.date,g.date_released,g.blog_desc,g.blog_title,ga.path_file,g.status,ga.blog_id ');
		$this->db->from('blog g');
		$this->db->join('users u', 'g.user_id=u.id','left');
		$this->db->join('post_attach ga', 'ga.blog_id=g.id','left');		
		$this->db->where('g.status', 1);
		$this->db->where('g.type', 'article');
		
		$query = $this->db->get();
		return $query->num_rows() ? $query->result() : NULL;
	}
}