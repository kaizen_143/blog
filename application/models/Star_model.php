<?php
defined('BASEPATH') or exit('No direct script access allowed foo!');

class Star_model extends CI_Model
{
	
	public function add_star($data){
		$result = $this->db->insert('star',$data);
		return $result ? $this->db->insert_id() : NULL;

	}

	public function attach_path($data){
		$result = $this->db->insert('star_attach',$data);
		return $result ? $this->db->insert_id() : NULL;
	}

	public function show_video($offset,$star){
			$this->db->select('*');
			$this->db->from('star');
			$this->db->join('star_attach', 'star_attach.star_id = star.id');
			$this->db->order_by('order','asc');
			$this->db->limit(3,$offset);
			$this->db->where('status',1);
			$this->db->where('star_type',$star);
			$query = $this->db->get();
		  	return $query->result();

	}

	public function show_star_data($type,$user_id,$per_page = FALSE, $segment = FALSE){
		$this->db->select('*');
		$this->db->from('star');
		$this->db->limit($per_page, $segment);
		$this->db->where('user_id',$user_id);
		$this->db->where('star_type',$type);		
		$this->db->order_by('order','asc');
		$query = $this->db->get();
		return $query->num_rows() ? $query->result() : NULL;
	}

	public function getFileThumb()
	{
		$this->db->select('*');
		$this->db->from('star_attach');
		$query = $this->db->get()->result();
		
		$star_attach_id = array();

		foreach ($query as $key) {
			$star_attach_id[$key->star_id] = $key->filepath ;
		}

		return $star_attach_id;
		
	}

	public function count_data($type){
		$this->db->select('COUNT(*) as NUM');
		$this->db->from('star');
		$this->db->where('star_type',$type);
		$query = $this->db->get();
		return $query->num_rows() ? $query->row()->NUM : NULL;
	}

	

	public function show_details($id)
	{
		$this->db->select('*');
		$this->db->from('star s');
		$this->db->join('star_attach sa','s.id=sa.star_id','left');
		$this->db->where('s.id',$id);
		$query = $this->db->get();
		return $query->num_rows() ? $query->result()[0] : NULL;

	}

	public function update_star($id,$data)
	{
		$this->db->where('id',$id);
		$result = $this->db->update('star',$data);
		return $this->db->affected_rows() ? TRUE : FALSE;

	}

	public function delete_video($id,$data){
	
		$result = $this->db->delete('star_attach',$data);
		return $this->db->affected_rows() ? TRUE : FALSE;
	}

	public function delete_post($id,$data1,$data2){
		$this->db->delete('star',$data1);
		$this->db->delete('star_attach',$data2);
		return ($this->db->affected_rows() >=1) ? TRUE : FALSE;
	}
/*	public function delete_star($data){
		$result = $this->db->delete('star',$data);
		return $this->db->affected_rows() ? TRUE : FALSE;
	}*/

	public function getStarOrder(){
		$this->db->select('order');
		$this->db->from('star');
		$query = $this->db->get();
		return $query->num_rows() ? $query->result() : NULL;
	}

	public function count_info($type,$user_id){
		$this->db->select("COUNT(*) AS NUM");
		$this->db->from('star');	
		$this->db->where('user_id',$user_id);
		$this->db->where('star_type',$type);
		$query = $this->db->get();
		return $query->num_rows() ? $query->result()[0]->NUM : NULL;
	}







}