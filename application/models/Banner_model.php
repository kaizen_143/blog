<?php

Class Banner_model extends CI_Model {
        public function save($org_file_name, $file_name){
            $this->db->select('*');
            $this->db->from('banner');
            $query = $this->db->get()->result();
            if (count($query)>=3) {
                $display = 0;
            }else{
                 $display = 1;
            }
        $data = array(
            'org_file_name' => $org_file_name,
            'file_name' => $file_name,
            'display' => $display,
            'order' => 0,
            'date_added' => date('Y-m-d H:i:s')
        );

        $this->db->insert('banner', $data);
        echo json_encode(array("status"=>"success"));
    }

        public function get($per_page = FALSE, $segment = FALSE){
        $this->db->select('*');
        $this->db->from('banner');
        $this->db->order_by("date_added", "asc");
        $this->db->limit($per_page,$segment);

        return $this->db->get()->result_array();
    }

        public function getCount()
    {
        $this->db->select('COUNT(id) ref_count');
        $this->db->from('banner');
        $query=$this->db->get()->row();
        return $query->ref_count;
    }

        public function get_for_site(){
        $this->db->select('*');
        $this->db->from('banner');
        $this->db->where('display',1);
        $this->db->order_by("order");

        return $this->db->get()->result_array();
    }

        public function delete_banner($id)
    {
        $this->db->select('file_name');
        $this->db->from('banner');
        $this->db->where('id',$id);
        $query= $this->db->get()->row();

        // unlink($_SERVER['DOCUMENT_ROOT'] . '/assets/banners/' . $query->file_name))
        unlink(FCPATH . 'uploads/banners/' . $query->file_name);
        $this->db->delete('banner', array('id' => $id));
        echo json_encode(array("status"=>"success"));
    }

        public function display_banner($id, $data)
    {   
        $this->db->select('count(*) as num');
        $this->db->from('banner');
         $this->db->where('display', 1);
        $query=$this->db->get()->row()->num;
        
        if ($data == 1) {
            if ($query >=3) {
            $this->db->where('id', $id);
            $data = array('display' => 0, );
            $this->db->update('banner', array('display' => 0));
            echo json_encode(array("status"=>"not","msg"=>"Sorry Only 3 banners allowed to display!"));
            die();
              }
        }


        $this->db->where('id', $id);
        $this->db->update('banner', array('display' => $data));
        echo json_encode(array("status"=>"success"));
    }

        public function display_order()
    {
        $this->db->select('*');
        $this->db->from('banner');
        $this->db->where('display',1);
        $this->db->order_by("order");
        
        return $this->db->get()->result_array();
    }

        public function update_order($data)
    {
        $position = 0;
        foreach ($data as $value1) {
            $each = explode('&', $value1);
            foreach ($each as $value) {
                $id = substr($value, strpos($value, "c") + 1);
                // echo "id" . $id;
                $position++;
                // echo "position" . $position;
                $this->db->where('id', $id);
                $this->db->update('banner', array('order' => $position));
            }

            echo json_encode(array("status"=>"success"));
        }
    }




}

?>