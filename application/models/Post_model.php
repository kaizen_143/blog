<?php
defined('BASEPATH') or exit('No direct script access allowed foo!');

class Post_model extends CI_Model
{
	
	public function add_post($data){
		$result = $this->db->insert('blog',$data);
		return $result ? $this->db->insert_id() : NULL;

	}

	public function attach_path($data){
		$result = $this->db->insert_batch('post_attach',$data);
		return (!empty($result)) ? TRUE : FALSE;
	}

    //Maintenance View//
	public function view_posts($type,$user_id,$per_page = FALSE, $segment = FALSE,$emp_type=''){
		$this->db->select('*'); 
        $this->db->from('blog');
        $this->db->limit($per_page, $segment);
        $this->db->order_by("order");
        $this->db->order_by("date_released");
        $this->db->where('user_id',$user_id);
        $this->db->where('type',$type);      
       	$query = $this->db->get();
		return $query->num_rows() ? $query->result() : NULL;
	}

    public function count_data($type){
        $this->db->select('COUNT(*) as NUM');
        $this->db->from('blog');
        $this->db->where('type',$type);
        $query = $this->db->get();
        return $query->num_rows() ? $query->row()->NUM : NULL;
    }

	public function get_post($id){
		$this->db->select('b.thumbnail,b.blog_author,b.blog_url,b.blog_sub_title,b.status,b.id as blog_id,pa.thumb, pa.type, pa.path_file, b.blog_title, b.blog_url, b.blog_desc,pa.id as att_id '); 
        $this->db->from('blog b');
        $this->db->join('post_attach pa','b.id=pa.blog_id','left');       
        $this->db->where('b.id',$id);        
        $query = $this->db->get();
		return $query->num_rows() ? $query->result() : NULL;
    }

    public function update($id,$data)
    {
    	$this->db->where('id',$id);
    	$this->db->update('blog',$data);
    	return $this->db->affected_rows() ? TRUE : FALSE;
    }

    public function delete_post($id)
    {
    	$this->db->where('id',$id);
    	$this->db->delete('blog');
    	return $this->db->affected_rows() ? TRUE : FALSE;
    }
    public function get_att($id)
    {
    	$this->db->select('*'); 
        $this->db->from('post_attach');       
        $this->db->where('blog_id',$id);
        $query = $this->db->get();
		return $query->num_rows() ? $query->result() : NULL;
    }

    public function view_image($id)
    {
    	$this->db->select('path_file'); 
        $this->db->from('post_attach');       
        $this->db->where('id',$id);
        $this->db->where('type','any');
        $query = $this->db->get();
		return $query->num_rows() ? $query->result() : NULL;
    }

    public function change_thumbnails($file_id){
        $this->db->select('*');
        $this->db->from('post_attach');
        $this->db->where('id',$file_id);
        $result = $this->db->get()->result()[0]->path_file;
         $this->db->where('id',$file_id);
         $this->db->delete('post_attach');
        return $result ;
    }

    public function change_thumbnails_download($file_id){
        $this->db->select('*');
        $this->db->from('blog');
        $this->db->where('id',$file_id);
        $result = $this->db->get();
         return $result->num_rows() ? $result->row()->thumbnail : FALSE;
       
    }    



    public function GetFiles($id)
    {
        $this->db->select('path_file');
        $this->db->from('post_attach');
        $this->db->where('blog_id',$id);
        $this->db->where('type','any');
         $query = $this->db->get();
        return $query->num_rows() ? $query->result() : NULL;    
    }

    public function count_info($user_id,$type){
        $this->db->select('COUNT(*) as num'); 
        $this->db->from('blog');    
        // $this->db->order_by("id");
        // $this->db->where('user_id',$user_id);
        $this->db->where('type',$type);
        $query = $this->db->get();
        return $query->num_rows() ? $query->row()->num : NULL;
    }

    public function checkthumb($id)
    {
        $this->db->select('COUNT(*) as num'); 
        $this->db->from('post_attach');
        $this->db->where('thumb',1);
        $this->db->where('blog_id',$id);               
        $query = $this->db->get();
        return $query->num_rows() ? $query->row()->num : NULL;
    }

        public function download_checkthumb($id)
    {
        $this->db->select('COUNT(*) as num'); 
        $this->db->from('blog');
        $this->db->where('thumbnail !=',"");
        $this->db->where('id',$id);               
        $query = $this->db->get();
        return $query->num_rows() ? $query->row()->num : NULL;
    }

    public function checkblogUrl($url)
    {
        $this->db->select('COUNT(*) as num'); 
        $this->db->from('blog');   
        $this->db->where('blog_url',$url);               
        $query = $this->db->get();
        return $query->num_rows() ? $query->row()->num : NULL;
    }




}