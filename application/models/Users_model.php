<?php
defined('BASEPATH') or exit('No direct script access allowed foo!');

class Users_model extends CI_Model
{
	public function add($data){
		$result = $this->db->insert('users',$data);
		return $this->db->affected_rows() ? TRUE : FALSE;
	}

	public function get_users($username){
			$this->db->select('*');
			$this->db->from('users');
			$this->db->where('username',$username);
			$query = $this->db->get();
		return $query->num_rows() ? $query->result()[0] : NULL;
	}

	public function getUserData($id)
	{
		$this->db->select('*');
		$this->db->from('users');
		$this->db->where('id',$id);
		$query = $this->db->get();
		return $query->num_rows() ? $query->result()[0] : NULL;
	}
	public function profile_change($id,$data)
	{
		$this->db->where('id',$id);
		$this->db->update('users',$data);
		return $this->db->affected_rows() ? TRUE : FALSE;
	}

	public function update_profile()
	{
		
	}

}
	
