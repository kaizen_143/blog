<?php
defined('BASEPATH') or exit('No direct script access allowed foo!');

class Blog_model extends CI_Model
{
	
	public function ViewListBlog() 
	{
		$date_today = $date_today = gmdate('Y-m-d', strtotime('now'));
		$this->db->select('b.id,b.date,b.blog_title,b.blog_url,b.blog_desc,b.display,pa.path_file,b.display,b.date_released,pa.type,u.profile_pic');
		$this->db->from('blog b');
		$this->db->join('post_attach pa','b.id=pa.blog_id','left');
		$this->db->join('users u','b.user_id=u.id','left');
		$this->db->where('b.status',1);
		$this->db->where('pa.thumb',1);
		$this->db->limit(3);
		$this->db->order_by('order','asc');
		$this->db->order_by('date_released');
		$query = $this->db->get();
		return $query->num_rows() ? $query->result() : NULL;
	}

		public function clickedBlog($id)
	{
		$date_today = $date_today = gmdate('Y-m-d', strtotime('now'));
		$this->db->select('b.id,b.blog_title,b.blog_url,b.blog_desc,b.display,pa.path_file,b.display,b.date_released');
		$this->db->from('blog b');
		$this->db->join('post_attach pa','b.id=pa.blog_id','left');
		$this->db->where('b.id',$id);
		$this->db->order_by('order','asc');
		$query = $this->db->get();
		return $query->num_rows() ? $query->result()[0] : NULL;
	}

	public function viewBlog($id){
		$this->db->select('*');
		$this->db->from('blog b');
		$this->db->join('post_attach pa','pa.blog_id=b.id');
		$this->db->where('b.id',$id);
		$query = $this->db->get();
		return $query->num_rows() ? $query->result()[0] : NULL;
	}

	    public function getPublish($type){
       	$this->db->select('b.blog_title,b.blog_desc,b.date_released,u.username,u.profile_pic,b.id'); 
        $this->db->from('blog b');
        $this->db->join('users u','b.user_id=u.id');
        $this->db->where('status',1);   
        $this->db->where('type',$type);      
        $query = $this->db->get();
        return $query->num_rows() ? $query->result() : NULL;
    }
    public function getDownload($limit,$offset)
    {
    	$this->db->select('pa.path_file,b.thumbnail,b.blog_title,b.blog_desc,b.date_released,b.id,b.type as blog_type,b.blog_sub_title'); 
        $this->db->from('blog b');         
        $this->db->join('post_attach pa','b.id=pa.blog_id','left');         
        $this->db->where('b.type','download');     
 		$this->db->limit($limit,$offset);
        $this->db->where('status',1);       
         $this->db->order_by('order','asc');         
        $this->db->group_by('b.id');       
        $query = $this->db->get();
        return $query->num_rows() ? $query->result() : NULL;
    }
    public function getAjaxDownload($type,$limit,$offset)
    {
    	$this->db->select('pa.path_file,b.thumbnail,b.blog_title,b.blog_desc,b.date_released,b.id,b.type as blog_type,b.blog_sub_title'); 
        $this->db->from('blog b');         
        $this->db->join('post_attach pa','b.id=pa.blog_id','left');         
        $this->db->where('b.type',$type); 
        $this->db->where('status',1);   	      
        $this->db->order_by('order','asc');
        $this->db->limit($limit,$offset);    
        $this->db->group_by('b.id');    
        $query = $this->db->get();
        return $query->num_rows() ? $query->result() : NULL;
    }

        public function getArticle($limit,$offset)
    {
    	$this->db->select('pa.path_file,b.thumbnail,b.blog_title,b.blog_desc,b.date_released,b.id,b.type as blog_type,b.blog_sub_title'); 
        $this->db->from('blog b');         
        $this->db->join('post_attach pa','b.id=pa.blog_id','left');  
        $this->db->where('b.type','article'); 
        $this->db->limit($limit,$offset);
        $this->db->where('status',1);       
        $this->db->group_by('b.id');       
        $query = $this->db->get();
        return $query->num_rows() ? $query->result() : NULL;
    }

        public function getStudy($limit,$offset)
    {
    	$this->db->select('pa.path_file,b.thumbnail,b.blog_title,b.blog_desc,b.date_released,b.id,b.type as blog_type,b.blog_sub_title'); 
        $this->db->from('blog b');         
        $this->db->join('post_attach pa','b.id=pa.blog_id','left');        
      	$this->db->where('b.type','study'); 
      	$this->db->limit($limit,$offset);
        $this->db->where('status',1);       
        $this->db->group_by('b.id');       
        $query = $this->db->get();
        return $query->num_rows() ? $query->result() : NULL;
    }

	public function getpublishpost($key = "",$type)
	{
		$this->db->select('u.profile_pic,u.username,g.id as blog_id, g.date,g.date_released,g.blog_desc,g.blog_url,g.blog_title,ga.path_file,g.status,ga.blog_id ');
		$this->db->from('blog g');
		$this->db->join('users u', 'g.user_id=u.id','left');
		$this->db->join('post_attach ga', 'ga.blog_id=g.id','left');		
		$this->db->where('g.status', 1);
		$this->db->where('g.type', $type);
		if (!empty($key)) {
			$this->db->like($key);
		}
		 

		
		$query = $this->db->get();
		return $query->num_rows() ? $query->result() : NULL;
	}

	public function show_star_student($star, $segment = FALSE){
			$this->db->select('*');
			$this->db->from('star');
			$this->db->join('star_attach', 'star_attach.star_id = star.id');
			$this->db->order_by('order','asc');
			$this->db->limit(3, $segment);
			$this->db->where('status',1);
			$this->db->like('star_type',$star);
			$query = $this->db->get();
		  	return $query->result();

	}

	public function count_student()
	{
		$this->db->select('*');
			$this->db->from('star');
			$this->db->join('star_attach', 'star_attach.star_id = star.id');
			$this->db->order_by('order','desc');
			$this->db->where('status',1);
			$this->db->like('star_type','Student');
			$query = $this->db->get();
		  	return $query->result();
	}

	public function getStarData($limit,$offset,$type)
	{
			$this->db->select('*');
			$this->db->from('star');
			$this->db->join('star_attach', 'star_attach.star_id = star.id');			
			$this->db->limit($limit,$offset);
			$this->db->where('status',1);
			$this->db->where('star_type',$type);
			$this->db->order_by('order','asc');
			$query = $this->db->get();
		  	return $query->result();
	}

	public function viewArticle($url,$type)
	{
		$this->db->select('*');
		$this->db->from('blog');
		$this->db->where('id',$url);
		$this->db->order_by('order','asc');
		// $this->db->where('type',$type);
		$query = $this->db->get();
		return $query->num_rows() ? $query->row(): NULL;
	
	}

	public function getDownloadFile($url)
	{
		$this->db->select('*');
		$this->db->from('post_attach');
		$this->db->where('blog_id',$url);
		$this->db->where('type','any');

		// $this->db->where('type',$type);
		$query = $this->db->get();
		return $query->num_rows() ? $query->row(): NULL;
	}

	public function get_post_data($type,$limit,$offset)
	{

		$this->db->select('g.blog_author,g.id as blog_id, g.date,g.date_released,g.blog_desc,g.blog_url,g.blog_title,ga.path_file,g.status,ga.blog_id ');
		$this->db->from('blog g');
		$this->db->where('g.type',$type);
		$this->db->where('g.status',1);
		$this->db->join('post_attach ga', 'ga.blog_id=g.id','left');		
		$this->db->limit($limit,$offset);
		$this->db->order_by('order','asc');
		$query = $this->db->get();
		return $query->num_rows() ? $query->result() : NULL;
	}

	public function banner(){
		$this->db->select('file_name,org_file_name');
		$this->db->from('banner');
		$this->db->where('display',1);
		$this->db->order_by('order');
		$query = $this->db->get();
		return $query->num_rows() ? $query->result() : NULL;
	}



}