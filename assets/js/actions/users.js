
$(function() {


	$('#username,#password').keypress(function (e) {
	
		if (e.which == 13) {
			$('#login').click();
			return false;   
		}
	});


	$('#user_add').click(function(e){
		e.preventDefault();
		var first_name =$('#first_name').val();
		var last_name = $('#last_name').val();
		var email =$('#email').val();
		var user = $('#user').val();
		var pass = $('#pass').val();
		var con_pass = $('#con_pass').val();

		var message ="";

	if(first_name == "" || last_name == "" || email == "" || user == "" || pass == "" || con_pass == ""){
		if(first_name == ""){
			message += "<span>First Name is required</span></br>"
		} 
		if(last_name == ""){
		message += "<span>Last Name is required</span></br>"
		} 
		if(email == ""){
		message += "<span>Email is required</span></br>"
		} 
		if(user == ""){
		message += "<span>Username is required</span></br>"
		} 
		if(pass  == ""){
		message += "<span>Password  is required</span></br>"
		} 
		if(con_pass == ""){
		message += "<span>Confirm Password is required</span></br>"
		} 
				new PNotify({
					delay: 4000,
					hide: true,
					title: 'Message',
					text: message,
					type: 'warning'
				});

			return
	}

		if(pass != con_pass){
			message +="Pasword and confirm password dont match"
			new PNotify({
					delay: 4000,
					hide: true,
					title: 'Message',
					text: message,
					type: 'warning'
				});

			return
		}
		
		$.ajax({
			type:'POST',
			url: window.App.baseUrl + 'users/add_user',
			dataType:'json',
			data:{first_name:first_name, last_name:last_name, email:email, user:user, pass:pass},
			cache:!1,

			success:function(e){

				if(e.success){
					new PNotify({
					delay: 4000,
					hide: true,
					title: 'Message',
					text: e.msg,
					type: 'success'
				});
			 }else{
			 	new PNotify({
					delay: 4000,
					hide: true,
					title: 'Message',
					text: e.msg,
					type: 'warning'
				});
			 }
				
			},
			error:function(e){

			}
		});		

	})
});

 	$(document).ready(function(){
 		$("#sign").click(function(){
 			$("#sign").hide();
 			$("#log").show();
 			$("#head_logo").fadeOut();
 			$(".log").stop().fadeOut(function(){
 				$(".signup").stop().fadeIn();
 				$("#upload_img").stop().fadeIn();
 			});
 		});
 		$("#log").click(function(){
 			$("#log").hide();
 			$("#sign").show();
 			$("#head_logo").stop().fadeIn();
 			$("#upload_img").stop().fadeOut();
 			$(".signup").stop().fadeOut(function(){
 				$(".log").stop().fadeIn();
 			});
 		});
 	});