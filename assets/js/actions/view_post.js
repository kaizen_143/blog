
$('#open_banner').click(function(e){
    e.preventDefault(e)
        // PopupCenter(window.App.baseUrl+'admin/banner','Banner Settings','900','500'); 
        $('#banner_body').load(window.App.baseUrl+'admin/banner');
        $('#banner_settings').modal('show');
});

function PopupCenter(url, title, w, h) {
    // Fixes dual-screen position                         Most browsers      Firefox
    var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;
    var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;

    var width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
    var height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

    var left = ((width / 2) - (w / 2)) + dualScreenLeft;
    var top = ((height / 2) - (h / 2)) + dualScreenTop;
    var newWindow = window.open(url, title, 'postwindow,scrollbars=yes, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);

   
    if (window.focus) {
        newWindow.focus();
    }
}

if (!$('div#star_videos').length == 0) {
Dropzone.autoDiscover = false;
  var myDropzone = new Dropzone('div#star_videos',  { 
    url: window.App.baseUrl+'star/upload_file',
    autoProcessQueue: false,
    parallelUploads: 10,    
    thumbnailWidth: 250,
    thumbnailHeight: 250,
    addRemoveLinks: true,
    maxFiles: 1,   
    maxFilesize: 0.200, 
    uploadMultiple: false,
     acceptedFiles: 'video/*',
    init: function() {
      this.on("success", function(file, responseText) {

        new PNotify({
          delay: 4000,
          hide: true,
          title: 'Message',
          text: 'Success!',
          type: 'success'
        });

            setTimeout(function(){ 
               location.reload()
             }, 3000);  
        });
      this.on("error", function(file, message) { 
        new PNotify({
          delay: 4000,
          hide: true,
          title: 'Message',
          text: message,
          type: 'error'
        });
        this.removeFile(file); 
      });

    },    
    maxfilesexceeded: function(file) {
      this.removeAllFiles();
      this.addFile(file);
    },
  }); 
}
  function post_video(id,type)
  {
    if (myDropzone.files.length > 0) {

     myDropzone.options.url = window.App.baseUrl+'post/upload_file/'+id+"/"+type
      //myDropzone.options.url = 'upload_file/'+id

      if (myDropzone.getQueuedFiles().length > 0)
      {
        myDropzone.processQueue();
      }
      else
      {
        myDropzone.uploadFiles([]);
      }

      myDropzone.removeAllFiles();
    }


  }

if(!$('div#any_file').length == 0) {
Dropzone.autoDiscover = false;
  var myDropzone3 = new Dropzone('div#any_file',  { 
    url: window.App.baseUrl+'star/upload_file',
    autoProcessQueue: false,
    parallelUploads: 10,    
    thumbnailWidth: 250,
    thumbnailHeight: 250,
    addRemoveLinks: true,  
    uploadMultiple: false,
   maxFilesize: 0.200, 
    init: function() {
      this.on("success", function(file, responseText) {

  var stat = JSON.parse(responseText);

        if(stat.status == "success")
        localStorage.setItem('response',"success");

           
        });
      this.on("error", function(file, message) { 
        new PNotify({
          delay: 4000,
          hide: true,
          title: 'Message',
          text: message,
          type: 'error'
        });
        this.removeFile(file); 
      });

    },    
    maxfilesexceeded: function(file) {
      this.removeAllFiles();
      this.addFile(file);
    },
  }); 
}
  function any_file(id,type)
  {
    if (myDropzone3.files.length > 0) {

     myDropzone3.options.url = window.App.baseUrl+'post/upload_file/'+id+"/"+type
      //myDropzone3.options.url = 'upload_file/'+id

      if (myDropzone3.getQueuedFiles().length > 0)
      {
        myDropzone3.processQueue();
      }
      else
      {
        myDropzone3.uploadFiles([]);
      }

      myDropzone3.removeAllFiles();
    }


  }

  Dropzone.autoDiscover = false;
  var myDropzone2 = new Dropzone('div#star_photo',  { 
    url: window.App.baseUrl+'star/upload_file',
    autoProcessQueue: false,
    parallelUploads: 10,    
    thumbnailWidth: 250,
    thumbnailHeight: 250,
    addRemoveLinks: true,
       acceptedFiles: 'image/*',
    maxFiles: 1,   
   maxFilesize: 0.200, 
    uploadMultiple: false,
    init: function() {
      this.on("success", function(file, responseText) {

        new PNotify({
          delay: 4000,
          hide: true,
          title: 'Message',
          text: 'Uploading Success!',
          type: 'success'
        });

            setTimeout(function(){ 
               location.reload()
             }, 3000);  
        });
      this.on("error", function(file, message) { 
        new PNotify({
          delay: 4000,
          hide: true,
          title: 'Message',
          text: message,
          type: 'error'
        });
        this.removeFile(file); 
      });

    },    
    maxfilesexceeded: function(file) {
      this.removeAllFiles();
      this.addFile(file);
    },
  }); 

  function post_photo(id,type)
  {
    if (myDropzone2.files.length > 0) {

     myDropzone2.options.url = window.App.baseUrl+'post/upload_file/'+id+"/"+type
      //myDropzone.options.url = 'upload_file/'+id

      if (myDropzone2.getQueuedFiles().length > 0)
      {
        myDropzone2.processQueue();
      }
      else
      {
        myDropzone2.uploadFiles([]);
      }

      myDropzone2.removeAllFiles();
    }


  }  

$('.order_by').change(function(e){
  var id = $(this).parent().parent().attr('data-id');
  var order_by = $(this).val();

  $.ajax({
        url:window.App.baseUrl+'post/update_order',
        type:'post',
        dataType:'json',
        data:{id:id,order_by:order_by},
        cache:!1,        
        success: function(e){          
           console.log(e);
           if (e.success) {
            new PNotify({
              delay: 4000,
              hide: true,
              title: 'Message',
              text: e.msg,
              type: 'success'
            });
           }
        },
        error:function(e){

        }
    });
});

$('#post_status').change(function(e){
  var id = $(this).attr('data-id-status');
  var status = $(this).val();

if($(this).val()==2){
  $('#schedule_post').modal('show');
  $('#save_schedule_post').attr('data-id-modal',id)
  return
}
  $.ajax({
        url:window.App.baseUrl+'post/update_status',
        type:'post',
        dataType:'json',
        data:{id:id,status:status},
        cache:!1,        
        success: function(e){          
           console.log(e);
           if (e.success) {
            new PNotify({
              delay: 4000,
              hide: true,
              title: 'Message',
              text: e.msg,
              type: 'success'
            })
            setTimeout(function(){ 
              location.reload()
             }, 2000);  
            
           }else{
            $('#star_photo').css('border-color','red');
            new PNotify({
              delay: 4000,
              hide: true,
              title: 'Message',
              text: e.msg,
              type: 'warning'
            })
            
            

           }
          // var test = $('tbody').find("tr[data-id ="+id+"]");
          //   console.log(test);
          //  $('.post_status').parent().parent().find("td[data-publish ="+id+"]").text(e.date);
          
        },
        error:function(e){

        }
    });
});

$('#save_schedule_post').click(function(e){
  
  var id = $(this).attr('data-id-modal')
  var schedule_post = $('#schedule_post_input').val();
  var status = 2;
    $.ajax({
        url:window.App.baseUrl+'post/update_status',
        type:'post',
        dataType:'json',
        data:{id:id,schedule_post:schedule_post,status:status},
        cache:!1,        
        success: function(e){          
           console.log(e);
           if (e.success) {
            new PNotify({
              delay: 4000,
              hide: true,
              title: 'Message',
              text: e.msg,
              type: 'success'
            });
           }
           setTimeout(function(){ 
              location.reload()
             }, 2000);  

        },
        error:function(e){

        }
    });

})

$('.open_confirm_delete_modal').click(function(e){
    var id = $(this).parent().parent().attr('data-id');
    $('#btn_delete_modal').attr('data-id-delete',id);
    $('#confirm_delete_post').modal('show');
});

$('#btn_delete_modal').click(function(e){
  e.preventDefault()
  var id = $(this).attr('data-id-delete');
    $.ajax({
        url:window.App.baseUrl+'post/delete_post',
        type:'post',
        dataType:'json',
        data:{id:id},
        cache:!1,        
        success: function(e){          
           console.log(e);
           if (e.success) {
            new PNotify({
              delay: 4000,
              hide: true,
              title: 'Message',
              text: e.msg,
              type: 'success'
            });

           }

          setTimeout(function(){ 
              location.reload()
             }, 2000);  
        },
        error:function(e){

        }
    });
})

$(document).ready(function(){
      $( ".date_publish" ).datepicker();
     $('#schedule_post_input').datepicker({
      format: "yyyy-mm-dd"      
    
    });

  
})

$('#update_post_modal').click(function(e){
  e.preventDefault()
  var id = $(this).attr('data-id-update-post');
 var post_title = $('#post_title').val();
 var post_author = $('#post_author').val();
 var post_sub_title = $('#post_sub_title').val();
 var blog_desc =CKEDITOR.instances['blog_desc'].getData();



     $.ajax({
        url:window.App.baseUrl+'post/edit_post_details',
        type:'post',
        dataType:'json',
        data:{id:id,post_title:post_title,post_author:post_author, post_sub_title:post_sub_title, blog_desc:blog_desc}, 
        cache:!1,
        success: function(e){          
           console.log(e);
           if (e.success) {
            new PNotify({
              delay: 4000,
              hide: true,
              title: 'Message',
              text: e.msg,
              type: 'success'
            });

           }

           setTimeout(function(){ 
              location.reload()
             }, 2000);  
        },
        error:function(e){

        }
    });

});

$('.view_post_details').click(function(e){
 e.preventDefault(e)
 var id = $(this).parent().parent().attr('data-id');
 var item = "";
 var thumbnail = "";
 var count = 0 ;
 var number_of_files = "";

  $('#update_post_modal').attr('data-id-update-post',id);
  $.ajax({
        url:window.App.baseUrl+'post/edit_post',
        type:'post',
        dataType:'json',
        data:{id:id},
        cache:!1,        
        success: function(e){
        console.log(e);
              $.each(e, function(index, data) {
                 $('#post_preview').attr('href',window.App.baseUrl+'blog/post/'+id);
               if(data.thumb == 1 && data.type == "photo"){
                $('#select_thumbnails').hide();
                thumbnail += '<button class="btn btn-primary" id = "change_thumbnails">Change Post thumbnail</button><button style= "display:none" data-att-id = "'+data.att_id+'"  data-id-blog = "'+data.blog_id+'"  id="yes_change_thumb" class = "btn btn-danger" >Yes</button><button style= "display:none" id="no_change_thumb" class = "btn btn-success">No</button><img id = "video_thumbs"  style = "width:100%" src="'+window.App.baseUrl+'uploads/post/'+data.path_file+'" alt="" />';
                $('#no_dropzone').hide();
               }else{
                $('#no_dropzone').show();
               }

               if(data.thumb == 1 && data.type == "video"){
                $('#select_thumbnails').hide();                
                thumbnail += '<button class="btn btn-primary" id = "change_thumbnails">Change Post thumbnail</button>'
                thumbnail += '<button style= "display:none" data-att-id = "'+data.att_id+'" data-id-blog = "'+data.blog_id+'" id="yes_change_thumb" class = "btn btn-danger" >Yes</button>'
                thumbnail +='<button style= "display:none" id="no_change_thumb" class = "btn btn-success">No</button>'
                thumbnail +='<video id = "video_thumbs" autoplay  src="'+window.App.baseUrl+'uploads/post/'+data.path_file+'" type="video/mp4" style = "width:100%"></video>'
    
               }else{
                // thumbnail += '<button></button>'
               
               }
               if(data.path_file != null){
                item += "<a style='margin:5px' class = 'btn btn-primary view_image download_file' href='"+window.App.baseUrl+'uploads/post/'+data.path_file+"' data-att-id = '"+data.att_id+"' download>"+data.path_file+" </a>";
                count = count+1;
               }
            
          });



          number_of_files += '<span class = "badge">'+count+'</span>'
          var status_text = e[0].status;
          if (status_text == 0) {
            status_text = "Draft";
          }

          if (status_text == 1) {
            status_text = "Published";
          }
          $('#count').html(number_of_files);
          $('#download_section').html(item);
          $('#thumnail_edit').html(thumbnail);
          $('#post_author').val(e[0].blog_author)
          $('#modal_update').modal('show');
          $('#post_status').attr('data-id-status',id)        
          $('#post_status_title').html(status_text)
          $('#download_all_file').attr('data-file-download',id);
          $('#upload_new_thumb').attr('data-file-id',id)
          $('#add_any_file').attr('data-file-ids',id)
          $('#post_title').val(e[0].blog_title);
          $('#post_sub_title').val(e[0].blog_sub_title);
          CKEDITOR.instances['blog_desc'].setData(e[0].blog_desc);       



           bindevent();
        },
        error:function(e){

        }
    });


  
});


 function bindevent(){
  $('#add_any_file').click(function(e){
    e.preventDefault()
    var id = $(this).attr('data-file-ids');
  any_file(id,'any');
            if(localStorage.getItem('response') == "success"){
            localStorage.removeItem('response');

                new PNotify({
                  delay: 4000,
                  hide: true,
                  title: 'Message',
                  text: 'Thumbnail Uploading Success!',
                  type: 'success'
                });

                    setTimeout(function(){ 
                        location.reload();
                     }, 3000);  
          }else{
              // new PNotify({
              //     delay: 4000,
              //     hide: true,
              //     title: 'Message',
              //     text: 'Please add a file',
              //     type: 'warning'
              //   });
          }
  })

 $('#upload_new_thumb').unbind("click").click(function(e) {
  e.preventDefault();
  var id = $(this).attr('data-file-id');
// post_video(id,'video')
post_photo(id,'photo')

}); 

    $('.download_files').unbind("click").click(function(e) {
      e.preventDefault()
      var id = $(this).attr('data-att-id');
      $.ajax({
        url:window.App.baseUrl+'post/download_file',
        type:'post',
        dataType:'json',
        data:{id:id},
        cache:!1,        
        success: function(e){          
           console.log(e);
          // $('#show_download_modal').modal('show');
          // $('#modal_show_image').html('<img style = "width:100%" src="'+window.App.baseUrl+'uploads/post/'+e.path_file+'" alt="" />')
         
        },
        error:function(e){

        }
    });
  }) 

    $('#change_thumbnails').unbind("click").click(function(e) {
      e.preventDefault()
      
      $(this).hide();

$('#yes_change_thumb').show();
$('#no_change_thumb').show();
      

  }) 


$('#yes_change_thumb').click(function(e){
  e.preventDefault();
  var file_id = $(this).attr('data-att-id');
  var id = $(this).attr('data-id-blog');
  $('#select_thumbnails').show();
  $('#video_thumbs').hide();
  $('#yes_change_thumb').hide();
  $('#no_change_thumb').hide();
  $('#change_thumbnails').hide();

      $.ajax({
        url:window.App.baseUrl+'post/change_thumbnails',
        type:'post',
        dataType:'json',
        data:{file_id:file_id,id:id},
        cache:!1,        
        success: function(e){          
           console.log(e);


        if(e.success){

        $('#yes_change_thumb').hide();
        $('#no_change_thumb').hide();
        $('#change_thumbnails').hide();
        $('#no_dropzone').show();

        }
         
        },
        error:function(e){

        }
        
    });

});


$('#no_change_thumb').click(function(e){
  e.preventDefault();
$('#no_change_thumb').hide();
$('#yes_change_thumb').hide();
  $('#change_thumbnails').show();
}); 
    



 }

 // $('#select_thumbnails').change(function(e){
 //  var type = $(this).val();
 //  if (type == 0) {
 //    $('#star_videos').show(); 
 //    $('#star_photo').hide(); 
 //   $('#no_dropzone').show();
 //  }
 //  if(type == 1) {
 //     $('#star_videos').hide(); 
 //    $('#star_photo').show();
 //  $('#no_dropzone').show();
 //  }
 
  
 // })

