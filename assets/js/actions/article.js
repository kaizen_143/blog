$(function() {

	$('#submit_article').click(function(e){
		e.preventDefault()
		
		var status = $('#status').val();
		var article_title = $('#t_post').val();
		var sub_title = $('#sub_title').val();
		var article_link = $('#link_post').val();
		var article_desc  = CKEDITOR.instances['article_desc'].getData();		
		var error = "";
		
		if ( article_desc == "" || !$("#thumb").hasClass("dz-started" ) || !$("#article_dropzone").hasClass("dz-started" ) || article_title == "" || sub_title == "") 
		{			
			if(!$("#thumb").hasClass("dz-started" )){
				error += "Thumbnail is missing</br>";
			}

			if(!$("#article_dropzone").hasClass("dz-started" )){
				error += "File is missing</br>";
			}

			if(article_desc == ""){
			error += "Description is missing</br>";

			}
			if(article_title == ""){
			error += "Title is missing</br>";

			}

			if(sub_title == ""){
			error += "Sub Title is missing</br>";

			}
				new PNotify({
					delay: 4000,
					hide: true,
					title: 'Message',
					text: error,
					type: 'warning'
				});
				return
		}else{
			$(this).attr('disabled',true);
			var type = 'article';
			$.ajax({
				url: window.App.baseUrl + 'article/add_new_article',
				type:'post',
				dataType:'json',
				cache:!1,
				data:{type:type,article_desc:article_desc,sub_title:sub_title,status:status,article_title:article_title,article_link:article_link},
				success:function(e){
					console.log(e)
					article_upload(e.id);	
					upload_thumb(e.id)				
				
				},
				error:function(e){
					$(this).attr('disabled',false);
				}

			});
		}
	});

$('#upload_attachment_new').click(function(e){
	var id = $('#article_id_post').val();
	article_upload(id);
});

if(!$('#article_dropzone').length == 0) {
  Dropzone.autoDiscover = false;
	var myDropzone = new Dropzone('#article_dropzone',  { 
		url: window.App.baseUrl+'article/upload_file',
		autoProcessQueue: false,
		parallelUploads: 10,		
		thumbnailWidth: 250,
		thumbnailHeight: 250,
		addRemoveLinks: true,
		maxFiles: 1,   
		acceptedFiles: 'image/*',
		uploadMultiple: false,
		init: function() {
			this.on("success", function(file, responseText) {
				new PNotify({
					delay: 4000,
					hide: true,
					title: 'Message',
					text: 'Success',
					type: 'success'
				});

            setTimeout(function(){ 
                window.location = window.App.baseUrl+"article/";
             }, 3000);  
        });
			this.on("error", function(file, message) { 
				new PNotify({
					delay: 4000,
					hide: true,
					title: 'Message',
					text: message,
					type: 'error'
				});
				this.removeFile(file); 
			});

		},    
		maxfilesexceeded: function(file) {
			this.removeAllFiles();
			this.addFile(file);
		},
	});	
}

if(!$('#thumb').length == 0) {
     Dropzone.options.myDropzone2 = false;
  var myDropzone3 = new Dropzone('#thumb', {     
    url: window.App.baseUrl+'download/upload_file',
    autoProcessQueue: false,
    parallelUploads: 10,
    maxFiles: 1,   
    thumbnailWidth: 250,
    thumbnailHeight: 250,
    addRemoveLinks: true,      
    uploadMultiple: true,
    init: function() {
      this.on("success", function(file, responseText) {
        
        var stat = JSON.parse(responseText);

        if(stat.status == "success"){
        localStorage.setItem('response',"success");
        new PNotify({
                  delay: 4000,
                  hide: true,
                  title: 'Message',
                  text: 'Thumbnail Uploading Success!',
                  type: 'success'
                });
                    setTimeout(function(){ 
                        location.reload();
                     }, 3000);  
        }

        });
      this.on("error", function(file, message) { 
        new PNotify({
          delay: 4000,
          hide: true,
          title: 'Message',
          text: message,
          type: 'error'
        });
        this.removeFile(file); 
      });

    },    
    maxfilesexceeded: function(file) {
      this.removeAllFiles();
      this.addFile(file);
    },

  });

  }


  function upload_thumb(id)
  {
    if (myDropzone3.files.length > 0) {
      myDropzone3.options.url = window.App.baseUrl+'/download/upload_file/'+id+"/photo" ;
      if (myDropzone3.getQueuedFiles().length > 0)
      {
        myDropzone3.processQueue();
      }
      else
      {
        myDropzone3.uploadFiles([]);

          
      }

      myDropzone3.removeAllFiles();
    }


  }



	function article_upload(id)
	{
		if (myDropzone.files.length > 0) {
		
			myDropzone.options.url = window.App.baseUrl+'article/upload_file/'+id+'/photo'
			//myDropzone.options.url = 'upload_file/'+id

			if (myDropzone.getQueuedFiles().length > 0)
			{
				myDropzone.processQueue();
			}
			else
			{
				myDropzone.uploadFiles([]);
			}

			myDropzone.removeAllFiles();
		}


	} 
	$('.article_delete').click(function(e){
		var id = $(this).attr('data-article-id');
		$('#yes_delete').attr('data-article-modal-id',id)
		$('#article_delete_modal').modal('show');

	});

	$('#yes_delete').click(function(e){
		e.preventDefault();

		var id = $(this).attr('data-article-modal-id');		
			$.ajax({
				url: window.App.baseUrl + 'article/delete_article',
				type:'post',
				dataType:'json',
				cache:!1,
				data:{id:id},
				success:function(e){
					console.log(e)
					if (e.success) {
							new PNotify({
					delay: 4000,
					hide: true,
					title: 'article Deleted',
					text: e.msg,
					type: 'success'
				});
					}
					$('#article_delete_modal').modal('hide');
					 setTimeout(function(){ 
               location.reload();
             }, 3000); 

				},
				error:function(e){					
				}

			});

	})

	$('.view_details_article').click(function(e){
		var id = $(this).attr('data-article-id');		
		var status_val = "";
					$.ajax({
				url: window.App.baseUrl + 'article/edit_article',
				type:'post',
				dataType:'json',
				cache:!1,
				data:{id:id},
				success:function(e){
					console.log(e)
					
					$('#gen_id').val(e.blog_ids);
					$('#upload_attachment_new').attr('data-article-ids',e.blog_ids);
					$('#yes_change_thumb').attr('data-article-id',e.blog_ids);
					$('#article_id_post').val(e.blog_ids);
				

					
					if (e.status == 0 ) {
						status_val = "Draft";
					}
					if (e.status == 1 ) {
						status_val = "Publish";
					}	
					if ( e.path_file == null) {
						$('#thumb_view').hide();
						$('#thumb_image_modal').show();
						$('#thumb_check').val('1');
					}else{
						$('#thumb_check').val('0');
						$('#thumb_view').show();
						$('#thum_prev').attr('src', window.App.baseUrl +'uploads/post/'+e.path_file);
					}				
					$('#modal_status').text(status_val);
					$('#article_title').val(e.blog_title);
					$('#article_link').val(e.blog_link);
					CKEDITOR.instances['article_desc'].setData(e.blog_desc);
				

				},
				error:function(e){					
				}

			});
		$('#article_edit_modal').modal('show');
	})

	$('#chang_the_thumb').click(function(e){
		$(this).hide();
		$('#thumb_change').show();
	})

	$('#yes_change_thumb').click(function(e){
var id = $(this).attr('data-article-id');
	
				$.ajax({
				url: window.App.baseUrl + 'article/change_thumb',
				type:'post',
				dataType:'json',
				cache:!1,
				data:{id:id},
				success:function(e){
					console.log(e)
			if (e.success) {
					new PNotify({
					delay: 4000,
					hide: true,
					title: 'Thumbnails',
					text: e.msg,
					type: 'success'
				});
			}
			$('#thumb_change').hide();
$('#thumb_view').hide();
$('#thumb_image_modal').show();
				},
				error:function(e){					
				}

			});

	});

	$("#status_article").change(function(e){
		var id = $('#article_id_post').val();
		var status = $('#status_article').val()
		var thumb_check =  $('#thumb_check').val();
		if (status == "") {
			return
		}

		if(status == 1 && thumb_check == 1){
					new PNotify({
					delay: 4000,
					hide: true,
					title: 'Alert',
					text: 'Please add thumbnail.',
					type: 'Warning'
				});
					$('#article_dropzone').css('border-color','red');
			return
		}


			$.ajax({
				url: window.App.baseUrl + 'article/change_status',
				type:'post',
				dataType:'json',
				cache:!1,
				data:{id:id,status:status},
				success:function(e){
					console.log(e)
					if (e.success) {
							new PNotify({
					delay: 4000,
					hide: true,
					title: 'Status Update',
					text: e.msg,
					type: 'success'
				});
						setTimeout(function(){ 
               location.reload();
             }, 3000); 
							
					}

				},
				error:function(e){					
				}

			});

	});

	$('#update_article').click(function(e){
		e.preventDefault();
		var article_title = $('#article_title').val();
		var id = $('#article_id_post').val();
		var article_link = $('#article_link').val();
		var article_desc = CKEDITOR.instances['article_desc'].getData();	


			$.ajax({
				url: window.App.baseUrl + 'article/update_article',
				type:'post',
				dataType:'json',
				cache:!1,
				data:{article_title:article_title, article_link:article_link, article_desc:article_desc,id:id,},
				success:function(e){
					console.log(e)
					if (e.success) {
							new PNotify({
					delay: 4000,
					hide: true,
					title: 'Status Update',
					text: e.msg,
					type: 'success'
				});
						setTimeout(function(){ 
               location.reload();
             }, 3000); 
							
					}

				},
				error:function(e){					
				}

			});

	})
	$("#status_game").change(function(e){
		var id = $('#game_id_post').val();
		var status = $('#status_game').val()
		var thumb_check =  $('#thumb_check').val();

		if (status == "") {
			return
		}

		if(status == 1 && thumb_check == 1){
					new PNotify({
					delay: 4000,
					hide: true,
					title: 'Alert',
					text: 'Please add thumbnail.',
					type: 'Warning'
				});
					$('#game_dropzone').css('border-color','red');
			return
		}

	

			$.ajax({
				url: window.App.baseUrl + 'game/change_status',
				type:'post',
				dataType:'json',
				cache:!1,
				data:{id:id,status:status},
				success:function(e){
					console.log(e)
					if (e.success) {
							new PNotify({
					delay: 4000,
					hide: true,
					title: 'Status Update',
					text: e.msg,
					type: 'success'
				});
						setTimeout(function(){ 
               location.reload();
             }, 3000); 
							
					}

				},
				error:function(e){					
				}

			});

	});
});

