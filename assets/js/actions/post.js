$(function() {

	$('#submit_post').click(function(e){
		e.preventDefault();
		var t_post = $('#t_post').val();
		var status = $('#status').val();		
		// var sub_title = $('#sub_title').val();
		var sub_title = "";
		var upload_type = $('#upload_type').val();
		var post_author = $('#post_author').val();
		var post_link = $('#post_link').val();
		// var desc = $('#desc').val();

		var desc = CKEDITOR.instances['desc'].getData();
		var error = "";
		// if (upload_type == "video") {
			
		// 		if (!$("form#video").hasClass("dz-started" )){
		// 			new PNotify({
		// 		delay: 4000,
		// 		hide: true,
		// 		title: 'Required',
		// 		text: 'Please add Video file to thumbnail',
		// 		type: 'warning'
		// 	});
		// 			return
		// 		}
		// 	}

			
				
				if (!$("form#photo").hasClass("dz-started" )){
					new PNotify({
				delay: 4000,
				hide: true,
				title: 'Required',
				text: 'Please add Image file to thumbnail',
				type: 'warning'
			});
					return
				}
			

		if(t_post == "" ||  desc == "" || upload_type == "" || post_author == "" || post_link == ""){
			if(t_post == ""){
				error += "<span>Title is Required</span></br>";
			}
			// if(sub_title == ""){
			// 	error += "<span>Sub Title is Required</span></br>";
			// }
			if(desc == ""){
				error += "<span>Description is Required</span></br>";
			}

			if(upload_type == ""){
				error += "<span>File Post Thumbnail is Required</span></br>";
			}

			if(post_link == ""){
				error += "<span>Game Link is Required</span></br>";
			}

			if(post_author == ""){
				error += "<span>Author is Required</span></br>";
			}

			new PNotify({
				delay: 4000,
				hide: true,
				title: 'Required',
				text: error,
				type: 'warning'
			});
			return
			

		}else{
			$(this).attr('disabled','disabled');
			$.ajax({
				url: window.App.baseUrl + 'post/add_post',
				type:'post',
				dataType:'json',
				cache:!1,
				data:{t_post:t_post,post_author:post_author,sub_title:sub_title, desc:desc,status:status,post_link:post_link},
				success:function(e){
					
					upload_photo(e.id,"photo");
					// upload_video(e.id,"video");
					// upload_audio(e.id,"any")				
					
					if(localStorage.getItem('response') == "success"){
						localStorage.removeItem('response');

								new PNotify({
									delay: 4000,
									hide: true,
									title: 'Message',
									text: 'Post Added!',
									type: 'success'
								})

				 setTimeout(function(){ 
                 window.location = window.App.baseUrl + '/post/';
             }, 3000);  
					}else{


							new PNotify({
									delay: 4000,
									hide: true,
									title: 'Message',
									text: 'Post Added!',
									type: 'success'
								})

				setTimeout(function(){ 
                 window.location = window.App.baseUrl + '/post/';
             }, 3000);  
					}

					

				},
				error:function(e){
					console.log(e)
				}

			});
		}
	});


	// Dropzone.options.myDropzone = false;
	// var myDropzone = new Dropzone('#video', {     
	// 	url: window.App.baseUrl+'post/upload_file',
	// 	autoProcessQueue: false,
	// 	parallelUploads: 10,
	// 	maxFiles: 1,
	// 	acceptedFiles: 'video/*',
	// 	thumbnailWidth: 250,
	// 	thumbnailHeight: 250,
	// 	addRemoveLinks: true,
	// 	maxFilesize:10,		
	// 	uploadMultiple: false,
	// 	init: function() {
	// 		this.on("success", function(file, responseText) {

	// 			// new PNotify({
	// 			// 	delay: 4000,
	// 			// 	hide: true,
	// 			// 	title: 'Message',
	// 			// 	text: 'Thumbnail Uploading Success!',
	// 			// 	type: 'success'
	// 			// });

 //    //         setTimeout(function(){ 
 //    //             location.reload();
 //    //          }, 3000);  
 //        });
	// 		this.on("error", function(file, message) { 
	// 			new PNotify({
	// 				delay: 4000,
	// 				hide: true,
	// 				title: 'Message',
	// 				text: message,
	// 				type: 'error'
	// 			});
	// 			this.removeFile(file); 
	// 		});

	// 	},    
	// 	maxfilesexceeded: function(file) {
	// 		this.removeAllFiles();
	// 		this.addFile(file);
	// 	},



	// });


	// function upload_video(id,type)
	// {
	// 	if (myDropzone.files.length > 0) {

	// 		myDropzone.options.url = window.App.baseUrl+'post/upload_file/'+id+"/"+type

	// 		if (myDropzone.getQueuedFiles().length > 0)
	// 		{
	// 			myDropzone.processQueue();
	// 		}
	// 		else
	// 		{
	// 			myDropzone.uploadFiles([]);
	// 		}

	// 		myDropzone.removeAllFiles();
	// 	}
	// }


	//    Dropzone.options.myDropzone2 = false;
	// var myDropzone2 = new Dropzone('#audio', {     
	// 	url: window.App.baseUrl+'post/upload_file',
	// 	autoProcessQueue: false,
	// 	parallelUploads: 10,
	// 	maxFiles: 10,		
	// 	thumbnailWidth: 250,
	// 	thumbnailHeight: 250,
	// 	addRemoveLinks: true,		   
	// 	uploadMultiple: true,
	// 	init: function() {
	// 		this.on("success", function(file, responseText) {
				
	// 			var stat = JSON.parse(responseText);

	// 			if(stat.status == "success"){
	// 			localStorage.setItem('response',"success");
	// 			}

 //        });
	// 		this.on("error", function(file, message) { 
	// 			new PNotify({
	// 				delay: 4000,
	// 				hide: true,
	// 				title: 'Message',
	// 				text: message,
	// 				type: 'error'
	// 			});
	// 			this.removeFile(file); 
	// 		});

	// 	},    
	// 	maxfilesexceeded: function(file) {
	// 		this.removeAllFiles();
	// 		this.addFile(file);
	// 	},



	// });


	// function upload_audio(id,type)
	// {
	// 	if (myDropzone2.files.length > 0) {
	// 		myDropzone2.options.url = window.App.baseUrl+'/post/upload_file/'+id+"/"+type ;
	// 		if (myDropzone2.getQueuedFiles().length > 0)
	// 		{
	// 			myDropzone2.processQueue();
	// 		}
	// 		else
	// 		{
	// 			myDropzone2.uploadFiles([]);

					
	// 		}

	// 		myDropzone2.removeAllFiles();
	// 	}


	// }

	 Dropzone.options.myDropzone3 = false;
	var myDropzone3 = new Dropzone('#photo', {     
		url:window.App.baseUrl+'post/upload_file',
		autoProcessQueue: false,
		parallelUploads: 10,
		maxFiles: 1,
		thumbnailWidth: 250,
		thumbnailHeight: 250,
		addRemoveLinks: true,
		uploadMultiple: false,
		maxFilesize: 0.200,		
		acceptedFiles: 'image/*',
		init: function() {
			this.on("success", function(file, responseText) {

				// new PNotify({
				// 	delay: 4000,
				// 	hide: true,
				// 	title: 'Message',
				// 	text: 'Thumbnail Uploading Success!',
				// 	type: 'success'
				// });

    //         setTimeout(function(){ 
    //             location.reload();
    //          }, 3000);  
        });
			this.on("error", function(file, message) { 
				new PNotify({
					delay: 4000,
					hide: true,
					title: 'Message',
					text: message,
					type: 'error'
				});
				this.removeFile(file); 
			});

		},    
		maxfilesexceeded: function(file) {
			this.removeAllFiles();
			this.addFile(file);
		},



	});



	function upload_photo(id,type)
	{
		if (myDropzone3.files.length > 0) {

			myDropzone3.options.url = window.App.baseUrl+'post/upload_file/'+id+"/"+type

			if (myDropzone3.getQueuedFiles().length > 0)
			{
				myDropzone3.processQueue();
			}
			else
			{
				myDropzone3.uploadFiles([]);
			}

			myDropzone3.removeAllFiles();
		}
	}  

	$('#upload_type').change(function(e){
		var type = $(this).val();
		if(type == "video"){
			$('#video').show();
			$('#photo').hide();
			$('#upload_type_title').html('Video');
		}else{
			$('#video').hide();
			$('#photo').show();
			$('#upload_type_title').html('Image');
		}

	})


$(document).ready(function(){
	// $('#video').hide();
	// $('#photo').hide();
			
})




});	