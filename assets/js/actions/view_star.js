$(function() {
	$('.show_star').click(function(e){
		e.preventDefault();
		var id = $(this).attr('star-id');

		$.ajax({
			url: window.App.baseUrl + 'star/show_details',
			type:'post',
			dataType:'json',
			cache:!1,
			data:{id:id},
			success:function(e){
				console.log(e)
				if (e.success) {
					CKEDITOR.instances['star_desc'].setData(e.star_desc);

					$('#show_star').modal('show');
					$('#update_star_id').val(id);
					$('#status_modal').attr('publish-status',id);
					$('#star_title').text(e.star_type);
					$('#vid_link').val(e.star_link);

					$('#st_title').val(e.star_title);
					$('#st_author').val(e.star_author);

					if(e.filepath == null){
						$('#video_show').hide();
						$('#upload_new_video').show();
						$('#add_video_star').show();
					}else{
						$('#change_video_modal').attr('data-id',e.id);
						$('#change_video_modal').attr('data-path',e.filepath);
						$('#add_video_star').hide();
						$('#video_show').show();
						$('#upload_new_video').hide();
						$('#path').attr('src',window.App.baseUrl+'uploads/star/'+e.filepath);	
					}

					$('#star_type').val(e.star_type);						
					$('#star_type').text('Selected: '+e.star_type);			

				}


			},
			error:function(e){
				console.log(e)
			}

		});

	});

	$('#status_modal').change(function(e){

		var id = $(this).attr('publish-status');
		var status = $(this).val();
// 					<div class="input-group">
// 					<input type="text" class="datepicker form-control" value="'.$string.'">
// 					<span class="input-group-btn"><button class="btn btn-success" type="button"><i class="fa fa-calendar"></i> Save date</button>
// 					</span></div>
if($(this).val()==2){
	$('#schedule_post').modal('show');
	$('#publish_date_modal').attr('data-id-modal',id)
	return

}
$.ajax({
	url: window.App.baseUrl + 'star/update_status',
	type:'post',
	dataType:'json',
	cache:!1,
	data:{id:id,status:status},
	success:function(e){
		console.log(e)

		if (e.success) {

			new PNotify({
				delay: 4000,
				hide: true,
				title: 'Message',
				text: e.msg,
				type: 'success'
			});

		}
		setTimeout(function(){ 
			location.reload();
		}, 3000); 

	},
	error:function(e){
		console.log(e)
	}

});


});

	$('#update_post').click(function(e){
		var star_desc =	$('#star_desc').val();
		var star_link =	$('#vid_link').val();
		var id = $('#update_star_id').val();
		var type = $('#view_star').val();
		var st_title = $('#st_title').val();
		var st_author = $('#st_author').val();
		var star_desc = CKEDITOR.instances['star_desc'].getData();	
			var error = "";
		if (star_link == "" || star_desc == "" || st_title == "" || st_author == "") 
		{

			if(star_link == ""){
				error += "Link is missing</br>";
			}
			if(st_title == "" ){
				error += "Title is missing</br>";
			}
			if(st_author == ""){
				error += "Author is missing</br>";
			}			

			if(star_desc == ""){
				error += "Star Description is missing</br>";

			}

			
			new PNotify({
				delay: 4000,
				hide: true,
				title: 'Message',
				text: error,
				type: 'warning'
			});
			return
		}

		$.ajax({
			url: window.App.baseUrl + 'star/update_star',
			type:'post',	
			dataType:'json',
			cache:!1,
			data:{ id:id, type:type,star_link:star_link,st_title:st_title,st_author:st_author,star_desc:star_desc},
			success:function(e){
				console.log(e)
				if (e.success) {
					new PNotify({
						delay: 4000,
						hide: true,
						title: 'Message',
						text: e.msg,
						type: 'success'
					});
					setTimeout(function(){ 
						location.reload();
					}, 3000);  
				}

			},
			error:function(e){
				console.log(e)
			}

		});
		
	});
	$( ".datepicker" ).datepicker();
});

$(document).ready(function() {
	$('.status_schedule').hide();
	$( ".date_publish" ).datepicker();
	$('#schedule_post_input').datepicker({
		format: "yyyy-mm-dd"      

	});
});

$('#publish_date_modal').click(function(e){
	var id = $(this).attr('data-id-modal');
	var date_post = $('#schedule_post_input').val();

	$.ajax({
		url: window.App.baseUrl + 'star/schedul_post',
		type:'post',	
		dataType:'json',
		cache:!1,
		data:{date_post:date_post, id:id},
		success:function(e){

			if (e.success) {
				new PNotify({
					delay: 4000,
					hide: true,
					title: 'Message',
					text: e.msg,
					type: 'success'
				});
				$('#schedule_post').modal('hide');
						// location.reload();
					}

				},
				error:function(e){
					console.log(e)
				}

			});
});

$('.order_by').change(function(e){
	e.preventDefault()
	var order_by = $(this).val();
	var id = $(this).attr('data-id-order-by');
	$.ajax({
		url: window.App.baseUrl + 'star/update_order',
		type:'post',
		dataType:'json',
		cache:!1,
		data:{order_by:order_by,id:id},
		success:function(e){
			console.log(e)
			if (e.success) {
				new PNotify({
					delay: 4000,
					hide: true,
					title: 'Message',
					text: e.msg,
					type: 'success'
				});

			}

		},
		error:function(e){
			console.log(e)
		}

	});
})

function GetFormattedDate() {
	var todayTime = new Date();
	var month = format(todayTime .getMonth() + 1);
	var day = format(todayTime .getDate());
	var year = format(todayTime .getFullYear());
	return month + "/" + day + "/" + year;
}

$('#publish_date_modal').click(function(e){
	var id = $(this).attr('data-id-modal');
	var schedule_post_input = $('#schedule_post_input').val();

	$.ajax({
		url: window.App.baseUrl + 'star/update_publish_date',
		type:'post',
		dataType:'json',
		cache:!1,
		data:{schedule_post_input:schedule_post_input,id:id},
		success:function(e){

			if (e.success) {
				new PNotify({
					delay: 4000,
					hide: true,
					title: 'Message',
					text: e.msg,
					type: 'success'
				});
				location.reload();

			}else{
				new PNotify({
					delay: 4000,
					hide: true,
					title: 'Message',
					text: e.msg,
					type: 'warning'
				});
			}

		},
		error:function(e){
			console.log(e)
		}

	});
});


$('#change_video_modal').click(function(e){
	e.preventDefault();
	$('#delete_video_modal').modal('show');
	var id =$('#update_star_id').val();
	var pathfile = $(this).attr('data-path');

	$('#id_star_modal').val(id);
	$('#path_modal').val(pathfile);
})

$('#yes_video_change').click(function(e){

	e.preventDefault()
	var id  = $('#id_star_modal').val();
	var pathfile  = $('#path_modal').val();
	$.ajax({
		url: window.App.baseUrl + 'star/delete_video',
		type:'post',
		dataType:'json',
		cache:!1,
		data:{pathfile:pathfile,id:id},
		success:function(e){
			console.log(e)
			if (e.success) {
				$('#delete_video_modal').modal('hide');
				new PNotify({
					delay: 4000,
					hide: true,
					title: 'Message',
					text: e.msg,
					type: 'success'
				});



			}

		},
		error:function(e){
			console.log(e)
		}

	});

	$('#video_show').hide();
	$('#add_video_star').show();
	$('#upload_new_video').show();


})

$('#upload_new_video').click(function(e){
	var id = $('#update_star_id').val();
	star_upload(id)

})


Dropzone.autoDiscover = false;
var myDropzone = new Dropzone('form#add_video_star',  { 
	url: window.App.baseUrl+'star/upload_file',
	autoProcessQueue: false,
	parallelUploads: 10,		
	thumbnailWidth: 250,
	thumbnailHeight: 250,
	addRemoveLinks: true,
	maxFiles: 1,   
	acceptedFiles: 'image/*',
	maxFilesize:0.0100,
	uploadMultiple: false,
	init: function() {
		this.on("success", function(file, responseText) {

			new PNotify({
				delay: 4000,
				hide: true,
				title: 'Message',
				text: 'Success',
				type: 'success'
			});
			console.log(responseText);


			setTimeout(function(){ 
				location.reload();
			}, 3000);  
		});
		this.on("error", function(file, message) { 
			new PNotify({
				delay: 4000,
				hide: true,
				title: 'Message',
				text: message,
				type: 'error'
			});
			this.removeFile(file); 
		});

	},    
	maxfilesexceeded: function(file) {
		this.removeAllFiles();
		this.addFile(file);
	},
});	

function star_upload(id)
{
	if (myDropzone.files.length > 0) {

		myDropzone.options.url = window.App.baseUrl+'star/upload_file/'+id
			//myDropzone.options.url = 'upload_file/'+id

			if (myDropzone.getQueuedFiles().length > 0)
			{
				myDropzone.processQueue();
			}
			else
			{
				myDropzone.uploadFiles([]);
			}

			myDropzone.removeAllFiles();
		}


	} 

	$('.btn_remove').click(function(e){
		var id = $(this).attr('data-delete-id');

		var file_path_delete_post = $(this).attr('data-path-delete');
		$('#delete_star_modal_post').modal('show');
		$('#id_delete_modal').val(id);
		$('#file_path_delete_post').val(file_path_delete_post);

	})

	$('#yes_delete_star_modal').click(function(e){
		e.preventDefault()
		var id = $('#id_delete_modal').val();
		var filepath = $('#file_path_delete_post').val();

		$.ajax({
			url: window.App.baseUrl + 'star/delete_post',
			type:'post',
			dataType:'json',
			cache:!1,
			data:{id:id,filepath:filepath},
			success:function(e){
				console.log(e)
				if (e.success) {
					$('#delete_star_modal_post').modal('hide');
					new PNotify({
						delay: 4000,
						hide: true,
						title: 'Message',
						text: e.msg,
						type: 'success'
					})

					setTimeout(function(){ 
						location.reload();
					}, 3000); 

				}

			},
			error:function(e){
				console.log(e)
			}

		});

	})