$(function(){

  Dropzone.autoDiscover = false;
	var myDropzone = new Dropzone('#banner',  { 
		url: window.App.baseUrl+'banner/upload_image',	
		uploadMultiple: true,
		parallelUploads: 10,
		maxFiles: 10,
		 maxFilesize:1,
		acceptedFiles: '.jpg, .jpeg, .png',
		init: function() {
			this.on("success", function(file, responseText) {
				new PNotify({
					delay: 4000,
					hide: true,
					title: 'Message',
					text: 'Banner Uploaded!',
					type: 'success'
				});
            
              $('#banner_body').empty();
              $('#banner_body').load(window.App.baseUrl+'admin/banner');
              
        });
			this.on("error", function(file, message) { 
				new PNotify({
					delay: 4000,
					hide: true,
					title: 'Message',
					text: message,
					type: 'error'
				});
				this.removeFile(file); 
			});

		},    
		maxfilesexceeded: function(file) {
			this.removeAllFiles();
			this.addFile(file);
		},
	});	


	$('body').on('click','.btn_remove',function(){
    var id = $(this).siblings('span').html();
    $("#get_did").val(id);
    $("#modal_warning").modal("show");

});

$(document).on("click", "#btn_remove_confirm", function(e){
 
    var id = $("#get_did").val();
    $.ajax({
        async: false,
        url: window.App.baseUrl+'/Banner/delete_banner/'+id,
        cache: false,
        dataType: 'json',
        success: function(data) {
            if(data['status']=="success"){
                new PNotify({
                    delay: 100,
                    hide: true,
                    title: 'Success!',
                    text: 'You have successfully deleted this banner',
                    type: 'success',
                    success:function(){
                        $('#modal_warning').modal('hide');
                    },
                    after_close: function() {
                        // window.location.reload();

                $('#banner_body').empty();
              $('#banner_body').load(window.App.baseUrl+'admin/banner');
                    }
                });
            } else{
                new PNotify({
                    hide: true,
                    title: 'Error!',
                    text: 'Error deleting this banner!',
                    type: 'error'
                });
            }
        }
    })
});

$(".display").change(function() {
    var id = $(this).siblings('span').html();
    var check ;
    if(this.checked) {
        check = 1;
    } else if($(this).not(':checked')){
        check = 0;
    }


    var max = 3;
    if( $(".display:checked").length == max ){
        $(".display").attr('disabled', 'disabled');
        $(".display:checked").removeAttr('disabled');
    }else{
         $(".display").removeAttr('disabled');
         return
    }

    $.ajax({
        async: false,
        type: 'POST',
        url:window.App.baseUrl+'/Banner/display_banner/'+id,
        data: {'check':check},
        cache: false,
        dataType: 'json',
        success: function(data){



            // if (data.status =='not') {
            //      new PNotify({
            //         hide: true,
            //         title: 'Error!',
            //         text: data.msg,
            //         type: 'error'
            //     });


            // }
                

        }
    })
});


var sortable = $('.sortable');
var text = "";

$('body').on('click','.order',function(){

    $("#order_modal").modal("show");
    
    sortable.sortable({
        revert: 100,
        placeholder: 'placeholder'
    });
    sortable.disableSelection();

    $.ajax({
        url:window.App.baseUrl+'/Banner/display_order',
        dataType:'json',
        success: function(data){
            console.log(data)
            for (i = 0; i < data.length; i++) { 
                sortable.append("<tr id='tr_c"+ data[i]['id'] +"'>"+
                                   "<td><img src="+window.App.baseUrl+"/uploads/banners/"+data[i]['file_name']+" height='150'></td>"+
                                " </tr>" );
            }
        }
    })

    $('#order_modal').on('hidden.bs.modal', function () {
        sortable.html("");
    });

});

$('#save_order').on('click', function(e){
    e.preventDefault(); 
    var data = sortable.sortable('serialize'); 
console.log(data)
    $.ajax({
        async: false,
        type: 'POST',
        url:window.App.baseUrl+'/Banner/update_order/',
        data: {'data':data},
        cache: false,
        dataType: 'json',
        success: function(data){
            
            if(data['status']=="success"){
                new PNotify({
                    delay: 100,
                    hide: true,
                    title: 'Success!',
                    text: 'You have successfully updated the banner display order',
                    type: 'success',
                    after_close: function() {
                        $('#order_modal').modal('hide');
                    }
                });
            } else{
                new PNotify({
                    hide: true,
                    title: 'Error!',
                    text: 'Error updating the banner display order!',
                    type: 'error'
                });
            }
        }
    })   
});

});