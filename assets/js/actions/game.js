$(function() {

	$('#submit_game').click(function(e){
		e.preventDefault()
		
		var status = $('#status').val();
		var game_title = $('#game_title').val();
		var game_sub = $('#game_sub').val();
		var post_link = $('#post_link').val();

		// var game_desc  = CKEDITOR.instances['game_desc'].getData();
		var file_video = $('#file_video').val();
		var error = "";

		if ( post_link == "" || $(".dz-preview" ).length == 0 || game_title == "" ) 
		{
			

			if($(".dz-preview" ).length == 0){
				error += "File is required</br>";
			}

			if(post_link == ""){
			error += "Link is required</br>";

			}

			if(game_title == ""){
			error += "Game Title is required</br>";

			}
				new PNotify({
					delay: 4000,
					hide: true,
					title: 'Message',
					text: error,
					type: 'warning'
				});
				return
		}else{
			$(this).attr('disabled',true);
			var type = 'game';
			$.ajax({
				url: window.App.baseUrl + 'game/add_game_func',
				type:'post',
				dataType:'json',
				cache:!1,
				data:{type:type,status:status,game_title:game_title,game_sub:game_sub,post_link:post_link},
				success:function(e){										
					game_upload(e.id);
					console.log(e)
				},
				error:function(e){
					
				}

			});
		}
	});

$('#upload_attachment_new').click(function(e){
	var id = $(this).attr('data-game-ids');
	game_upload(id);
});

if(!$('form#game_dropzone').length == 0) {
  Dropzone.autoDiscover = false;
	var myDropzone = new Dropzone('form#game_dropzone',  { 
		url: window.App.baseUrl+'star/upload_file',
		autoProcessQueue: false,
		parallelUploads: 10,		
		thumbnailWidth: 250,
		thumbnailHeight: 250,
		addRemoveLinks: true,
		maxFilesize: 0.200,
		maxFiles: 1,   
		acceptedFiles: 'image/*',
		uploadMultiple: false,
		init: function() {
			this.on("success", function(file, responseText) {

				new PNotify({
					delay: 4000,
					hide: true,
					title: 'Message',
					text: 'Success!',
					type: 'success'
				});

            setTimeout(function(){ 
                window.location = window.App.baseUrl+"game/";
             }, 3000);  
        });
			this.on("error", function(file, message) { 
				new PNotify({
					delay: 4000,
					hide: true,
					title: 'Message',
					text: message,
					type: 'error'
				});
				this.removeFile(file); 
			});

		},    
		maxfilesexceeded: function(file) {
			this.removeAllFiles();
			this.addFile(file);
		},
	});	
}




	function game_upload(id)
	{
		if (myDropzone.files.length > 0) {

			myDropzone.options.url = window.App.baseUrl+'game/upload_file/'+id+'/game'
			//myDropzone.options.url = 'upload_file/'+id

			if (myDropzone.getQueuedFiles().length > 0)
			{
				myDropzone.processQueue();
			}
			else
			{
				myDropzone.uploadFiles([]);
			}

			myDropzone.removeAllFiles();
		}


	} 
	$('.game_delete').click(function(e){
		var id = $(this).attr('data-game-id');
		$('#yes_delete').attr('data-game-modal-id',id)
		$('#game_delete_modal').modal('show');

	});

	$('#yes_delete').click(function(e){
		e.preventDefault();

		var id = $(this).attr('data-game-modal-id');		
			$.ajax({
				url: window.App.baseUrl + 'game/delete_game',
				type:'post',
				dataType:'json',
				cache:!1,
				data:{id:id},
				success:function(e){
					console.log(e)
					if (e.success) {
							new PNotify({
					delay: 4000,
					hide: true,
					title: 'Game Deleted',
					text: e.msg,
					type: 'success'
				});
					}
					$('#game_delete_modal').modal('hide');
					 setTimeout(function(){ 
               location.reload();
             }, 3000); 

				},
				error:function(e){					
				}

			});

	})

	$('.view_details_game').click(function(e){
		var id = $(this).attr('data-game-id');
		var status_val = "";
					$.ajax({
				url: window.App.baseUrl + 'game/edit_game',
				type:'post',
				dataType:'json',
				cache:!1,
				data:{id:id},
				success:function(e){
					console.log(e)

					$('#gen_id').val(e.blog_id);
					$('#upload_attachment_new').attr('data-game-ids',id);
					$('#yes_change_thumb').attr('data-game-id',e.blog_id);
					$('#game_id_post').val(id);
					
					if (e.status == 0 ) {
						status_val = "Draft";
					}
					if (e.status == 1 ) {
						status_val = "Publish";
					}	
					if ( e.path_file == null) {
						$('#thumb_view').hide();
						$('#thumb_image_modal').show();
						$('#thumb_check').val('1');
					}else{
						$('#thumb_image_modal').hide();
						$('#thumb_check').val('0');
						$('#thumb_view').show();
						$('#thum_prev').attr('src', window.App.baseUrl +'uploads/game/'+e.path_file);
					}				
					$('#modal_status').text(status_val);
					$('#game_title').val(e.blog_title);
					$('#game_link').val(e.blog_url);
					$('#view_game_article').attr('href',window.App.baseUrl +'blog/game_post/'+e.blog_id);
					// CKEDITOR.instances['game_desc'].setData(e.blog_desc);
				

				},
				error:function(e){					
				}

			});
		$('#game_edit_modal').modal('show');
	})

	$('#chang_the_thumb').click(function(e){
		$(this).hide();
		$('#thumb_change').show();
	})
$('#no_change_thumb').click(function(e){
	e.preventDefault();
	$('#thumb_change').hide();
	$('#chang_the_thumb').show();
})
	$('#yes_change_thumb').click(function(e){
				var id = $(this).attr('data-game-id');	
				
				$.ajax({
				url: window.App.baseUrl + 'game/change_thumb',
				type:'post',
				dataType:'json',
				cache:!1,
				data:{id:id},
				success:function(e){
					console.log(e)
			if (e.success) {
					new PNotify({
					delay: 4000,
					hide: true,
					title: 'Thumbnails',
					text: e.msg,
					type: 'success'
				});
			}
			$('#thumb_change').hide();
$('#thumb_view').hide();
$('#thumb_image_modal').show();
				},
				error:function(e){					
				}

			});

	});

	$('.order_by').change(function(e){
  var id = $(this).attr('data-id');
  var order_by = $(this).val();

  $.ajax({
        url:window.App.baseUrl+'post/update_order',
        type:'post',
        dataType:'json',
        data:{id:id,order_by:order_by},
        cache:!1,        
        success: function(e){          
           console.log(e);
           if (e.success) {
            new PNotify({
              delay: 4000,
              hide: true,
              title: 'Message',
              text: e.msg,
              type: 'success'
            });
           }
        },
        error:function(e){

        }
    });
});

	$("#status_game").change(function(e){
		var id = $('#game_id_post').val();
		var status = $('#status_game').val()
		var thumb_check =  $('#thumb_check').val();

		if (status == "") {
			return
		}

		if(status == 1 && thumb_check == 1){
					new PNotify({
					delay: 4000,
					hide: true,
					title: 'Alert',
					text: 'Please add thumbnail.',
					type: 'Warning'
				});
					$('#game_dropzone').css('border-color','red');
			return
		}

	

			$.ajax({
				url: window.App.baseUrl + 'game/change_status',
				type:'post',
				dataType:'json',
				cache:!1,
				data:{id:id,status:status},
				success:function(e){
					console.log(e)
					if (e.success) {
							new PNotify({
					delay: 4000,
					hide: true,
					title: 'Status Update',
					text: e.msg,
					type: 'success'
				});
						setTimeout(function(){ 
               location.reload();
             }, 3000); 
							
					}

				},
				error:function(e){					
				}

			});

	});

	$('#update_game').click(function(e){
		e.preventDefault();
		var game_title = $('#game_title').val();
		var id = $('#game_id_post').val();
		var blog_url = $('#game_link').val();
		var error = "";
		// var game_desc = CKEDITOR.instances['game_desc'].getData();
	
			$.ajax({
				url: window.App.baseUrl + 'game/update_game',
				type:'post',
				dataType:'json',
				cache:!1,
				data:{game_title:game_title, blog_url:blog_url,id:id,},
				success:function(e){
					console.log(e)
					if (e.success) {
							new PNotify({
					delay: 4000,
					hide: true,
					title: 'Status Update',
					text: e.msg,
					type: 'success'
				});
				setTimeout(function(){ 
               location.reload();
             }, 3000); 
							
					}

				},
				error:function(e){					
				}

			});

	})
});

