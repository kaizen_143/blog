$(function() {

	$('#submit_study').click(function(e){
		e.preventDefault()
		
		var status = $('#status').val();
		var study_title = $('#t_post').val();
		var study_link = $('#link_post').val();
		var study_desc  = CKEDITOR.instances['study_desc'].getData();		
		var error = "";
		if ( study_desc == "" || $(".dz-preview" ).length == 0 ) 
		{			
			if($(".dz-preview" ).length == 0){
				error += "File is missing</br>";
			}

			if(study_desc == ""){
			error += "Star Description is missing</br>";

			}
				new PNotify({
					delay: 4000,
					hide: true,
					title: 'Message',
					text: error,
					type: 'warning'
				});
				return
		}else{
			var type = 'study';
			$.ajax({
				url: window.App.baseUrl + 'study/add_new_study',
				type:'post',
				dataType:'json',
				cache:!1,
				data:{type:type,study_desc:study_desc,status:status,study_title:study_title,study_link:study_link},
				success:function(e){
					console.log(e)
					study_upload(e.id);					
				
				},
				error:function(e){
					
				}

			});
		}
	});

$('#upload_attachment_new').click(function(e){
	var id = $('#study_id_post').val();
	study_upload(id);
});

if(!$('#study_dropzone').length == 0) {
  Dropzone.autoDiscover = false;
	var myDropzone = new Dropzone('#study_dropzone',  { 
		url: window.App.baseUrl+'study/upload_file',
		autoProcessQueue: false,
		parallelUploads: 10,		
		thumbnailWidth: 250,
		thumbnailHeight: 250,
		addRemoveLinks: true,
		maxFiles: 1,   
		acceptedFiles: 'image/*',
		uploadMultiple: false,
		init: function() {
			this.on("success", function(file, responseText) {
				new PNotify({
					delay: 4000,
					hide: true,
					title: 'Message',
					text: 'Success!',
					type: 'success'
				});

            // setTimeout(function(){ 
            //     window.location = window.App.baseUrl+"study/";
            //  }, 3000);  
        });
			this.on("error", function(file, message) { 
				new PNotify({
					delay: 4000,
					hide: true,
					title: 'Message',
					text: message,
					type: 'error'
				});
				this.removeFile(file); 
			});

		},    
		maxfilesexceeded: function(file) {
			this.removeAllFiles();
			this.addFile(file);
		},
	});	
}




	function study_upload(id)
	{
		if (myDropzone.files.length > 0) {
		
			myDropzone.options.url = window.App.baseUrl+'study/upload_file/'+id+'/study'
			//myDropzone.options.url = 'upload_file/'+id

			if (myDropzone.getQueuedFiles().length > 0)
			{
				myDropzone.processQueue();
			}
			else
			{
				myDropzone.uploadFiles([]);
			}

			myDropzone.removeAllFiles();
		}


	} 
	$('.study_delete').click(function(e){
		var id = $(this).attr('data-study-id');
		$('#yes_delete').attr('data-study-modal-id',id)
		$('#study_delete_modal').modal('show');

	});

	$('#yes_delete').click(function(e){
		e.preventDefault();

		var id = $(this).attr('data-study-modal-id');		
			$.ajax({
				url: window.App.baseUrl + 'study/delete_study',
				type:'post',
				dataType:'json',
				cache:!1,
				data:{id:id},
				success:function(e){
					console.log(e)
					if (e.success) {
							new PNotify({
					delay: 4000,
					hide: true,
					title: 'study Deleted',
					text: e.msg,
					type: 'success'
				});
					}
					$('#study_delete_modal').modal('hide');
					 setTimeout(function(){ 
               location.reload();
             }, 3000); 

				},
				error:function(e){					
				}

			});

	})

	$('.view_details_study').click(function(e){
		var id = $(this).attr('data-study-id');		
		var status_val = "";
					$.ajax({
				url: window.App.baseUrl + 'study/edit_study',
				type:'post',
				dataType:'json',
				cache:!1,
				data:{id:id},
				success:function(e){
					console.log(e)
					
					$('#gen_id').val(e.blog_ids);
					$('#upload_attachment_new').attr('data-study-ids',e.blog_ids);
					$('#yes_change_thumb').attr('data-study-id',e.blog_ids);
					$('#study_id_post').val(e.blog_ids);
				

					
					if (e.status == 0 ) {
						status_val = "Draft";
					}
					if (e.status == 1 ) {
						status_val = "Publish";
					}	
					if ( e.path_file == null) {
						$('#thumb_view').hide();
						$('#thumb_image_modal').show();
						$('#thumb_check').val('1');
					}else{
						$('#thumb_check').val('0');
						$('#thumb_view').show();
						$('#thum_prev').attr('src', window.App.baseUrl +'uploads/study/'+e.path_file);
					}				
					$('#modal_status').text(status_val);
					$('#study_title').val(e.blog_title);
					$('#study_link').val(e.blog_link);
					CKEDITOR.instances['study_desc'].setData(e.blog_desc);
				

				},
				error:function(e){					
				}

			});
		$('#study_edit_modal').modal('show');
	})

	$('#chang_the_thumb').click(function(e){
		$(this).hide();
		$('#thumb_change').show();
	})

	$('#yes_change_thumb').click(function(e){
var id = $(this).attr('data-study-id');
	
				$.ajax({
				url: window.App.baseUrl + 'study/change_thumb',
				type:'post',
				dataType:'json',
				cache:!1,
				data:{id:id},
				success:function(e){
					console.log(e)
			if (e.success) {
					new PNotify({
					delay: 4000,
					hide: true,
					title: 'Thumbnails',
					text: e.msg,
					type: 'success'
				});
			}
			$('#thumb_change').hide();
$('#thumb_view').hide();
$('#thumb_image_modal').show();
				},
				error:function(e){					
				}

			});

	});

	$("#status_study").change(function(e){
		var id = $('#study_id_post').val();
		var status = $('#status_study').val()
		var thumb_check =  $('#thumb_check').val();
		if (status == "") {
			return
		}

		if(status == 1 && thumb_check == 1){
					new PNotify({
					delay: 4000,
					hide: true,
					title: 'Alert',
					text: 'Please add thumbnail.',
					type: 'Warning'
				});
					$('#study_dropzone').css('border-color','red');
			return
		}


			$.ajax({
				url: window.App.baseUrl + 'study/change_status',
				type:'post',
				dataType:'json',
				cache:!1,
				data:{id:id,status:status},
				success:function(e){
					console.log(e)
					if (e.success) {
							new PNotify({
					delay: 4000,
					hide: true,
					title: 'Status Update',
					text: e.msg,
					type: 'success'
				});
						setTimeout(function(){ 
               location.reload();
             }, 3000); 
							
					}

				},
				error:function(e){					
				}

			});

	});

	$('#update_study').click(function(e){
		e.preventDefault();
		var study_title = $('#study_title').val();
		var id = $('#study_id_post').val();
		var study_link = $('#study_link').val();
		var study_desc = CKEDITOR.instances['study_desc'].getData();	


			$.ajax({
				url: window.App.baseUrl + 'study/update_study',
				type:'post',
				dataType:'json',
				cache:!1,
				data:{study_title:study_title, study_link:study_link, study_desc:study_desc,id:id,},
				success:function(e){
					console.log(e)
					if (e.success) {
							new PNotify({
					delay: 4000,
					hide: true,
					title: 'Status Update',
					text: e.msg,
					type: 'success'
				});
						setTimeout(function(){ 
               location.reload();
             }, 3000); 
							
					}

				},
				error:function(e){					
				}

			});

	})
	$("#status_game").change(function(e){
		var id = $('#game_id_post').val();
		var status = $('#status_game').val()
		var thumb_check =  $('#thumb_check').val();

		if (status == "") {
			return
		}

		if(status == 1 && thumb_check == 1){
					new PNotify({
					delay: 4000,
					hide: true,
					title: 'Alert',
					text: 'Please add thumbnail.',
					type: 'Warning'
				});
					$('#game_dropzone').css('border-color','red');
			return
		}

	

			$.ajax({
				url: window.App.baseUrl + 'game/change_status',
				type:'post',
				dataType:'json',
				cache:!1,
				data:{id:id,status:status},
				success:function(e){
					console.log(e)
					if (e.success) {
							new PNotify({
					delay: 4000,
					hide: true,
					title: 'Status Update',
					text: e.msg,
					type: 'success'
				});
						setTimeout(function(){ 
               location.reload();
             }, 3000); 
							
					}

				},
				error:function(e){					
				}

			});

	});
});

