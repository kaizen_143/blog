
$(function() {
	$('#login').click(function(e){
		e.preventDefault();
		
		var username = $("#username").val().trim();
		var password = $("#password").val().trim();

		
		$.ajax({
			type:'POST',
			url: window.App.baseUrl+'auth/login',
			dataType:'json',
			data:{username:username,  password:password},
			cache:!1,
			success:function(e){
				
				if (e.success) {
					 new PNotify({
					        delay: 3000,
					        hide: true,
					        title: 'login',
					        text: e.msg,
					        type: 'success'
					    });	

					    setTimeout(function() {
							window.location = window.App.baseUrl+"admin/post";
						}, 3000);	
				}else{
					 new PNotify({
					        delay: 4000,
					        hide: true,
					        title: 'login',
					        text: e.msg,
					        type: 'warning'
					    });	
				}
					
			},
			error:function(e){
				
				 new PNotify({
					        delay: 4000,
					        hide: true,
					        title: 'wrong username or password',
					        text: '',
					        type: 'warning'
					    });	
				 
			}
		});		

	})
});