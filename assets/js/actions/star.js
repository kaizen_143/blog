$(function() {

	$('#submit_star').click(function(e){
		e.preventDefault()
		var star_type = $('#type_star').val();
		var status = $('#status').val();
		var star_link = $('#star_link').val();
		var star_desc  = CKEDITOR.instances['star_desc'].getData();
		var file_video = $('#file_video').val();
		var st_title = $('#st_title').val();
		var st_author = $('#st_author').val();
		var error = "";


		if ($(".dz-preview" ).length == 0 || star_link == "" || star_desc == "" || st_title == "" || st_author == "") 
		{

			if($(".dz-preview" ).length == 0){
				error += "Thumbnail is missing</br>";
			}

			if(star_link == ""){
				error += "Link is missing</br>";
			}
			if(st_title == "" ){
				error += "Title is missing</br>";
			}
			if(st_author == ""){
				error += "Author is missing</br>";
			}			

			if(star_desc == ""){
				error += "Star Description is missing</br>";

			}

			
			new PNotify({
				delay: 4000,
				hide: true,
				title: 'Message',
				text: error,
				type: 'warning'
			});
			return
		}else{
			$(this).attr('disabled',true);
			$.ajax({
				url: window.App.baseUrl + 'star/add_star',
				type:'post',
				dataType:'json',
				cache:!1,
				data:{star_type:star_type, star_link:star_link,status:status,star_desc:star_desc,st_title:st_title,st_author:st_author},
				success:function(e){

					star_upload(e.id);



				},
				error:function(e){
					
				}

			});
		}
	});

	if(!$('form#star_videos').length == 0) {
		Dropzone.autoDiscover = false;
		var myDropzone = new Dropzone('form#star_videos',  { 
			url: window.App.baseUrl+'star/upload_file',
			autoProcessQueue: false,
			parallelUploads: 10,		
			thumbnailWidth: 250,
			thumbnailHeight: 250,
			addRemoveLinks: true,
			maxFiles: 1,   
			acceptedFiles: 'image/*',
			uploadMultiple: false,
			init: function() {
				this.on("success", function(file, responseText) {

					new PNotify({
						delay: 4000,
						hide: true,
						title: 'Message',
						text: 'Success!',
						type: 'success'
					});

					setTimeout(function(){ 
						location.reload();
					}, 3000);  
				});
				this.on("error", function(file, message) { 
					new PNotify({
						delay: 4000,
						hide: true,
						title: 'Message',
						text: message,
						type: 'error'
					});
					this.removeFile(file); 
				});

			},    
			maxfilesexceeded: function(file) {
				this.removeAllFiles();
				this.addFile(file);
			},
		});	
	}




	function star_upload(id)
	{
		if (myDropzone.files.length > 0) {

			myDropzone.options.url = window.App.baseUrl+'star/upload_file/'+id
			//myDropzone.options.url = 'upload_file/'+id

			if (myDropzone.getQueuedFiles().length > 0)
			{
				myDropzone.processQueue();
			}
			else
			{
				myDropzone.uploadFiles([]);
			}

			myDropzone.removeAllFiles();
		}


	} 
});
$(document).ready(function(){
	$("#view_parent").click(function(){
		$("#student,#school").fadeOut(function(){
			$("#parent").fadeIn();				
		});
	});


// $( "#main_star_view" ).load( window.App.baseUrl+'star/show_star_student/0',function(){} );

// });

// $('.stack').click(function(e){
// 	 		var datas = parseInt($('#count_slide').val()); 		
//  			var my_data =  datas + 1 ; 		
//  			$('#count_slide').val(my_data);
// });

// $('#next_slide').click(function(e){
// 	e.preventDefault();
// 	$(this).attr('disabled',true);
// 	setTimeout(function(){$('#next_slide').removeAttr('disabled');}, 1000);
// 	var offset = parseInt($('#count_slide').val());
// 	var count_everything = parseInt($('#count_everything').val());
// 	offset = offset + count_everything;	
// 	$('#count_everything').val(offset);
// 	$('#main_star_view').empty();
// 	$( "#main_star_view" ).load( window.App.baseUrl+'star/show_star_student/'+offset,function(){});
// 	// 	offset = parseInt(offset) + 1;
// 	// 	$('#count_slide').val(offset);
// 	// } );
// 	// if ($('#check_value').val() >= 1) {		
// 	// 	$( "#main_star_view" ).load( window.App.baseUrl+'star/show_star_student/'+0,function(){ return} );
// 	// 	}
// });

// $('#prev_slide').click(function(e){
// 	e.preventDefault();
// 	$(this).attr('disabled',true);
// 	setTimeout(function(){$('#prev_slide').removeAttr('disabled');
// 		}, 1000);
// 	var offset = $('#count_slide').val();

// 	$('#main_star_view').empty();
// 	$( "#main_star_view" ).load( window.App.baseUrl+'star/show_star_student/'+offset,function(){
// 		if ($('#check_value').val() >= 1) {
// 			// $('#prev_slide').text('No More slide awww');
// 			// $('#count_slide').val(0);
// 			// $('#check_value').val(0);
// 			offset = 0;
// 		}
// 		offset = parseInt(offset) - 1;
// 	if (offset <=0) {
// 		offset = 1;
// 	}
// 		$('#count_slide').val(offset);
// 	} );
// });



});
