$(function() {



  $( document ).ready(function() {


});

$('.order_by').change(function(e){
  var id = $(this).parent().parent().attr('data-id');
  var order_by = $(this).val();


  $.ajax({
        url:window.App.baseUrl+'post/update_order',
        type:'post',
        dataType:'json',
        data:{id:id,order_by:order_by},
        cache:!1,        
        success: function(e){          
           console.log(e);
           if (e.success) {
            new PNotify({
              delay: 4000,
              hide: true,
              title: 'Message',
              text: e.msg,
              type: 'success'
            });
           }
        },
        error:function(e){

        }
    });
});

	$('.view_download_files').click(function(e){
		e.preventDefault();
		var id = $(this).attr('data-id');
		var item = "";
		var count = 0;
		$.ajax({
				type:'POST',
				url: window.App.baseUrl+'post/view_files',
				dataType:'json',
				data:{id:id},
				cache:!1,
				success:function(e){
				    $.each(e, function(index, data) {
					item += "<a style='margin:5px' class = 'btn btn_blue view_image download_file' href='"+window.App.baseUrl+'uploads/post/'+data.path_file+"' data-att-id = '"+data.att_id+"' download>"+data.path_file+" </a>";
	                count = count+1;});
					    $('#download_sec').html(item);
					    $('#counts').html(count);
					    $('#view_downloads').modal('show');					
				},
				error:function(e){
				}
			});		
	})

	$('#submit_download').click(function(e){
		e.preventDefault();
		var status = $('#status').val();
    var t_post = $('#t_post').val();
		var sub_title = $('#sub_title').val();
    var link_post = $('#link_post').val();
		var post_type = $('#post_type').val();
		var desc = CKEDITOR.instances['desc'].getData();
		var error = "";	
		if(t_post == "" || link_post == "" || desc == "" ||  !$("form#thumb").hasClass("dz-started" ) ){
			// if (!$("form#attach").hasClass("dz-started" )){
			// 	error += "<span>File is required.</span></br>";
			// }
			if(t_post == ""){
				error += "<span>Title is Required</span></br>";
			}
			if(link_post == ""){
				error += "<span>Author is Required</span></br>";
			}
			if(desc == ""){
				error += "<span>Description is Required</span></br>";
			}

      if(!$("form#thumb").hasClass("dz-started" ) ){
        error += "<span>Thumbnail is Required</span></br>";
      }  
       if(sub_title == ""){
        error += "<span>Sub Title is Required</span></br>";
      }  

			new PNotify({
				delay: 4000,
				hide: true,
				title: 'Required',
				text: error,
				type: 'warning'
			});
			return
		}else{

			$(this).attr('disabled','disabled');
			$.ajax({
				url: window.App.baseUrl + 'download/add_new_download',
				type:'post',
				dataType:'json',
				cache:!1,
				data:{status:status, t_post:t_post,sub_title:sub_title,post_type:post_type, link_post:link_post, desc:desc,}, 
				success:function(e){
					console.log(e)
          upload_file(e.id);                          
					upload_thumb(e.id);													
					
					// if(localStorage.getItem('response') == "success"){
					// 	localStorage.removeItem('response');
					// 			new PNotify({
					// 				delay: 4000,
					// 				hide: true,
					// 				title: 'Message',
					// 				text: 'Success!',
					// 				type: 'success'
					// 			});
				 //            setTimeout(function(){ 
				 //                location.reload();
				 //             }, 3000);  
					// }else{
					// 		new PNotify({
					// 				delay: 4000,
					// 				hide: true,
					// 				title: 'Message',
					// 				text: 'Success!',
					// 				type: 'success'
					// 			});

				 //            setTimeout(function(){ 
				 //                location.reload();
				 //             }, 3000); 
					// }

				},
				error:function(e){
					console.log(e)
				}

			});
		}

	});

if(!$('#attach').length == 0) {
	   Dropzone.options.myDropzone2 = false;
	var myDropzone2 = new Dropzone('#attach', {     
		url: window.App.baseUrl+'download/upload_file',
		autoProcessQueue: false,
		parallelUploads: 10,
		maxFiles: 1,	  
		thumbnailWidth: 250,
		thumbnailHeight: 250,
		addRemoveLinks: true,		   
    uploadMultiple: true,
    
		maxFilesize: 1,
		init: function() {
			this.on("success", function(file, responseText) {
				
				// var stat = JSON.parse(responseText);

				// if(stat.status == "success"){
				// localStorage.setItem('response',"success");
				// }
        new PNotify({
                 delay: 4000,
                 hide: true,
                 title: 'Message',
                 text: 'Success!',
                 type: 'success'
               });
         setTimeout(function(){ 
                        location.reload();
                     }, 3000);  

        });
			this.on("error", function(file, message) { 
				new PNotify({
					delay: 4000,
					hide: true,
					title: 'Message',
					text: message,
					type: 'error'
				});
				this.removeFile(file); 
			});

		},    
		maxfilesexceeded: function(file) {
			this.removeAllFiles();
			this.addFile(file);
		},

	});

	}


	function upload_file(id)
	{
		if (myDropzone2.files.length > 0) {
			myDropzone2.options.url = window.App.baseUrl+'/download/upload_file/'+id+"/any" ;
			if (myDropzone2.getQueuedFiles().length > 0)
			{
				myDropzone2.processQueue();
			}
			else
			{
				myDropzone2.uploadFiles([]);

					
			}

			myDropzone2.removeAllFiles();
		}


	}

  if(!$('#thumb').length == 0) {
     Dropzone.options.myDropzone2 = false;
  var myDropzone3 = new Dropzone('#thumb', {     
    url: window.App.baseUrl+'download/upload_file',
    autoProcessQueue: false,
    parallelUploads: 10,
    maxFiles: 1,   
    thumbnailWidth: 250,
    thumbnailHeight: 250,
    addRemoveLinks: true,      
    uploadMultiple: true,
    acceptedFiles: 'image/*',
    maxFilesize: 0.0100,
    init: function() {
      this.on("success", function(file, responseText) {
        
        // var stat = JSON.parse(responseText);

        // if(stat.status == "success"){
        // localStorage.setItem('response',"success");
        new PNotify({
                  delay: 4000,
                  hide: true,
                  title: 'Message',
                  text: 'Success!',
                  type: 'success'
                });
                    setTimeout(function(){ 
                        location.reload();
                     }, 3000);  
        // }

        });
      this.on("error", function(file, message) { 
        new PNotify({
          delay: 4000,
          hide: true,
          title: 'Message',
          text: message,
          type: 'error'
        });
        this.removeFile(file); 
      });

    },    
    maxfilesexceeded: function(file) {
      this.removeAllFiles();
      this.addFile(file);
    },

  });

  }


  function upload_thumb(id)
  {
    if (myDropzone3.files.length > 0) {
      myDropzone3.options.url = window.App.baseUrl+'/download/upload_thumbnail/'+id;
      if (myDropzone3.getQueuedFiles().length > 0)
      {
        myDropzone3.processQueue();
      }
      else
      {
        myDropzone3.uploadFiles([]);

          
      }

      myDropzone3.removeAllFiles();
    }


  }

	$('.open_confirm_delete_modal').click(function(e){
		e.preventDefault();
    var id = $(this).attr('data-download-id');

    $('#btn_delete_modal').attr('data-id-delete',id);
    $('#confirm_delete_post').modal('show');
});

$('#btn_delete_modal').click(function(e){
  e.preventDefault()
  var id = $(this).attr('data-id-delete');
    $.ajax({
        url:window.App.baseUrl+'post/delete_post',
        type:'post',
        dataType:'json',
        data:{id:id},
        cache:!1,        
        success: function(e){          
           console.log(e);
           if (e.success) {
            new PNotify({
              delay: 4000,
              hide: true,
              title: 'Message',
              text: e.msg,
              type: 'success'
            });

           }

          setTimeout(function(){ 
              location.reload()
             }, 2000);  

        },
        error:function(e){

        }
    });
});



$('.view_post_details').click(function(e){
 e.preventDefault(e)

 var id = $(this).attr('data-download-id');
 var item = "";
 var thumbnail = "";
 var count = 0 ;
 var number_of_files = "";

  $('#update_post_modal').attr('data-id-update-post',id);
  $.ajax({
        url:window.App.baseUrl+'post/edit_post',
        type:'post',
        dataType:'json',
        data:{id:id},
        cache:!1,        
        success: function(e){
              var flag_thumb = 0;
              $.each(e, function(index, data) {
                
                if (data.thumbnail != "") {
                    thumbnail += '<button class="btn btn-primary" id = "change_thumbnails">Change Post thumbnail</button><button style= "display:none" data-att-id = "'+data.att_id+'"  data-id-blog = "'+data.blog_id+'"  id="yes_change_thumb" class = "btn btn-danger" >Yes</button><button style= "display:none" id="no_change_thumb" class = "btn btn-success">No</button><img id = "video_thumbs"  style = "width:100%" src="'+window.App.baseUrl+'uploads/post/'+data.thumbnail+'" alt="" />';
               $('#attach_thumb').hide();
                }else{
               //                   flag_thumb = 1
               //  thumbnail += '<button class="btn btn-primary" id = "change_thumbnails">Change Post thumbnail</button><button style= "display:none" data-att-id = "'+data.att_id+'"  data-id-blog = "'+data.blog_id+'"  id="yes_change_thumb" class = "btn btn-danger" >Yes</button><button style= "display:none" id="no_change_thumb" class = "btn btn-success">No</button><img id = "video_thumbs"  style = "width:100%" src="'+window.App.baseUrl+'uploads/post/'+data.thumbnail+'" alt="" />';
               // $('#attach_thumb').hide();
                }

 
              

               // if(data.thumb == 1 && data.type == "video"){
               //  $('#select_thumbnails').hide();                
               //  thumbnail += '<button class="btn btn-primary" id = "change_thumbnails">Change Post thumbnail</button>'
               //  thumbnail += '<button style= "display:none" data-att-id = "'+data.att_id+'" data-id-blog = "'+data.blog_id+'" id="yes_change_thumb" class = "btn btn-danger" >Yes</button>'
               //  thumbnail +='<button style= "display:none" id="no_change_thumb" class = "btn btn-success">No</button>'
               //  thumbnail +='<video id = "video_thumbs" autoplay  src="'+window.App.baseUrl+'uploads/post/'+data.path_file+'" type="video/mp4" style = "width:100%"></video>'
    
               // }else{
               //  // thumbnail += '<button></button>'
               
               // }

               if(data.path_file != null && data.type == 'any'){        
                 $('#attach_section').hide();
                  if (data.type  == "any") {
                  item += "<div class = 'text-center'><a style='margin:5px' class = 'btn btn-primary view_image ' href='"+window.App.baseUrl+'uploads/post/'+data.path_file+"' data-att-id = '"+data.att_id+"' download>"+data.path_file+" </a><button class='btn btn-danger' data-id-blog = '"+data.blog_id+"' data-att-id = '"+data.att_id+"' id = 'delete_attach_file'><i class='fa fa-trash'></i></button></div>";
                  count = count+1;
                  }
               }else{
                    $('#attach_section').show();
                  }
            
          });

              if (flag_thumb==1) {
                 $('#attach_section').hide();
              }

          number_of_files += '<span class = "badge">'+count+'</span>'
          var status_text = e[0].status;
          if (status_text == 0) {
            status_text = "Draft";
          }
          if (status_text == 1) {
            status_text = "Published";
          }
          console.log(e);
          $('#count').html(number_of_files);
          $('#download_section').html(item);
          $('#thumnail_edit').html(thumbnail);
          $('#modal_update').modal('show');
          $('#post_status').attr('data-id-status',id)        
          $('#post_status_title').html(status_text)
          $('#download_all_file').attr('data-file-download',id);
          $('#upload_new_thumb').attr('data-file-id',id);
          $('#add_any_file').attr('data-file-ids',id);
          $('#thumb_upload').attr('data-blog-id',id);       

          $('#post_sub_title').val(e[0].blog_sub_title);
          $('#post_title').val(e[0].blog_title);
          $('#post_link').val(e[0].blog_author);
          CKEDITOR.instances['blog_desc'].setData(e[0].blog_desc);       
       bindevent();
        },
        error:function(e){

        }
    });


  
});

 function bindevent(){

 $('#thumb_upload').unbind("click").click(function(e){
  e.preventDefault(e)
  var id = $(this).attr('data-blog-id');
    upload_thumb(id);       
 });

$('#delete_attach_file').click(function(e){

  e.preventDefault();
  var file_id = $(this).attr('data-att-id');
  var id = $(this).attr('data-id-blog');

      $.ajax({
        url:window.App.baseUrl+'post/change_file',
        type:'post',
        dataType:'json',
        data:{file_id:file_id,id:id},
        cache:!1,        
        success: function(e){
           new PNotify({
              delay: 4000,
              hide: true,
              title: 'Message',
              text: 'File Deleted you can add new one.',
              type: 'success'
            });
             $('#download_section').hide();
          $('#attach_section').show();
      
         
        },
        error:function(e){
           new PNotify({
              delay: 4000,
              hide: true,
              title: 'Message',
              text: 'File Deleted you can add new one.',
              type: 'success'
            });

          $('#download_section').hide();
          $('#attach_section').show();

        }
        
    });

})

 $('#change_thumbnails').unbind("click").click(function(e) {
      e.preventDefault()     
      $(this).hide();
$('#yes_change_thumb').show();
$('#no_change_thumb').show();
      

  }) 

$('#no_change_thumb').click(function(e){
  e.preventDefault(e);
  $(this).hide();
  $('#yes_change_thumb').hide();
  $('#change_thumbnails').show();
});

$('#yes_change_thumb').click(function(e){
  e.preventDefault();
  var file_id = $(this).attr('data-att-id');
  var id = $(this).attr('data-id-blog');

  $('#select_thumbnails').show();
  $('#video_thumbs').hide();
  $('#yes_change_thumb').hide();
  $('#no_change_thumb').hide();
  $('#change_thumbnails').hide();
     $('#attach_thumb').show();
      $.ajax({
        url:window.App.baseUrl+'download/change_thumbnails',
        type:'post',
        dataType:'json',
        data:{file_id:file_id,id:id},
        cache:!1,        
        success: function(e){          
           console.log(e);

        if(e.success){
     
        $('#yes_change_thumb').hide();
        $('#no_change_thumb').hide();
        $('#change_thumbnails').show();

        }
         
        },
        error:function(e){

        }
        
    });

});



$('#post_status').change(function(e){
  var id = $(this).attr('data-id-status');
  var status = $(this).val();
if($(this).val()==2){
  $('#schedule_post').modal('show');
  $('#save_schedule_post').attr('data-id-modal',id)
  return
}

  $.ajax({
        url:window.App.baseUrl+'post/download_checkthumb',
        type:'post',
        dataType:'json',
        data:{id:id,status:status},
        cache:!1,        
        success: function(e){          
           console.log(e);

           if (e.success) {
            new PNotify({
              delay: 4000,
              hide: true,
              title: 'Message',
              text: e.msg,
              type: 'success'
            });
           }else{
            new PNotify({
              delay: 4000,
              hide: true,
              title: 'Message',
              text: e.msg,
              type: 'warning'
            });
            
            // $('#select_thumbnails').css('border-color','red');
            return
           }
          // var test = $('tbody').find("tr[data-id ="+id+"]");
          //   console.log(test);
          //  $('.post_status').parent().parent().find("td[data-publish ="+id+"]").text(e.date);
          setTimeout(function(){ 
              location.reload()
             }, 2000);  
        },
        error:function(e){

        }
    });
});

 	  $('#add_any_file').click(function(e){
    e.preventDefault()
    var id = $(this).attr('data-file-ids');

  upload_file(id);
            if(localStorage.getItem('response') == "success"){
            localStorage.removeItem('response');

                new PNotify({
                  delay: 4000,
                  hide: true,
                  title: 'Message',
                  text: 'Success!',
                  type: 'success'
                });

                    setTimeout(function(){ 
                        location.reload();
                     }, 3000);  
          }else{
              // new PNotify({
              //     delay: 4000,
              //     hide: true,
              //     title: 'Message',
              //     text: 'Please add a file',
              //     type: 'warning'
              //   });
          }
  })
 	$('#update_post_modal').click(function(e){
  e.preventDefault()

  var id = $(this).attr('data-id-update-post');
 var post_title = $('#post_title').val();
 var post_author = $('#post_link').val();
 var post_sub_title = $('#post_sub_title').val();
 var blog_desc =CKEDITOR.instances['blog_desc'].getData();

     $.ajax({
        url:window.App.baseUrl+'post/edit_post_details',
        type:'post',
        dataType:'json',
        data:{id:id,post_title:post_title, post_author:post_author, blog_desc:blog_desc,post_sub_title:post_sub_title}, 
        cache:!1,
        success: function(e){          
           console.log(e);
           if (e.success) {
            new PNotify({
              delay: 4000,
              hide: true,
              title: 'Message',
              text: e.msg,
              type: 'success'
            });

           }

           setTimeout(function(){ 
              location.reload()
             }, 2000);  
        },
        error:function(e){

        }
    });

});
 }

});