$(function() {




	
	$('.post_download').show();
	$('.loading').hide();
//student//
   // STUDENT PART
   $("#view_student").click(function(){  
   	$('#page_view_star').val(1);         
   	$("#view_parent,#view_school").removeClass("active_nav"); 
   	$("#view_student").addClass("active_nav");
   	
   	$("*#student").stop().fadeIn(function(){

   	}); 
   	$('#main_star_view').show();
   	$('#main_parent_view').hide();
   	$('#main_school_view').hide();


   });

   $('#main_parent_view').hide();
   $('#main_school_view').hide();
    // PARENT PART
    $("#view_parent").click(function(){
    	$('#page_view_star').val(2)		 
    	$("#view_student,#view_school").removeClass("active_nav");
    	$("#view_parent").addClass("active_nav");
    	$("*#parent").stop().fadeIn(); 
    	$("#student,#school").stop().fadeOut(function(){

    	});

    	$('#main_star_view').hide();
    	$('#main_parent_view').show();
    	$('#main_school_view').hide();
    });

     // SCHOOL PART
     $("#view_school").click(function(){
     	$('#page_view_star').val(3)
     	
     	$("#view_student,#view_parent").removeClass("active_nav");
     	$("#view_school").addClass("active_nav");
     	$("*#school").stop().fadeIn(); 
     	$("#student,#parent").stop().fadeOut(function(){ });
     	$('#main_star_view').hide();
     	$('#main_parent_view').hide();
     	$('#main_school_view').show();
     });


     $("#student").click(function(){
     	$(this).hide();
     });





     $('.open_modal_desc').click(function(e){
     	e.preventDefault();
     	var id = $(this).attr('data-post');
     	
     	$.ajax({
     		type:'POST',
     		url: window.App.baseUrl+'blog/open_post',
     		dataType:'json',
     		data:{id:id},
     		cache:!1,
     		success:function(e){
     			$('#path').attr('src',window.App.baseUrl+'uploads/post/'+e.path);

     			$('#post_details').modal('show');
     			$('#view_post_title').text(e.title);
     			$('#view_blog_desc').html(e.desc);
     			$('#view_blog_path').html(e.link);
     			$('#view_blog_path').attr('href',e.link);			
     		},
     		error:function(e){

     		}
     	});		

     });


     $('.view_video_details').click(function(e){
     	e.preventDefault();
     	var id = $(this).attr('data-id');
     	
     	$.ajax({
     		type:'POST',
     		url: window.App.baseUrl+'blog/open_post',
     		dataType:'json',
     		data:{id:id},
     		cache:!1,
     		success:function(e){
     			$('#path').attr('src',window.App.baseUrl+'uploads/post/'+e.path);

     			$('#post_details').modal('show');
     			$('#view_post_title').text(e.title);
     			$('#view_blog_desc').html(e.desc);
     			$('#view_blog_path').html(e.link);
     			$('#view_blog_path').attr('href',e.link);			
     		},
     		error:function(e){

     		}
     	});		

     });
 });
$('.loadings').show();
$(document).ready(function(){
	var flag = 0;
	var flag_game = 0;
	var star_flag = 0;
	var flag_study = 0;
	var flag_article = 0;
	var flag_download = 0;
	var flag_student = 0;
	var flag_parent = 0;
	var flag_school = 0;

	$('#main_view').show();

//LOAD DOWNLOAD CENTER//
$('#download_view').show();
$('#download_article').hide();
$('#download_study').hide();
if ($('#main_view').hasClass('blog_download_page')) {
	
	setTimeout(function(){
		$.ajax({
			type:'GET',
			url:window.App.baseUrl+'blog/getDownloadView',
			data:{
				'offset':flag_study,
				'limit':8,
				'page_view':3,
			},
			success:function(e){
				if (e=="") {
					$('#download_study').css('height','500px');
				}

				$('#download_study').append(e);
				$('#end_of_file').hide();
				flag_study += 8;
			},
		}); 
		$.ajax({
			type:'GET',
			url:window.App.baseUrl+'blog/getDownloadView',
			data:{
				'offset':flag_article,
				'limit':8,
				'page_view':2,
			},
			success:function(e){
				if (e=="") {
					$('#download_article').css('height','500px');
				}

				$('#download_article').append(e);
				$('#end_of_file').hide();
				flag_article += 8;
			},
		});    
		$.ajax({
			type:'GET',
			url:window.App.baseUrl+'blog/getDownloadView',
			data:{
				'offset':flag_download,
				'limit':8,
				'page_view':1,
			},
			success:function(e){
				if (e=="") {
					$('#download_view').css('height','500px');
				}

				$('#download_view').append(e);
				$('#end_of_file').hide();
				flag_download += 8;
			},
		});  
		$('.loadings').hide();
	}, 1000);
//END LOAD DOWNLOAD CENTER//
}
//STAR SECTION//
$('#main_star_view').load(window.App.baseUrl + 'blog/load_student/'+flag_student+'/Student');
$('#main_parent_view').load(window.App.baseUrl + 'blog/load_parent/'+flag_student+'/Parent');
$('#main_school_view').load(window.App.baseUrl + 'blog/load_school/'+flag_student+'/School');

$('#next_star_view').click(function(e){
	e.preventDefault();
	$('.loading').show();	
	var page_view_star = parseInt($('#page_view_star').val());

	if (page_view_star == 1) {
		$('#main_star_view').empty();
		
			// $.ajax({
			// 	type:'GET',
			// 	url:window.App.baseUrl+'blog/getStarViewAjax',
			// 	data:{
			// 		'offset':flag_student,
			// 		'limit':3,
			// 		'page_view_star':page_view_star,
			// 	},
			// 	success:function(e){
			// 		console.log(e)
			// 		$('.loading').hide();
			//  		$('#main_star_view').append(e);
			//  		// $('#end_of_file').hide();
			// 		
			// 	},
			// }); 		
			// $('#main_star_view').load(window.App.baseUrl + 'blog/load_student/'+flag_student+'/Student');
			// flag_student += 3;
		}

		if (page_view_star == 2) {
			$('#main_parent_view').empty();
			$.ajax({
				type:'GET',
				url:window.App.baseUrl+'blog/getStarViewAjax',
				data:{
					'offset':flag_student,
					'limit':3,
					'page_view_star':page_view_star,
				},
				success:function(e){
					$('.loading1').hide();
					$('#main_parent_view').append(e);	

			 		// $('#end_of_file').hide();
			 		flag_student += 3;
			 	},
			 }); 		
		}

		if (page_view_star == 3) {
			$('#main_school_view').empty();
			$.ajax({
				type:'GET',
				url:window.App.baseUrl+'blog/getStarViewAjax',
				data:{
					'offset':flag_student,
					'limit':3,
					'page_view_star':page_view_star,
				},
				success:function(e){
					$('.loading2').hide();
					$('#main_school_view').append(e);
			 		// $('#end_of_file').hide();
			 		flag_student += 3;
			 	},
			 }); 		
		}		
	});


$('#next_view').click(function(e){
	e.preventDefault();
	var page_view = parseInt($('#page_view').val());

	if (page_view == 3) {
		
		$.ajax({
			type:'GET',
			url:window.App.baseUrl+'blog/getDownloadView',
			data:{
				'offset':flag_study,
				'limit':3,
				'page_view':page_view,
			},
			success:function(e){

				$('#download_study').append(e);
				$('#end_of_file').hide();
				flag_study += 3;
			},
		});    
		return		
	}

	if (page_view == 2) {
		
		$.ajax({
			type:'GET',
			url:window.App.baseUrl+'blog/getDownloadView',
			data:{
				'offset':flag_article,
				'limit':3,
				'page_view':page_view,
			},
			success:function(e){
				
				$('#download_article').append(e);
				$('#end_of_file').hide();
				flag_article += 3;
			},
		});    
		return		
	}

	if (page_view == 1) {

		$.ajax({
			type:'GET',
			url:window.App.baseUrl+'blog/getDownloadView',
			data:{
				'offset':flag_download,
				'limit':3,
				'page_view':page_view,
			},
			success:function(e){

				$('#download_view').append(e);
				$('#end_of_file').hide();
				flag_download += 3;
			},
		});    
		return		
	}    	    	
	
	
})


var page_view_star = parseInt($('#page_view_star').val());
var page_view = parseInt($('#page_view').val());

$("#myCarousel").swipe({
	swipeRight:function(){
		$('.left').click();
		
	},

	swipeLeft:function(){
		$('.right').click();
		
	}
});

$('#main,#main_view').swipe({
	swipeRight:function(){
		console.clear()  
		if ($('#main').hasClass('blog_index_page')) {
			$('#click_game').removeClass('active_nav');
			$('#click_post').addClass('active_nav');
			$('#end_of_file').show();
			$('#game_post').empty();
		}

		$('#click_game').removeClass('active_nav');
		$('#click_post').addClass('active_nav');
		$('#end_of_file').show();
		$('#game_post').empty();
		$(window).scrollTop(0); 


		setTimeout(function(){
			$('#end_of_file').hide();
			$('#click_post').click();
		}, 500);	    	


		if (page_view_star <=1) {     	
			$('#page_view_star').val(1);
		}else{
			page_view_star = page_view_star - 1;
			$('#page_view_star').val(page_view_star);
		}

		//DOWNLOAD BLOG VIEW SWIPE RIGHT
		if (page_view <=1) {
			$('#page_view').val(1);
		}else{
			page_view = page_view - 1;
			$('#page_view').val(page_view);

		}      
		page_show()

		//END SWIPE RIGHT//
	},

	swipeLeft:function(){    
		console.clear()   
		if ($('#main').hasClass('blog_index_page')) {
			$('#click_post').removeClass('active_nav');
			$('#click_game').addClass('active_nav');
			$('#end_of_file').show();
			$('#blog_post').empty();
		}

		$(window).scrollTop(0);


		setTimeout(function(){
			$('#click_game').click();
			$('#end_of_file').hide();
		}, 500);

		if (page_view_star >=3) {
			$('#page_view_star').val(3);
		}else{   
			page_view_star = page_view_star + 1;
			$('#page_view_star').val(page_view_star);
		}

			//DOWNLOAD BLOG VIEW SWIPE LEFT
			if (page_view >=3) {
				$('#page_view').val(3);
			}else{
				page_view = page_view + 1;
				$('#page_view').val(page_view);
			}
			page_show()
			//END SWIPE RIGHT//
		}
	});  


function page_show()
{
	if ($('#page_view').val()=='1') {    
		$('#download_view').show();
		$('#download_article').hide();
		$('#download_study').hide();
	}

	if ($('#page_view').val()=='2') {
		
		$('#download_view').hide();
		$('#download_article').show();
		$('#download_study').hide();

		
	}

	if ($('#page_view').val()=='3') {
		
		$('#download_view').hide();
		$('#download_article').hide();
		$('#download_study').show();
	}

	if ($('#page_view_star').val()==1) {
		$("#view_student").click();
		
	}

	if ($('#page_view_star').val()==2) {
		
		$("#view_parent").click();
	}

	if ($('#page_view_star').val()==3) {
		
		$("#view_school").click();
	}


}



$('#student_click').click(function(e){
	e.preventDefault();
	$('#page_view').val(1);
	$('#download_view').show();
	$('#download_article').hide();
	$('#download_study').hide();
});
$('#article_click').click(function(e){
	e.preventDefault();
	$('#page_view').val(2);
	$('#download_view').hide();
	$('#download_article').show();
	$('#download_study').hide();
});
$('#download_click').click(function(e){
	e.preventDefault();
	$('#page_view').val(3);
	$('#download_view').hide();
	$('#download_article').hide();
	$('#download_study').show();
});

var loading = '<img src="'+window.App.baseUrl+'/images/loading.gif" alt="load" style="width:50px"/>'
$('#end_of_file').html(loading);
setTimeout(function(){
	$('#end_of_file').hide();
}, 1000);

	// $('#blog_post').hide();
	// $('#game_post').hide();

	// setTimeout(function(){ 
	// 	$('#blog_post').fadeIn();
	// 	$('#game_post').fadeIn();
	// }, 10000);

	
	if ($('#current_page').val() == 'post') {
		$('#end_of_file').html(loading);
		setTimeout(function(){
			$('#end_of_file').hide();
		}, 1000);
		$.ajax({
			type:'GET',
			url:window.App.baseUrl+'blog/get_post_data',
			data:{
				'offset':0,
				'limit':3
			},
			success:function(e){
				if (e=="") {
					$('#blog_post').css('height','500px');
				}else{
					$('#game_post').css('height','0px');
				}
				setTimeout(function(){
					$('#blog_post').fadeIn(function(){
						$('#blog_post').append(e);
					});					
					$('#end_of_file').hide();

				}, 1000);
				
				
				flag += 3;
			},
		});
	}
	if ($('#current_page').val()=='game') {
		$('#end_of_file').show();
		$.ajax({
			type:'GET',
			url:window.App.baseUrl+'blog/get_game_data',
			data:{
				'offset':0,
				'limit':3
			},
			success:function(e){
				
				setTimeout(function(){
					$('#game_post').fadeIn(function(){
						$('#game_post').append(e);
					});				
					$('#end_of_file').hide();

				}, 1000);
				
				
				flag_game += 3;
			},
		});
	}

	$('#click_post').click(function(e){
		e.preventDefault();
		if ($('#current_page').val() == 'post' ) {			
		// $('#click_game').click();
		return
	}else{
		$(window).scrollTop(0);
		flag = 0;
	}

	$('#end_of_file').show();
	$('#current_page').val('post');
	$('#game_post').fadeOut();
	$('#game_post').empty();
	$('#blog_post').empty();
	$(this).addClass('active_nav');
	$('#click_game').removeClass('active_nav');
	$.ajax({
		type:'GET',
		url:window.App.baseUrl+'blog/get_post_data',
		data:{
			'offset':0,
			'limit':3
		},
		success:function(e){
			if (e=="") {
				$('#blog_post').css('height','500px');
			}else{
				$('#game_post').css('height','0px');
			}
			
			$('#blog_post').fadeIn(function(){
				$('#blog_post').append(e);
			});			
			$('#end_of_file').hide();
			flag += 3;
		},
	});

})

	$('#click_game').click(function(e){
		e.preventDefault();
		$('#end_of_file').show();
		if ($('#current_page').val()=='game') {
		// $('#click_post').click();
		return
	}else{
		$(window).scrollTop(0);
		flag_game = 0;
	}

	$('#end_of_file').show();
	$('#current_page').val('game');			
	$('#blog_post').fadeOut();
	$('#game_post').empty();
	$('#blog_post').empty();

	$(this).addClass('active_nav');
	$('#click_post').removeClass('active_nav');
	$.ajax({
		type:'GET',
		url:window.App.baseUrl+'blog/get_game_data',
		data:{
			'offset':0,
			'limit':3
		},
		success:function(e){
			if (e=="") {
				$('#game_post').css('height','500px');
			}else{
				$('#blog_post').css('height','0px');

			}

			$('#game_post').fadeIn(function(){
				$('#game_post').append(e);
				
			});		
			$('#end_of_file').hide();

			
			
			flag_game += 3;
		},
	});

})

	$('#current_page').change(function(){

		if ($('#current_page').val() == 'post') {

		}
		if ($('#current_page').val()== 'game') {

		}

	});

	$(window).scroll(function(){

		if (page_view == 1) {
			$('#study_scroll').val($(window).scrollTop());
		}

		if (page_view == 2) {
			$('#article_scroll').val($(window).scrollTop());
		}
		if (page_view == 3) {
			$('#download_scroll').val($(window).scrollTop());
		}

		
		if ($(window).scrollTop() >= $(document).height() - $(window).height()) {
			// $('#next_star_view').click();
			$('#next_view').click();			
			
			if ($('#current_page').val()=='post') {
				$.ajax({
					type:'GET',
					url:window.App.baseUrl+'blog/get_post_data',
					data:{
						'offset':flag,
						'limit':3
					},
					success:function(e){
						
						// if (e=='No Post Available') {
						// 	$('#end_of_file').html('No more post availble');
						// 	return
						// }
						$('#blog_post').append(e);
						flag += 3;
					},error:function(e){

					}
				});
			}

			if ($('#current_page').val()=='game') {
				$.ajax({
					type:'GET',
					url:window.App.baseUrl+'blog/get_game_data',
					data:{
						'offset':flag_game,
						'limit':3
					},
					success:function(e){
						
			// if (e=='No Post Available') {
			// 	$('#end_of_file').html('No more post availble');
			// 	return
			// }
			$('#game_post').append(e);
			flag_game += 3;
		},error:function(e){

		}
	});	
			}

		}
	});


	$("*#blogvid").bind("play",function(){
		$(this).next(".desc_video").hide();			
	});	
	$("*#blogvid").bind("pause",function(){
		$(this).next(".desc_video").show();			
	});	

	$('.game_desc').click(function(e){
		e.preventDefault()
		var id = $(this).attr('data-post');
		var title = $(this).attr('data-title');
		
		$.ajax({
			type:'POST',
			url: window.App.baseUrl+'blog/open_post',
			dataType:'json',
			data:{id:id},
			cache:!1,
			success:function(e){
				$('#game_title_modal').text(title);
				$('#body_game_desc').html(e.desc);
				$('#game_desc_modal').modal('show');		
			},
			error:function(e){

			}
		});
	})

	$('.article_desc').click(function(e){
		e.preventDefault()
		var id = $(this).attr('data-post');
		$('#article_title_modal').text($(this).attr('data-title'));
		$.ajax({
			type:'POST',
			url: window.App.baseUrl+'blog/open_post',
			dataType:'json',
			data:{id:id},
			cache:!1,
			success:function(e){
				
				$('#body_article_desc').html(e.desc);
				$('#game_desc_modal').modal('show');		
			},
			error:function(e){

			}
		});		
		$('#article_desc_modal').modal('show');
	});
	
	$('.study_desc').click(function(e){
		e.preventDefault()
		var id = $(this).attr('data-post');
		
		$('#study_title_modal').text($(this).attr('data-title'));
		$.ajax({
			type:'POST',
			url: window.App.baseUrl+'blog/open_post',
			dataType:'json',
			data:{id:id},
			cache:!1,
			success:function(e){
				
				$('#body_study_desc').html(e.desc);
				$('#game_desc_modal').modal('show');		
			},
			error:function(e){

			}
		});		
		$('#study_desc_modal').modal('show');
	});
});