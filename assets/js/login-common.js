
 	$(document).ready(function(){
 		$("#sign").click(function(){
 			$("#sign").hide();
 			$("#log").show();
 			$("#head_logo").fadeOut();
 			$(".log").stop().fadeOut(function(){
 				$(".signup").stop().fadeIn();
 				$("#upload_img").stop().fadeIn();
 			});
 		});
 		$("#log").click(function(){
 			$("#log").hide();
 			$("#sign").show();
 			$("#upload_img").stop().fadeOut(function(){
 				$("#head_logo").stop().fadeIn();
 			});
 			$(".signup").stop().fadeOut(function(){
 				$(".log").stop().fadeIn();
 			});
 		});

 		$(".fa-cog").click(function(){
  			$("#accnt_set").toggle();
  		});

 		$('#account_settings').click(function(e){
 			$("#accnt_set").hide();  
 			$('#main_page').fadeOut(function(){
 				$('#login_design').fadeIn(); 				
 			});
 		});

 		$('#back_to_main').click(function(e){
 			e.preventDefault()
 			$('#login_design').fadeOut(function(){
 			$('#main_page').fadeIn();
 		});
 		})

 		$('#change_profile').click(function(e){
 			e.preventDefault();
 			$('#change_profile_modal').modal('show');
 			$('.dz-default').find('span').html('<span>Tap to upload</span>');
 					 		});

 		$('#change_profile_btn').click(function(e){
 			e.preventDefault()
profile_upload();
 		});

$('#edit_user').click(function(e){
var first_name = $('#first_name').val();
var last_name = $('#last_name').val();
var email = $('#email').val();
var pass = $('#pass').val();
var con_pass = $('#con_pass').val();
var error = "";

if(first_name == "" || last_name == "" || email == ""){
		if(first_name == ""){
		error += "First Name is missing!";
		}
		if(last_name == ""){
		error += "Last Name is missing!";
		}
		if(email == "" ){
		error += "Email  is missing!";
		}
		

			new PNotify({
				delay: 4000,
				hide: true,
				title: 'Message',
				text: error,
				type: 'warning'
				});
			return
}
if(pass != ""){
	if (pass != con_pass) {
	error += "Password and Confirm password dont match";
	new PNotify({
				delay: 4000,
				hide: true,
				title: 'Message',
				text: error,
				type: 'warning'
				});
			return
	}
}

   
			$.ajax({
				url: window.App.baseUrl + 'home/update_profile',
				type:'post',
				dataType:'json',
				cache:!1,
				data:{first_name:first_name, last_name:last_name, email:email, pass:pass},
				 success:function(e){										
					if (e.success) 
					{
						new PNotify({
				delay: 4000,
				hide: true,
				title: 'Message',
				text: e.msg,
				type: 'success'
				});
					}
				},
				error:function(e){
					
				}

			});


})

 

 		Dropzone.autoDiscover = false;
	var myDropzone = new Dropzone('form#profile_picture',  { 
		url: window.App.baseUrl+'star/upload_file',
		autoProcessQueue: false,
		parallelUploads: 10,		
		thumbnailWidth: 250,
		thumbnailHeight: 250,
		addRemoveLinks: true,
		maxFiles: 1,   
		// acceptedFiles: 'video/*',
		uploadMultiple: false,
		init: function() {
			this.on("success", function(file, responseText) {

            setTimeout(function(){ 
                location.reload();
             }, 3000);  
        });
			this.on("error", function(file, message) { 
				new PNotify({
					delay: 4000,
					hide: true,
					title: 'Message',
					text: message,
					type: 'error'
				});
				this.removeFile(file); 
			});

		},    
		maxfilesexceeded: function(file) {
			this.removeAllFiles();
			this.addFile(file);
		},
	});	

	function profile_upload()
	{
		if (myDropzone.files.length > 0) {

			myDropzone.options.url = window.App.baseUrl+'home/change_profile/'
			//myDropzone.options.url = 'upload_file/'+id

			if (myDropzone.getQueuedFiles().length > 0)
			{
				myDropzone.processQueue();
			}
			else
			{
				myDropzone.uploadFiles([]);
			}

			myDropzone.removeAllFiles();
		}


	} 
 
 	});